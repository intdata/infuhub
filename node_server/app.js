process.on('uncaughtException', function (err) {
  console.log("Execption : Node Exit Prevented ", err);
});


var express = require('express');
var path = require('path');
var http = require('http');
var logger = require('morgan');
var fs = require('fs');
var path = require('path');

//**=============== Sequalize init =============== */
mysql = require("mysql");
mysql_config = require("./config/mysql_config.js");
Sequelize = require("sequelize");

global.con = mysql.createConnection(mysql_config);

con.connect(function(err) {
  if (err) throw err;
  console.log("Mysql Connected with Sequalize!");
});

global.sequelize = new Sequelize(
  mysql_config.database,
  mysql_config.user,
  mysql_config.password,
  {
    host: mysql_config.host,
    dialect: "mysql",
    //operatorsAliases: false,

    pool: {
      max: 5,
      min: 0,
      acquire: 30000,
      idle: 10000
    }
  }
);

sequelize
  .authenticate()
  .then(() => {
    console.log("Connection has been established successfully.");
  })
  .catch(err => {
    console.error("Unable to connect to the database:", err);
  });

//**=============== Sequalize init =============== */


var bodyParser = require('body-parser');
//var config= require('./config/config').CONF_VAR;
var config = require('./admin/config/config');
////////////////////router file start////////////////////////////////////////////
var sspl 				= require('./admin/routes/sspl');
var common_function 				= require('./admin/routes/common_function');
var adminuser 			= require('./admin/routes/adminuser');
var creator 			= require('./admin/routes/creator');
var brand_owner 		= require('./admin/routes/brand_owner');
var admincms 			= require('./admin/routes/admincms');
var adminmenu 			= require('./admin/routes/adminmenu');
var faq 			= require('./admin/routes/faq');
var front_end_menu 			= require('./admin/routes/front_end_menu');
//var campaign 			= require('./admin/routes/campaign');
var email_template 			= require('./admin/routes/admin_email_template');
var testimonial 			= require('./admin/routes/testimonial');
var notification_templates 			= require('./admin/routes/notification-templates');
var general = require('./admin/routes/index');
var brand_owner_admin_transaction = require('./admin/routes/brand_owner_admin_transaction');
var influencer_admin_transaction = require('./admin/routes/influencer_admin_transaction');
var pdf = require('./admin/routes/pdf');


////////////////////////////router file start //////////////////
var app = express();
app.use(express.static(config.asset_path));
/////////////////// Passing db Object for router ends//////////////////////////
app.use(function (req, res, next) {        
    res.setHeader('Access-Control-Allow-Origin', config.base_path);     
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');    
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');      
    res.setHeader('Access-Control-Allow-Credentials', true);      
    next();  
});

//================= body parser===================//
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
//=================================================//

app.use(logger('dev'));
// log all requests to access.log
app.use(logger('common', {
  stream: fs.createWriteStream(path.join(__dirname, 'access.log'), { flags: 'a' })
}))

app.use('/sspl', sspl);
app.use('/common_function', common_function);
app.use('/adminuser', adminuser);
app.use('/creator', creator);
app.use('/brand_owner', brand_owner);
app.use('/admincms', admincms);
app.use('/adminmenu', adminmenu);
app.use('/faq', faq);
app.use('/front_end_menu', front_end_menu);
//app.use('/campaign', campaign);
app.use('/email_template', email_template);
app.use('/testimonial', testimonial);
app.use('/notification-templates', notification_templates);
app.use('/', general);
app.use('/brand_owner_admin_transaction', brand_owner_admin_transaction);
app.use('/influencer_admin_transaction', influencer_admin_transaction);
app.use('/pdf', pdf);



app.listen(config.port);
//app.listen(app.get('port'));
console.log('server start running at port '+ config.port);
module.exports = app;	
