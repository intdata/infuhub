//var _config = {}
//_config.PORT                = 8000;
//exports.CONF_VAR = _config;

var mysql = require('mysql');
var con = mysql.createPool({
  connectionLimit: 5,
  host: '127.0.0.1',
  user: 'root',
  password: 'root', 
  database: 'infuhub'
});

con.getConnection(function(err, connection) {
  // execute query
  if (err) throw err;
    console.log("Connected!");
});

module.exports = {con};