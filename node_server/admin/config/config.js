//var _config = {}
//_config.PORT                = 8000;
//exports.CONF_VAR = _config;



module.exports = {
    port : 8002,
    host : '127.0.0.1',
    asset_path: "public/assets/",
    base_path : 'http://10.0.4.4:42001',
    inventory_path: 'http://10.0.4.4:8002/',
    frontEndBaseUrl: 'http://10.0.4.4:42001',
    upload_path : 'http://10.0.4.4:8002/uploads/',
	no_profile_image: "http://10.0.4.4:8002/images/common/no_image.png",
    company_default_logo: "http://10.0.4.4:8002/images/common/company-default-logo.png",
    adminBaseUrl: "http://10.0.4.4:42001/",
    adminAssetpath: "public/assets/",
 
 	DB_CON : {
         user: 'root',
        password: 'root',
        server: '127.0.0.1',
        database: 'infuhub',
         options: {
             encrypt: true
         }
    },
    smtpConfig: {
        service: "Gmail",
        auth: {
            user: "guulpayapp@gmail.com",
            pass: "Passw0rd@123"
        }
    },

    slugify : function (string) {

          const a = 'àáäâãåèéëêìíïîòóöôùúüûñçßÿœæŕśńṕẃǵǹḿǘẍźḧ·/_,:;'
          const b = 'aaaaaaeeeeiiiioooouuuuncsyoarsnpwgnmuxzh------'
          const p = new RegExp(a.split('').join('|'), 'g')

          return string.toString().toLowerCase()
            .replace(/\s+/g, '-') // Replace spaces with
            .replace(p, c => b.charAt(a.indexOf(c))) // Replace special characters
            .replace(/&/g, '-and-') // Replace & with ‘and’
            .replace(/[^\w\-]+/g, '') // Remove all non-word characters
            .replace(/\-\-+/g, '-') // Replace multiple — with single -
            .replace(/^-+/, '') // Trim — from start of text .replace(/-+$/, '') // Trim — from end of text
},
mysql_real_escape_string:function(str) {
    return str.replace(/[\0\x08\x09\x1a\n\r"'\\\%]/g, function (char) {
        switch (char) {
            case "\0":
                return "\\0";
            case "\x08":
                return "\\b";
            case "\x09":
                return "\\t";
            case "\x1a":
                return "\\z";
            case "\n":
                return "\\n";
            case "\r":
                return "\\r";
            case "\"":
            case "'":
            case "\\":
            case "%":
                return "\\"+char; // prepends a backslash to backslash, percent,
                                  // and double/single quotes
        }
    });
}

    
   
}


