var config = require('../config/config');
var express = require('express');
var router = express.Router();
var db_connection = require('../config/db_connection');
var crypto = require('crypto');

router.post('/addMenu/', function(req, res) {
    var data    = {}; 
    var query   = '';
    var msg     = '';
    var qry     = '';
    var pqry    = ''; 
    var id      = req.body.id;
    if(id>0){
        query = " UPDATE ";
        msg = 'Record updated successfully';
    }
    else{
        query = " INSERT INTO ";
        msg = 'Record inserted successfully';
    }

    query += "admin_menu_master SET parent_id = '"+req.body.parent_id+"', menu_name = '"+req.body.menu_name+"', menu_slug = '"+req.body.menu_slug+"', menu_group_name = '"+req.body.menu_group_name+"', menu_order = '"+req.body.menu_order+"', status = '"+req.body.status+"'";
    if(id>0){
        query += " WHERE id="+id;
    }

    db_connection.con.query(query, function(err, result) {
        if (!!err) {
            data["msgcode"] = 0;
            data["msgtext"] = 'Error';
            data["results"] = err;
            res.send(data);
        }
        else {
            if(id < 1){
                qry = "SELECT id FROM admin AS A LEFT JOIN admin_role_type AS AR ON A.role_id=AR.id WHERE AR.admin_type='super-admin'";
                db_connection.con.query(qry, function(er,rs){
                    if(rs.length>0){
                        for(var i=0; i<rs.length; i++){
                            pqry = "INSERT INTO admin_menu_permission SET admin_menu_id = "+result.insertId+", admin_user_id = "+rs[i].id;
                            db_connection.con.query(pqry);
                        }
                    }
                });
            }

            data["msgcode"] = 1;
            data["msgtext"] = msg;
            data["results"] = result;
            res.send(data);
        }
    });
});

router.get('/menuList/', function(req, res){
    var data = {}; 
    var query = "SELECT T1.*, T2.menu_name AS 'group_name' FROM admin_menu_master AS T1 LEFT JOIN admin_menu_master AS T2 ON T1.parent_id = T2.id ";
    db_connection.con.query(query, function(err, result) {
        if (!!err) {
            data["msgcode"] = 0;
            data["msgtext"] = 'Error';
            data["results"] = err;
            res.send(data);
        }
        else {
            data["msgcode"] = 1;
            data["msgtext"] = 'Success';
            data["results"] = result;
            res.send(data);
        }
    });
});

router.get('/menuGroupName/', function(req, res){
    var data = {}; 
    var query = "SELECT *FROM admin_menu_master where parent_id = 0 and status = 'Active'";
    db_connection.con.query(query, function(err, result) {
        if (!!err) {
            data["msgcode"] = 0;
            data["msgtext"] = 'Error';
            data["results"] = err;
            res.send(data);
        }
        else {
            data["msgcode"] = 1;
            data["msgtext"] = 'Success';
            data["results"] = result;
            res.send(data);
        }
    });
});

router.get('/menuPermissionList/:adminId', function(req, res){
    var data = {}; 
    var adminId = req.params.adminId;
    var query = "SELECT AMP.*, AAM.menu_name, AAM.menu_slug FROM admin_menu_permission AS AMP LEFT JOIN admin_menu_master AS AAM ON AMP.admin_menu_id = AAM.id WHERE AMP.admin_user_id = "+adminId+" AND AAM.parent_id > 0";
    db_connection.con.query(query, function(err, result) {
        if (!!err) {
            data["msgcode"] = 0;
            data["msgtext"] = 'Error';
            data["results"] = err;
            res.send(data);
        }
        else {
            data["msgcode"] = 1;
            data["msgtext"] = 'Success';
            data["results"] = result;
            res.send(data);
        }
    });
});


router.get('/singleMenu/:id', function(req, res){
    var data = {};
    var id = req.params.id; 
    var query = "SELECT *FROM admin_menu_master where id = "+id;
    db_connection.con.query(query, function(err, result) {
        if (!!err) {
            data["msgcode"] = 0;
            data["msgtext"] = 'Error';
            data["results"] = err;
            res.send(data);
        }
        else {
            data["msgcode"] = 1;
            data["msgtext"] = 'Success';
            data["results"] = result[0];
            res.send(data);
        }
    });
});

// =================== Admin Permission ===================================================//
router.post('/permission/', function(req, res) {
    var data = {};
    var admin_id            = req.body.admin_id;
    var menu_id             = req.body.menu_id;
    var permission_action   = req.body.permission_action;
    var isCheck             = req.body.is_check;

    var query   = "UPDATE admin_menu_permission SET permission_"+permission_action+" = "+isCheck+" WHERE admin_menu_id = "+menu_id+" AND admin_user_id = "+admin_id;
 
    db_connection.con.query(query, function(err, result) {
        if (!!err) {
            data["msgcode"] = 1;
            data["msgtext"] = 'Invalid Credential';
            data["results"] = err;
        }
        else {
            data["msgcode"] = 1;
            data["msgtext"] = 'Success';
            data["results"] = result;
        }
        res.send(data);
    });
});

router.post('/getPermission/', function(req, res) {
    var data = {};
    var newData = [];
    var adminId = req.body.adminId;
    var query = "SELECT permission_name FROM permission WHERE admin_id='" + adminId + "'";
    db_connection.con.query(query, function(err, result) {
        data["msgcode"] = 1;
        data["msgtext"] = 'Success';
        data["results"] = result;
        res.send(data);
    });
});



router.get('/sidebarMenu/', function(req, res){
    var data = {};
    var newData = {};       
    var testData = [];
    var inc = 0; 
    var mydbdata;
    var menu_id;
    var query = "SELECT T1.id AS 'group_id', T1.menu_name AS 'group_name' FROM admin_menu_master AS T1 where T1.parent_id = 0 ORDER BY T1.menu_order ASC";
    db_connection.con.query(query, function(err, result) {     

        if (!!err) {
            data["msgcode"] = 0;
            data["msgtext"] = 'Error';
            data["results"] = err;
        }
        else {
            if(result.length>0)
            {
                for(i=0;i<result.length; i++)
                {
                    menu_id = result[i].group_id;
                    groupname = result[i].group_name;                    
                    (function(menu_id,groupname,newData){                    
                        var qry = "SELECT T1.id, T1.menu_name, T1.menu_slug FROM admin_menu_master AS T1 where T1.parent_id = "+menu_id+" ORDER BY T1.menu_order ASC";
                        db_connection.con.query(qry, function(err, resl) {                            
                           menudata(result.length,resl,groupname,i);                            
                        });
                    })(menu_id,groupname,newData);                                      
                }
            }            
        }
    });  
    function menudata(length,resl,groupname,i){
         mydata = {};
         mydata.parentMenuName = {};
         mydata.menuList = {};

        inc++;
        mydata.parentMenuName= groupname; 
        mydata.menuList=  resl;  
        newData[inc] =mydata;

        if(length == inc){
            data["msgcode"] = 1;
            data["msgtext"] = 'Success';
            data["results"] = newData;           
            res.send(data);
        }
    }

});

router.get('/parentMenuName/:id', function(req, res){
    var data = {};
    var id = req.params.id; 
    var query = "SELECT T2.menu_name AS 'group_name' FROM admin_menu_master AS T1 LEFT JOIN admin_menu_master AS T2 ON T1.parent_id = T2.id where T1.id = "+id;
    db_connection.con.query(query, function(err, result) {
        if (!!err) {
            data["msgcode"] = 0;
            data["msgtext"] = 'Error';
            data["results"] = err;
            res.send(data);
        }
        else {
            data["msgcode"] = 1;
            data["msgtext"] = 'Success'; 
            data["results"] = result[0].group_name;
            res.send(data);
        }
    });
});

//====Admin Role Permission===============//
router.get('/menuRolePermissionList/:roleId', function(req, res){
    var data = {}; 
    var roleId = req.params.roleId;
    var query = "SELECT ARP.*, AAM.menu_name, AAM.menu_slug FROM admin_role_permission AS ARP LEFT JOIN admin_menu_master AS AAM ON ARP.menu_id = AAM.id WHERE ARP.role_id = "+roleId+" AND AAM.parent_id > 0";   
    db_connection.con.query(query, function(err, result) {
        if (!!err) {
            data["msgcode"] = 0;
            data["msgtext"] = 'Error';
            data["results"] = err;
            res.send(data);
        }
        else {
            data["msgcode"] = 1;
            data["msgtext"] = 'Success';
            data["results"] = result;
            res.send(data);
        }
    });
});
router.get('/getAdminRoleTypeName/:id', function(req, res){
    var data = {};
    var id = req.params.id; 
    var query = "SELECT * FROM admin_role_type where id = "+id;
    db_connection.con.query(query, function(err, result) {
        if (!!err) {
            data["msgcode"] = 0;
            data["msgtext"] = 'Error';
            data["results"] = err;
            res.send(data);
        }
        else {
            data["msgcode"] = 1;
            data["msgtext"] = 'Success';
            data["results"] = result[0];
            res.send(data);
        }
    });
});
router.post('/rolePermission/', function(req, res) {
    var data = {};
    var role_id            = req.body.role_id;
    var menu_id             = req.body.menu_id;
    var permission_action   = req.body.permission_action;
    var isCheck             = req.body.is_check;
    var query   = "UPDATE admin_role_permission SET permission_"+permission_action+" = "+isCheck+" WHERE menu_id = "+menu_id+" AND role_id = "+role_id;
    console.log(query);
    db_connection.con.query(query, function(err, result) {
        if (!!err) {
            data["msgcode"] = 1;
            data["msgtext"] = 'Invalid Credential';
            data["results"] = err;
        }
        else {
            data["msgcode"] = 1;
            data["msgtext"] = 'Success';
            data["results"] = result;
        }
        res.send(data);
    });
});
router.get('/getAllRoleType/', function(req, res){
    var data = {};
    var id = req.params.id; 
    var query = "SELECT * FROM admin_role_type" ;
    db_connection.con.query(query, function(err, result) {
        if (!!err) {
            data["msgcode"] = 0;
            data["msgtext"] = 'Error';
            data["results"] = err;
            res.send(data);
        }
        else {
            data["msgcode"] = 1;
            data["msgtext"] = 'Success';
            data["results"] = result;
            res.send(data);
        }
    });
});



module.exports = router;