var express = require('express');
var router = express.Router();

//**=============== Sequalize init =============== */
Sequelize = require("sequelize");
Op = Sequelize.Op;
NotificationTemplates = require("./../../models/notification_template");

var notificationTemplatesObj = NotificationTemplates(sequelize, Sequelize);
//**=============== Sequalize init =============== */

/**
 * Method to get all active Notification Template list
 */
router.post('/get-notification-templates', function(req, res, next) {
  notificationTemplatesObj.findAll({
    where: { status: 1 }
  })
  .then(industries => { 
    return res.status(200).json({ msgcode: 1, msgtext: 'All active notification template list.', results: industries});
  })
  .catch(reason => {
    return res.status(200).json({ msgcode: 2, msgtext: 'Something went wrong!.', "error": reason});
  });
});

/**
 * Method to get Notification Template by id
 */
router.get('/get-single-notification-template', async function(req, res, next) {
  let { id } = req.query;
  if(!id){
    return res.status(200).json({
        responseCode: "420",
        responseDetails: "Something went wrong!",
    });
  }

  notificationTemplatesObj.findOne({
    where: { id: id },
  }).then(result => { 
    return res.status(200).json({ msgcode: 1, msgtext: 'Notification template data.', results: result});
  })
  .catch(reason => {
    return res.status(200).json({ msgcode: 2, msgtext: 'Something went wrong!.', "error": reason});
  });
});

/**
 * Method to updtae notification template
 */
router.post('/update-notification-template', async function(req, res) {
  let { title, content, id } = req.body;

  if (!title || !content || !id) {
      return res.status(200).json({
          msgcode: "201",
          msgtext: "Parameter missing.",
      });
  }

  let template = await getNotificationTemplate({ id: id });

  if(template){
    let submitData = {
      title: title,
      content: content
    }

    // update template
    template
      .update(submitData)
      .then((result) => {
        if(result){
          return res.json({msgcode: 200, msgtext: 'Success! Notification template has been changed.'});
        } else{
          return res.json({msgcode: 420, msgtext: 'Something went wrong!'});
        }
      }).catch(error => {
        // Ooops, do some error-handling
        return res.json({msgcode: 420, msgtext: 'Something went wrong!', error: error});
      });
  } else{
    return res.status(200).json({
        msgcode: 404,
        msgtext: "Notification template not found.",
    });
  }

});

/**
 * Method to get Notification Template by given condition
 * @param {*} obj 
 */
const getNotificationTemplate = async obj => {
  return await notificationTemplatesObj.findOne({
    where: obj,
  });
};

module.exports = router;
