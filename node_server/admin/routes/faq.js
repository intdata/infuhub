var config  = require('../config/config');
var express = require('express');
var router = express.Router();
var db_connection  = require('../config/db_connection');


router.post('/addeditFaqData/', function(req, res) {

	var x = config.mysql_real_escape_string("hello's");
	console.log(x);

   var data    = {};
   var query   = '';
   var msg     = '';
   var qry     = '';
   var pqry    = '';
   var extraquery ='';

   var id      = req.body.id;
   var lanData = ['en','ar'];

    var today = new Date();
	var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate(); 
	var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
	var dateTime = date+' '+time;

   if(id>0){
       query = " UPDATE ";
       extraquery =", updated_at  = '"+dateTime+"'";
       msg = 'Record updated successfully';
   }else{
       query = " INSERT INTO ";
       msg = 'Record inserted successfully';
       extraquery =", updated_at  = '"+dateTime+"', created_at   = '"+dateTime+"'";
   } 
   query += "faq SET status = '"+req.body.status+"'"+extraquery;
   if(id>0){
       query += " WHERE id="+id;
   }
   db_connection.con.query(query, function(err, result) {
       if (!!err) {
       	   console.log(err);

           data["msgcode"] = 0;
           data["msgtext"] = 'Error';
           data["results"] = err;
           res.send(data);

       }else {

             if(id > 0){

               qry = "DELETE FROM faq_lang WHERE faq_lang.faq_id = '"+id+"'";
               db_connection.con.query(qry, function(er,rs){                  
                       for(var i=0; i<lanData.length; i++){                           
                           pqry = "INSERT INTO faq_lang SET faq_id = "+id+",lang_code  = '"+lanData[i]+"', question  = '"+config.mysql_real_escape_string(req.body['quesn_'+lanData[i]])+"', answer  = '"+config.mysql_real_escape_string(req.body['ans_'+lanData[i]])+"'";
                          db_connection.con.query(pqry);                              
                       }

                  }); 

				data["msgcode"] = 1;
				data["msgtext"] = msg;
				data["results"] = result;
				res.send(data); 

                   }else{

                       for(var i=0; i<lanData.length; i++){

                            pqry = "INSERT INTO faq_lang SET faq_id = '"+result.insertId+"',lang_code  = '"+lanData[i]+"', question  = '"+config.mysql_real_escape_string(req.body['quesn_'+lanData[i]])+"', answer  = '"+config.mysql_real_escape_string(req.body['ans_'+lanData[i]])+"'";

                           db_connection.con.query(pqry);
                       }

	           data["msgcode"] = 1;
	           data["msgtext"] = msg;
	           data["results"] = result;
	           res.send(data);
	           
               }
       
           }

    });
 });



router.get('/deleteFaqData/:id', function(req, res) {
	var data = {};
	var _get 		= req.params;	
	var Id       = _get['id'];
	//console.log(req.query);
	var query="DELETE FROM faq WHERE faq.id = '"+Id+"'";;
	db_connection.con.query(query, function (err, result) {
		if(!!err){
			data["msgcode"] = 0;
			data["msgtext"] = 'Error Msg Text';			
			data["results"] = err;
	   		res.send(data);
		}else{
			data["msgcode"] = 1;
			data["msgtext"] = 'Success Msg Text';			
			data["results"] = result;
			res.send(data);
		}
	});
});



// =========================List of All sub admin List=====================================//

router.get('/getAllFaqList/', function(req, res) {
	var data = {};

var query="SELECT faq.id,faq.status,faq_lang.question,faq_lang.answer,faq_lang.lang_code FROM faq Right JOIN faq_lang on faq.id=faq_lang.faq_id where faq_lang.lang_code='en' ORDER BY faq.updated_at DESC";

	db_connection.con.query(query, function (err, result) {
		if(!!err){
			data["msgcode"] = 0;
			data["msgtext"] = 'Error Msg Text';			
			data["results"] = err;
	   		res.send(data);
		}else{
			data["msgcode"] = 1;
			data["msgtext"] = 'Success Msg Text';			
			data["results"] = result;

			res.send(data);
		}
	});
});



router.get('/getFaqData/:id', function(req, res) {
	var data = {};
	var _get 		= req.params;	
	var id       = _get['id'];	
	var query="SELECT * FROM faq INNER JOIN faq_lang on faq.id=faq_lang.faq_id where faq.id='"+id+"'";
	db_connection.con.query(query, function (err, result) {
		if(!!err){
			data["msgcode"] = 0;
			data["msgtext"] = 'Error Msg Text';			
			data["results"] = err;
	   		res.send(data);
		}else{
			data["msgcode"] = 1;
			data["msgtext"] = 'Success Msg Text';			
			data["results"] = result;

			//console.log(data);
			res.send(data);
		}
	});
});

module.exports = router;

