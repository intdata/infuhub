var config = require('../config/config');
var express = require('express');
var dateFormat = require('dateformat');
var router = express.Router();
var db_connection = require('../config/db_connection');
influencer_admin_transaction = require("./../../models/influencer_admin_transaction");
//**=============== Sequalize init =============== */
Sequelize = require("sequelize");
Op = Sequelize.Op;

//CampaignAssign = require("./../../models/campaign_assign");
//var campaignObj = campaign(sequelize, Sequelize);


router.post('/releaseManualPayment', function (req, res) {
	var data = {};
	console.log(req.body);
	let manual_payout_status = 'SUCCESS';
	let payout_status = 'SUCCESS';
	let influencerId = req.body.influencerId;

	query = "UPDATE influencer_admin_transaction SET manual_payout_status = '" +
		manual_payout_status + "', payout_status = '" +
		payout_status + "', created_at = '" + dateFormat(req.body.paymentRDate, 'yyyy-mm-dd HH:MM:ss') + "',manual_payout_on = '" + dateFormat(req.body.paymentRDate, 'yyyy-mm-dd HH:MM:ss') + "' WHERE id = '" + req.body.paymentId + "' ";
	console.log(query);
	db_connection.con.query(query, function (err, result) {
		if (!!err) {
			data["msgcode"] = 0;
			data["msgtext"] = 'Error Msg Text';
			data["results"] = err;
			res.send(data);
		} else {
			data["msgcode"] = 1;
			data["msgtext"] = 'Success Msg Text';
			data["results"] = result;
			//update total_earning in influencer
			var query = "SELECT net_amount FROM influencer_admin_transaction where id='" + req.body.paymentId + "'";
			db_connection.con.query(query, function (err, result1) {
				if (!!err) {
					data["msgcode"] = 0;
					data["msgtext"] = 'Error Msg Text';
					data["results"] = err;
					res.send(data);
				} else {
					//console.log(result[0].net_amount);return;
					
					var netAmount = result1[0].net_amount;
					var qry = "UPDATE influencer SET total_earning = total_earning + " + netAmount + "  WHERE id = '" + influencerId + "'";
					db_connection.con.query(qry, function (er, reslt) {
						if (er) {
							data["msgcode"] = 0;
							data["msgtext"] = 'Error';
							data["results"] = er;
							res.send(data);
						} else {
							data["msgcode"] = 1;
							data["msgtext"] = 'Success Msg Text';
							data["results"] = result[0];
							res.send(data);
						
						}
					});

				}
			
			});
			
		}
	});
});


/*router.get('/deleteFaqData/:id', function (req, res) {
	var data = {};
	var _get = req.params;
	var Id = _get['id'];
	//console.log(req.query);
	var query = "DELETE FROM faq WHERE faq.id = '" + Id + "'";;
	db_connection.con.query(query, function (err, result) {
		if (!!err) {
			data["msgcode"] = 0;
			data["msgtext"] = 'Error Msg Text';
			data["results"] = err;
			res.send(data);
		} else {
			data["msgcode"] = 1;
			data["msgtext"] = 'Success Msg Text';
			data["results"] = result;
			res.send(data);
		}
	});
});
*/

// =========================List of All sub admin List=====================================//
/*router.get('/getSingleData/:id', function (req, res) {
	var data = {};
	var _get = req.params;
	var id = _get['id'];
	var query = "SELECT * FROM campaign where id='" + id + "'";
	db_connection.con.query(query, function (err, result) {
		if (!!err) {
			data["msgcode"] = 0;
			data["msgtext"] = 'Error Msg Text';
			data["results"] = err;
			res.send(data);
		} else {
			var influencerData = "SELECT CA.status, I.first_name, I.last_name, I.picture  FROM campaign_assign AS CA LEFT JOIN influencer AS I ON CA.influencer_id = I.id WHERE CA.campaign_id = " + id;
			db_connection.con.query(influencerData, function (er, rslt) {
				data["msgcode"] = 1;
				data["msgtext"] = 'Success Msg Text';
				data["results"] = result[0];
				data["influencerResult"] = rslt;
				res.send(data);
			});

		}
	});
});*/

router.get('/getAllData/:id?', function (req, res) {
	var data = {};
	var _get = req.params;
	var id = _get['id'];
	if (id == '') {
		data["msgcode"] = 0;
		data["msgtext"] = 'Error Msg Text';
		data["results"] = 'err';
		res.send(data);
	}
	var query = "SELECT C.campaign_name,C.service_type,C.start_date,C.end_date,IAT.id as IAT_ID,IAT.*,BOT.id AS BOT_ID,CA.id as CAMP_ASSIGN_ID,CA.influencer_id,POP.id AS PROP_ID FROM `brand_owner_admin_transaction` AS BOT JOIN campaign_assign AS CA on BOT.campaign_assign_id=CA.id JOIN campaign AS C ON CA.campaign_id = C.id JOIN proposals AS POP on CA.id = POP.invitation_id JOIN influencer_admin_transaction AS IAT on BOT.id = IAT.brand_owner_admin_transaction_id WHERE CA.influencer_id = " + id;
	//console.log(query);return;
	// //var data  = campaignObj.findAll();
	// campaignObj.findAll().success(function(match) {
	// 	console.log(match)
	// 	res.send(match);
	// });
	// //console.log(data)
	db_connection.con.query(query, function (err, result) {
		if (!!err) {
			data["msgcode"] = 0;
			data["msgtext"] = 'Error Msg Text';
			data["results"] = 'Error';
			res.send(data);
		} else {
			data["msgcode"] = 1;
			data["msgtext"] = 'Success Msg Text';
			data["results"] = result;
			res.send(data);
		}
	});
});


router.post('/getAllExportData', function (req, res) {
	var data = {};
	var id = req.body.id;
	var service_type = req.body.service_type;
	var payment_date = req.body.payment_date;
	var where = " WHERE CA.influencer_id = " + id
	if (service_type != '') {

		where = where + " AND C.service_type LIKE '%" + service_type + "%'";
	}
	if (payment_date != '') {

		where = where + " AND DATE(IAT.created_at) = " + "'" + payment_date + "'";
	}
	var query = "SELECT C.campaign_name,C.service_type,C.start_date,C.end_date,IAT.id as IAT_ID,IAT.*,BOT.id AS BOT_ID,CA.id as CAMP_ASSIGN_ID,CA.influencer_id,POP.id AS PROP_ID FROM `brand_owner_admin_transaction` AS BOT  JOIN campaign_assign AS CA on BOT.campaign_assign_id=CA.id JOIN campaign AS C ON CA.campaign_id = C.id JOIN proposals AS POP on CA.id = POP.invitation_id JOIN influencer_admin_transaction AS IAT on BOT.id = IAT.brand_owner_admin_transaction_id" + where;
	//console.log(query);return;
	// //var data  = campaignObj.findAll();
	// campaignObj.findAll().success(function(match) {
	// 	console.log(match)
	// 	res.send(match);
	// });
	// //console.log(data)
	db_connection.con.query(query, function (err, result) {
		if (!!err) {
			data["msgcode"] = 0;
			data["msgtext"] = 'Error Msg Text';
			data["results"] = 'Error';
			res.send(data);
		} else {
			data["msgcode"] = 1;
			data["msgtext"] = 'Success Msg Text';
			data["results"] = result;
			res.send(data);
		}
	});
});
/*router.get('/statusChange/:id', function (req, res) {
	var data = {};
	var _get = req.params;
	var id = _get['id'];
	var qry = "UPDATE campaign set status = 'Approved' where id='" + id + "'";
	db_connection.con.query(qry, function (er, reslt) {
		if (er) {
			data["msgcode"] = 0;
			data["msgtext"] = 'Error';
			data["results"] = er;
			res.send(data);
		} else {
			var query = "SELECT * FROM campaign ";
			db_connection.con.query(query, function (err, result) {
				if (!!err) {
					data["msgcode"] = 0;
					data["msgtext"] = 'Error Msg Text';
					data["results"] = err;
					res.send(data);
				} else {
					data["msgcode"] = 1;
					data["msgtext"] = 'Success Msg Text';
					data["results"] = result;
					res.send(data);
				}
			});
		}
	});
});*/




module.exports = router;

