var config = require('../config/config');
var express = require('express');
var router = express.Router();
var db_connection = require('../config/db_connection');


router.post('/addEditEmailTemplate/', function (req, res) {

	var data = {};
	var query = '';
	var msg = '';
	var qry = '';
	var pqry = '';
	var extraquery = '';

	var id = req.body.id;
	
	var today = new Date();
	var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
	var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
	var dateTime = date + ' ' + time;

	if (id > 0) {
		query = " UPDATE ";

		extraquery = ", updated_at  = '" + dateTime + "'";

		msg = 'Record updated successfully';
	} else {
		query = " INSERT INTO ";
		msg = 'Record inserted successfully';
		extraquery = ", updated_at  = '" + dateTime + "', created_at   = '" + dateTime + "'";
	}

	query += "email_template SET template_subject = '" + req.body.subject + "', template_content  = '" + req.body.description + "', from_name  = '" + req.body.from_name + "', from_email  = '" + req.body.from_email + "', status = 'Active'" + extraquery;

	if (id > 0) {
		query += " WHERE id=" + id;
	}

	db_connection.con.query(query, function (err, result) {
		if (!!err) {
			data["msgcode"] = 0;
			data["msgtext"] = 'Error Msg Text';
			data["results"] = err;
			res.send(data);
		} else {
			data["msgcode"] = 1;
			data["msgtext"] = 'Success Msg Text';
			data["results"] = result;
			res.send(data);
		}
	});

	
});



router.get('/deleteEmailTemplate/:id', function (req, res) {
	var data = {};
	var _get = req.params;
	var Id = _get['id'];
	//console.log(req.query);
	var query = "DELETE FROM email_template WHERE id = '" + Id + "'";;
	db_connection.con.query(query, function (err, result) {
		if (!!err) {
			data["msgcode"] = 0;
			data["msgtext"] = 'Error Msg Text';
			data["results"] = err;
			res.send(data);
		} else {
			data["msgcode"] = 1;
			data["msgtext"] = 'Success Msg Text';
			data["results"] = result;
			res.send(data);
		}
	});
});



// =========================List of All sub admin List=====================================//

router.get('/getAllEmailTemplateData/', function (req, res) {
	var data = {};

	var query = "SELECT id,template_name,template_subject,template_content,from_name,from_email,status, created_at,updated_at,created_by,updated_by  FROM email_template ORDER BY  updated_at DESC";

	db_connection.con.query(query, function (err, result) {
		if (!!err) {
			data["msgcode"] = 0;
			data["msgtext"] = 'Error Msg Text';
			data["results"] = err;
			res.send(data);
		} else {
			data["msgcode"] = 1;
			data["msgtext"] = 'Success Msg Text';
			data["results"] = result;

			res.send(data);
		}
	});
});



router.get('/getEditData/:id', function (req, res) {
	var data = {};
	var _get = req.params;
	var id = _get['id'];
	//console.log(req.query);
	var query = "SELECT id,template_name,template_subject,template_content,from_name,from_email,status FROM email_template  where id='" + id + "'";
	db_connection.con.query(query, function (err, result) {
		if (!!err) {
			data["msgcode"] = 0;
			data["msgtext"] = 'Error Msg Text';
			data["results"] = err;
			res.send(data);
		} else {
			data["msgcode"] = 1;
			data["msgtext"] = 'Success Msg Text';
			data["results"] = result;

			//console.log(data);
			res.send(data);
		}
	});
});

router.post('/ChangeStatus/', function (req, res) {
	var data = {};
	//================================Edit Brand Owner==============================// 
	
	var id=req.body.id;
	var current_status=req.body.status;
	var status;
	if(current_status=='Active')
	{
		status='Inactive'
	}else{

		status='Active'  

	}

	var query = "UPDATE email_template SET status ='" +status+ "' WHERE id='" + req.body.id + "'";
	db_connection.con.query(query, function (err, result) {

		if (!!err) {
			data["msgcode"] = 0;
			data["msgtext"] = 'Error Msg Text';
			data["results"] = err;

			res.send(data);
		} else {
			data["msgcode"] = 1;
			data["msgtext"] = 'Success Msg Text';
			data["results"] = result;

			res.send(data);
		}
	});

});


module.exports = router;

