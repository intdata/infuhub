var config = require('../config/config');
var express = require('express');
var router = express.Router();
var db_connection = require('../config/db_connection');


router.post('/addeditCmsData/', function (req, res) {
	var data = {};
	var query = '';
	var msg = '';
	var qry = '';
	var pqry = '';
	var extraquery = '';

	var id = req.body.id;
	var lanData = ['en', 'ar'];

	var slag = config.slugify(req.body.title_en);
	var today = new Date();
	var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
	var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
	var dateTime = date + ' ' + time;

	if (id > 0) {
		query = " UPDATE ";

		extraquery = ", updated_at  = '" + dateTime + "'";

		msg = 'Record updated successfully';
	} else {
		query = " INSERT INTO ";
		msg = 'Record inserted successfully';
		extraquery = ", updated_at  = '" + dateTime + "', created_at   = '" + dateTime + "'";
	}

	query += "cms SET slug  = '" + slag + "', meta_title = '" + req.body.meta_title + "', meta_tag  = '" + req.body.meta_tag + "', meta_description = '" + req.body.meta_description + "', status = '" + req.body.status + "'" + extraquery;



	if (id > 0) {
		query += " WHERE id=" + id;
	}

	db_connection.con.query(query, function (err, result) {
		if (!!err) {

			data["msgcode"] = 0;
			data["msgtext"] = 'Error';
			data["results"] = err;
			res.send(data);

		} else {

			if (id > 0) {

				qry = "DELETE FROM cms_lang WHERE cms_lang.page_id = '" + id + "'";
				db_connection.con.query(qry, function (er, rs) {

					for (var i = 0; i < lanData.length; i++) {

						pqry = "INSERT INTO cms_lang SET page_id = " + id + ",lang_code  = '" + lanData[i] + "', title  = '" + req.body['title_' + lanData[i]] + "', description  = '" + req.body['description_' + lanData[i]] + "',short_description='" + req.body['Short_desc_' + lanData[i]] + "'";

						db_connection.con.query(pqry);

					}

				});

				data["msgcode"] = 1;
				data["msgtext"] = msg;
				data["results"] = result;
				res.send(data);

			} else {

				for (var i = 0; i < lanData.length; i++) {

					pqry = "INSERT INTO cms_lang SET page_id = " + result.insertId + ",lang_code  = '" + lanData[i] + "', title  = '" + req.body['title_' + lanData[i]] + "', description  = '" + req.body['description_' + lanData[i]] + "',short_description='" + req.body['Short_desc_' + lanData[i]] + "'";

					db_connection.con.query(pqry);
				}

				data["msgcode"] = 1;
				data["msgtext"] = msg;
				data["results"] = result;
				res.send(data);

			}

		}

	});
});



router.get('/deleteCmsData/:id', function (req, res) {
	var data = {};
	var _get = req.params;
	var Id = _get['id'];
	//console.log(req.query);
	var query = "DELETE FROM cms WHERE cms.id = '" + Id + "'";;
	db_connection.con.query(query, function (err, result) {
		if (!!err) {
			data["msgcode"] = 0;
			data["msgtext"] = 'Error Msg Text';
			data["results"] = err;
			res.send(data);
		} else {
			data["msgcode"] = 1;
			data["msgtext"] = 'Success Msg Text';
			data["results"] = result;
			res.send(data);
		}
	});
});



// =========================List of All sub admin List=====================================//

router.get('/getAllCmsList/', function (req, res) {
	var data = {};

	var query = "SELECT cms.id,cms.slug,cms.status,cms_lang.title,cms_lang.lang_code FROM cms Right JOIN cms_lang on cms.id=cms_lang.page_id where cms_lang.lang_code='en' ORDER BY cms.updated_at DESC";

	db_connection.con.query(query, function (err, result) {
		if (!!err) {
			data["msgcode"] = 0;
			data["msgtext"] = 'Error Msg Text';
			data["results"] = err;
			res.send(data);
		} else {
			data["msgcode"] = 1;
			data["msgtext"] = 'Success Msg Text';
			data["results"] = result;

			res.send(data);
		}
	});
});



router.get('/getEditData/:id', function (req, res) {
	var data = {};
	var _get = req.params;
	var id = _get['id'];
	//console.log(req.query);
	var query = "SELECT * FROM cms INNER JOIN cms_lang on cms.id=cms_lang.page_id where cms.id='" + id + "'";
	db_connection.con.query(query, function (err, result) {
		if (!!err) {
			data["msgcode"] = 0;
			data["msgtext"] = 'Error Msg Text';
			data["results"] = err;
			res.send(data);
		} else {
			data["msgcode"] = 1;
			data["msgtext"] = 'Success Msg Text';
			data["results"] = result;

			//console.log(data);
			res.send(data);
		}
	});
});



module.exports = router;

