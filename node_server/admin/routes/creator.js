var config = require('../config/config');
var express = require('express');
var router = express.Router();
var db_connection = require('../config/db_connection');
var crypto = require('crypto');
var path = require("path");
const multer = require('multer');
var im = require('imagemagick');
var fs = require('fs');

//**=============== Sequalize init =============== */
Sequelize = require("sequelize");
Op = Sequelize.Op;
Influencer = require("./../../models/influencer");

var influencerObj = Influencer(sequelize, Sequelize);
//**=============== Sequalize init =============== */

const DIR = "./public/assets/uploads/influencer";
const IMG_DIR = "./../../public/assets/uploads/influencer";
const DIR_THUMB = "./../../public/assets/uploads/influencer/thumb";


var productImage;
let storage = multer.diskStorage({
	destination: (req, file, cb) => {
		cb(null, DIR);
	},
	filename: (req, file, cb) => {
		productImage = file.originalname;
		//console.log(file.originalname);
		cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname));
	}
});
let upload = multer({ storage: storage });

router.post('/saveCreator/', upload.single('image'), function (req, res) {
	console.log(req.body);
	var data = {};
	//====================Date Time============================================//
	var today = new Date();
	var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
	var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
	var dateTime = date + ' ' + time;
	//================Password Hashing=================================//
	var password = req.body.password;
	var hashPassword = crypto.createHash('md5').update(password).digest('hex');
	//================================Create Sub Admin==============================//  


	if (req.file == undefined)
		var imgName = '';
	else
		var imgName = req.file.filename;
	fs.readFile(req.file.path, function (err, data) {
		//var imgName = req.file.image.name
		/// If there's an error
		if (!imgName) {
			console.log("There was an error")
			res.redirect("/");
			res.end();
		} else {
			var newPath = __dirname + "/" + IMG_DIR + "/" + imgName;
			var thumbPath = __dirname + "/" + DIR_THUMB + "/" + imgName;
			fs.writeFile(newPath, data, function (err) {
				// write file to uploads/thumbs folder
				im.resize({
					srcPath: newPath,
					dstPath: thumbPath,
					width: 200
				}, function (err, stdout, stderr) {
					if (err) throw err;
					console.log('resized image to fit within 200x200px');
				});

			});
		}
	});
	var query = "INSERT INTO influencer (platform_id,first_name,last_name,description,email,password,company_name,contact_number,picture,status,created_at,updated_at) VALUES ('" + req.body.platform + "','" + req.body.fname + "','" + req.body.lname + "','" + req.body.description + "','" + req.body.email + "','" + hashPassword + "','" + req.body.company_name + "','" + req.body.contact_number + "','" + imgName + "','" + req.body.status + "','" + dateTime + "','" + dateTime + "')";
	db_connection.con.query(query, function (err, result) {
		if (!!err) {
			data["msgcode"] = 0;
			data["msgtext"] = 'Error Msg Text';
			data["results"] = err;
			res.send(data);
		} else {
			data["msgcode"] = 1;
			data["msgtext"] = 'Success Msg Text';
			data["results"] = result;
			res.send(data);
		}
	});
});
router.post('/editCreator/', upload.single('image'), function (req, res) {
	var data = {};
	//====================Date Time============================================//
	var today = new Date();
	var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
	var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
	var dateTime = date + ' ' + time;
	//================================Edit Sub Admin==============================//  
	if (req.file == undefined)
		var imgName = '';
	else {
		var imgName = req.file.filename;
		fs.readFile(req.file.path, function (err, data) {
			var newPath = __dirname + "/" + IMG_DIR + "/" + imgName;
			var thumbPath = __dirname + "/" + DIR_THUMB + "/" + imgName;
			fs.writeFile(newPath, data, function (err) {
				// write file to uploads/thumbs folder
				im.resize({
					srcPath: newPath,
					dstPath: thumbPath,
					width: 200
				}, function (err, stdout, stderr) {
					if (err) throw err;
					console.log('resized image to fit within 200x200px');
				});
			});
		});
	}
	var query = "UPDATE influencer SET platform_id='" + req.body.platform + "',first_name='" + req.body.fname + "',last_name='" + req.body.lname + "',description='"+config.mysql_real_escape_string(req.body.description)+"',email='" + req.body.email + "',company_name='" + req.body.company_name + "',contact_number='" + req.body.contact_number + "',status='" + req.body.status + "', updated_at='" + dateTime + "'";

	if (imgName != '') {
		query += ", picture='" + imgName + "'";
	}
	query += " WHERE id='" + req.body.id + "'";
	console.log(query);
	db_connection.con.query(query, function (err, result) {
		if (!!err) {
			data["msgcode"] = 0;
			data["msgtext"] = 'Error Msg Text';
			data["results"] = err;
			res.send(data);
		} else {
			data["msgcode"] = 1;
			data["msgtext"] = 'Success Msg Text';
			data["results"] = result;
			res.send(data);
		}
	});
});
router.get('/deleteCreator/:id', function (req, res) {
	var data = {};
	var _get = req.params;
	var UserId = _get['id'];
	//console.log(req.query);
	var query = "DELETE FROM influencer WHERE influencer.id = '" + UserId + "'";
	db_connection.con.query(query, function (err, result) {
		if (!!err) {
			data["msgcode"] = 0;
			data["msgtext"] = 'Error Msg Text';
			data["results"] = err;
			res.send(data);
		} else {
			data["msgcode"] = 1;
			data["msgtext"] = 'Success Msg Text';
			data["results"] = result;
			res.send(data);
		}
	});
});
// =========================List of All sub admin List=====================================//
router.get('/getAllCreatorData/', function (req, res) {
	
	var data = {};
	let uploadPath = config.upload_path;
	let noProfileImage = config.no_profile_image;
	// influencer list fetch by Sequalize ORM
	influencerObj.findAll({
		attributes: [
			'id',
			'platform_id',
			'email',
			'first_name',
			'last_name',
			'total_earning',
			//'company_name',
			'contact_number',
			[sequelize.literal("IF(picture = '', '" +
			noProfileImage +
			"', CONCAT('" +
			uploadPath +
			"influencer/thumb/', picture))"), 'picture'],
			'is_email_verified',
			'status',
			'created_at'
		]}).then(result =>{ 
			data = {
				msgcode: 1,
				msgtext: "Success Msg Text",
				results: result
			}
			res.json(data);
		}).catch(error => {
			// Ooops, do some error-handling
			console.log("Something went worng!");
			console.log(error);
			data = {
				msgcode: 0,
				msgtext: "Error Msg Text",
				results: error
			}
			res.json(data);
		});

	// var data = {};
	// var query = "SELECT id,platform_id,email,first_name,last_name,company_name,contact_number,picture,status,created_at FROM influencer";
	// //console.log(query);
	// db_connection.con.query(query, function (err, result) {
	// 	if (!!err) {
	// 		data["msgcode"] = 0;
	// 		data["msgtext"] = 'Error Msg Text';
	// 		data["results"] = err;
	// 		res.send(data);
	// 	} else {
	// 		data["msgcode"] = 1;
	// 		data["msgtext"] = 'Success Msg Text';
	// 		data["results"] = result;
	// 		res.send(data);
	// 	}
	// });
});
router.get('/getEditData/:id', function (req, res) {
	var data = {};
	var _get = req.params;
	var UserId = _get['id'];
	//console.log(req.query);
	let uploadPath = config.upload_path;
	let noProfileImage = config.no_profile_image;
	var query = "SELECT id,platform_id,description,email,first_name,last_name,company_name,contact_number,IF(picture = '', '" +
	noProfileImage +
	"', CONCAT('" +
	uploadPath +
	"influencer/thumb/', picture)) as picture,status,created_at FROM influencer WHERE id='" + UserId + "'";
	db_connection.con.query(query, function (err, result) {
		if (!!err) {
			data["msgcode"] = 0;
			data["msgtext"] = 'Error Msg Text';
			data["results"] = err;
			res.send(data);
		} else {
			data["msgcode"] = 1;
			data["msgtext"] = 'Success Msg Text';
			data["results"] = result;
			res.send(data);
		}
	});
});

router.post('/ChangeStatus/', function (req, res) {
	var data = {};
	console.log('ok');
	//================================Edit Brand Owner==============================// 

	var id = req.body.id;
	var current_status = req.body.status;
	var status;
	if (current_status == 'Active') {
		status = 'Inactive'
	} else {

		status = 'Active'

	}
	var query = "UPDATE influencer SET status ='" + status + "' WHERE id='" + req.body.id + "'";

	db_connection.con.query(query, function (err, result) {

		if (!!err) {
			data["msgcode"] = 0;
			data["msgtext"] = 'Error Msg Text';
			data["results"] = err;

			res.send(data);
		} else {
			data["msgcode"] = 1;
			data["msgtext"] = 'Success Msg Text';
			data["results"] = result;

			res.send(data);
		}
	});

});

module.exports = router;
