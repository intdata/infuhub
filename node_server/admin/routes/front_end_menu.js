var config = require('../config/config');
var common_function = require('./function');
var express = require('express');
var router = express.Router();
var db_connection = require('../config/db_connection');

router.get('/menuList/', function (req, res) {
	var data = {};
	var query = "SELECT * FROM front_end_menu_master ";

	
	common_function.get_menu_tree1(0, function (response) {
		console.log(response);
	});

	db_connection.con.query(query, function (err, result) {
		if (!!err) {
			data["msgcode"] = 0;
			data["msgtext"] = 'Error';
			data["results"] = err;
			res.send(data);
		}
		else {
			data["msgcode"] = 1;
			data["msgtext"] = 'Success';
			data["results"] = result;
			res.send(data);
		}
	});
});

router.post('/addFrontMenuData/', function (req, res) {
	var data = {};
	var query = '';
	var msg = '';
	var qry = '';
	var pqry = '';
	var id = req.body.id;
	if (id > 0) {
		query = " UPDATE ";
		msg = 'Record updated successfully';
	}
	else {
		query = " INSERT INTO ";
		msg = 'Record inserted successfully';
	}

	if (req.body.parent_id == "") {
		parent_id = 0;
	} else {
		parent_id = req.body.parent_id;
	}

	if (req.body.link_type == '1') {
		menu_link_val = req.body.menu_link;
	} else {
		menu_link_val = req.body.page_list_name;
	}

	if (id == 0) {
		var slug = "";

		common_function.check_duplicate_slug(req.body.menu_name, function (response) {

			slug = response;

			query += "front_end_menu_master SET menu_position = '" + req.body.menu_position + "', menu_label  = '" + req.body.menu_name + "',slug='" + slug + "',menu_link   = '" + menu_link_val + "',menu_link_type= '" + req.body.link_type + "', parent_id  = '" + req.body.parent_id + "',menu_order = '" + req.body.menu_order + "',menu_icon = '" + req.body.menu_icon_class + "' , status = '" + req.body.status + "'";

			db_connection.con.query(query, function (err, result) {
				if (!!err) {
					data["msgcode"] = 0;
					data["msgtext"] = 'Error';
					data["results"] = err;
					res.send(data);
				}
				else {
		
					data["msgcode"] = 1;
					data["msgtext"] = msg;
					data["results"] = result;
					res.send(data);
				}
			});

		});
	} else {

		if (id > 0) {
			
			query += "front_end_menu_master SET menu_position = '" + req.body.menu_position + "', menu_label  = '" + req.body.menu_name + "',menu_link   = '" + menu_link_val + "',menu_link_type= '" + req.body.link_type + "', parent_id  = '" + req.body.parent_id + "',menu_order = '" + req.body.menu_order + "',menu_icon = '" + req.body.menu_icon_class + "' , status = '" + req.body.status + "'";

			query += " WHERE id=" + id;

            db_connection.con.query(query, function (err, result) {
				if (!!err) {
					data["msgcode"] = 0;
					data["msgtext"] = 'Error';
					data["results"] = err;
					res.send(data);
				}
				else {
		
					data["msgcode"] = 1;
					data["msgtext"] = msg;
					data["results"] = result;
					res.send(data);
				}
			});
               

		}

	}
});

router.get('/singleMenu/:id', function (req, res) {
	var data = {};
	var id = req.params.id;
	var query = "SELECT *FROM front_end_menu_master where id = " + id;
	db_connection.con.query(query, function (err, result) {
		if (!!err) {
			data["msgcode"] = 0;
			data["msgtext"] = 'Error';
			data["results"] = err;
			res.send(data);
		}
		else {
			data["msgcode"] = 1;
			data["msgtext"] = 'Success';
			data["results"] = result[0];
			res.send(data);
		}
	});
});

router.get('/getPageList/', function (req, res) {
	var data = {};
	var _get = req.params;

	var query = "SELECT cms.slug,cms_lang.title FROM cms INNER JOIN cms_lang on cms.id=cms_lang.page_id where cms_lang.lang_code='en'";
	db_connection.con.query(query, function (err, result) {
		if (!!err) {
			data["msgcode"] = 0;
			data["msgtext"] = 'Error Msg Text';
			data["results"] = err;
			res.send(data);
		} else {
			data["msgcode"] = 1;
			data["msgtext"] = 'Success Msg Text';
			data["results"] = result;
			res.send(data);
		}
	});
});

router.get('/getFrontParentMenuList/', function (req, res) {
	var data = {};
	category = {};
	var _get = req.params;

	var query = "SELECT * FROM front_end_menu_master WHERE parent_id=0";
	 db_connection.con.query(query, function (err, result) {	
		


		if (!!err) {
			data["msgcode"] = 0;
			data["msgtext"] = 'Error Msg Text';
			data["results"] = err;
			res.send(data);
		} else {
			data["msgcode"] = 1;
			data["msgtext"] = 'Success Msg Text';
			data["results"] = result;
			res.send(data);
		}
	});
 });

router.get('/menuTree/', function(req, res){
    var data = {};
    var newData = {};       
    var testData = [];
    var inc = 0; 
    var mydbdata;
    var menu_id;
    var query = "SELECT * FROM front_end_menu_master WHERE parent_id=0";
    db_connection.con.query(query, function(err, result) {     

        if (!!err) {
            data["msgcode"] = 0;
            data["msgtext"] = 'Error';
            data["results"] = err;
        }
        else {
            if(result.length>0)
            {
                for(i=0;i<result.length; i++)
                {
                    menu_id = result[i].id;
                    menu_label = result[i].menu_label;                
                    (function(menu_id,menu_label){                    
                        var qry = "SELECT * FROM front_end_menu_master where parent_id = "+menu_id;
                        db_connection.con.query(qry, function(err, resl) {                            
                           menudata(result.length,resl,menu_label,i);                            
                        });
                    })(menu_id,menu_label);                                      
                }
            }            
        }
    });
       
    
    function menudata(length,resl,groupname,i){
         mydata = {};
         mydata.parentMenuName = {};
         mydata.menuList = {};

        inc++;
        mydata.parentMenuName= groupname; 
        mydata.menuList=  resl;  
        newData[inc] =mydata;       
        if(length == inc){
            data["msgcode"] = 1;
            data["msgtext"] = 'Success';
            data["results"] = newData;           
            res.send(data);
        }
    }

});



router.get('/deleteMenu/:id', function (req, res) {
	var data = {};
	var _get = req.params;
	var Id = _get['id'];
	var query = "DELETE FROM  front_end_menu_master WHERE  front_end_menu_master.id = '" + Id + "'";
	console.log(query);
	db_connection.con.query(query, function (err, result) {
		if (!!err) {
			data["msgcode"] = 0;
			data["msgtext"] = 'Error Msg Text';
			data["results"] = err;
			res.send(data);
		} else {
			data["msgcode"] = 1;
			data["msgtext"] = 'Success Msg Text';
			data["results"] = result;
			res.send(data);
		}
	});
});


router.get('/getdata', function(req, res){
     var data= {};

    for(let i =0; i< 10; i++){
        exports.testfunction(i).then(function(result){
            console.log(result);
            if(i == 9){
                res.json(result);
            }
        })
    }
 })

 
 exports.testfunction = function(i){

    return new Promise(function (resolve, reject) {

        setTimeout(function () {
            i = i+1;
            resolve(i);
          }, 8000);

        
     });
}

module.exports = router;

