var config  = require('../config/config');
var express = require('express');
var router = express.Router();
var db_connection  = require('../config/db_connection');
var crypto = require('crypto');

//===============check Email Id Exit Or Not========================================//
router.post('/checkEmailExitsOrNot/', function (req, res) {
	var data = {};
	var custParams = "";
	var TableName = "";
	if (req.body.id > 0) {
		custParams = ' AND id!=' + req.body.id;
	}

	TableName = req.body.tablename;
	var query = "SELECT email FROM "+ TableName +" WHERE email='" + req.body.email + "'" + custParams;
	db_connection.con.query(query, function (err, result) {
		if (!!err) {

			data["msgcode"] = 0;
			data["msgtext"] = 'Error Msg Text';
			data["results"] = err;

		} else {

			if (result.length > 0) {
				data["statusCode"] = 400;
				data["error"] = 'Bad Request';
				data["message"] = 'Email address already registerd';

				res.status(400).send(data);
			} else {
				res.status(200).send('null')
			}

			//console.log(data);            
		}
	});

});

router.get('/getAllLanguage/', function (req, res) {
	var data = {};
	var query = "SELECT * FROM language WHERE status= 'Active'";
	db_connection.con.query(query, function (err, result) {
		if (!!err) {

			data["msgcode"] = 0;
			data["msgtext"] = 'Error Msg Text';
			data["results"] = err;

		} else {
	
			data["msgcode"] = 1;
			data["msgtext"] = 'Success message text';
			data["results"] = result;    
		}

		res.send(data);
	});

});

router.get('/getAllCurrencies/', function (req, res) {
	var data = {};
	var query = "SELECT * FROM currency order by name asc";
	db_connection.con.query(query, function (err, result) {
		if (!!err) {

			data["msgcode"] = 0;
			data["msgtext"] = 'Error Msg Text';
			data["results"] = err;

		} else {
	
			data["msgcode"] = 1;
			data["msgtext"] = 'Success message text';
			data["results"] = result;    
		}

		res.send(data);
	});

});
 
module.exports = router;
