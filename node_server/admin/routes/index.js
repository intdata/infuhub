var express = require('express');
var router = express.Router();

//**=============== Sequalize init =============== */
Sequelize = require("sequelize");
Op = Sequelize.Op;
Countries = require("./../../models/countries");
Industries = require("./../../models/industries");

var countriesObj = Countries(sequelize, Sequelize);
var industriesObj = Industries(sequelize, Sequelize);
//**=============== Sequalize init =============== */

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express powered by Plesk' });
});

/**
 * Method to get all active country list
 */
router.get('/get-countries', function(req, res, next) {
  return countriesObj.findAll({
    where: { status: 1 }
  })
  .then(countries => { 
    return res.status(200).json({ responseCode: 200, responseDetails: 'All active country list.', countries: countries});
  })
  .catch(reason => {
    return res.status(200).json({ responseCode: 420, responseDetails: 'Something went wrong!.', "error": reason});
  });
});

/**
 * Method to get all active industry list
 */
router.get('/get-industries', function(req, res, next) {
  return industriesObj.findAll({
    where: { status: 1 }
  })
  .then(industries => { 
    return res.status(200).json({ responseCode: 200, responseDetails: 'All active industry list.', industries: industries});
  })
  .catch(reason => {
    return res.status(200).json({ responseCode: 420, responseDetails: 'Something went wrong!.', "error": reason});
  });
});

module.exports = router;
