var config = require('../config/config');
var express = require('express');
var router = express.Router();
var db_connection = require('../config/db_connection');
var crypto = require('crypto');
var path = require("path");
const multer = require('multer');
var im = require('imagemagick');
var fs = require('fs');
var moment = require('moment');
var cryptoRandomString = require('crypto-random-string');

const DIR = "./public/assets/uploads/profile";
const IMG_DIR = "./../../public/assets/uploads/profile";
const DIR_THUMB = "./../../public/assets/uploads/profile/thumb";

upload = require("./../../middleware/upload");

//**=============== Sequalize init =============== */
Sequelize = require("sequelize");
Op = Sequelize.Op;
Admin = require("./../../models/admin");
EmailTemplate = require("./../../models/emailtemplate");

var adminObj = Admin(sequelize, Sequelize);
//**=============== Sequalize init =============== */

// var profileImage;
// let storage = multer.diskStorage({
//     destination: (req, file, cb) => {
//         cb(null, DIR);
//     },
//     filename: (req, file, cb) => {
//         productImage = file.originalname;
//         //console.log(file.originalname);
//         cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname));
//     }
// });
// let upload = multer({ storage: storage });


//router.post('/saveAdminUser/',upload.single('image'), function (req, res) {
router.post('/saveAdminUser/', upload.single({data: DIR, fieldName: 'image'}), function (req, res) {
    var data = {};
    // image validation
    if(req.fileValidationError) {
        data["msgcode"] = 0;
        data["msgtext"] = req.fileValidationError;
        res.send(data);
    }
    //====================Date Time============================================//
    var today = new Date();
    var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    var dateTime = date + ' ' + time;
    //================Password Hashing=================================//
    var password = req.body.password;
    var hashPassword = crypto.createHash('md5').update(password).digest('hex');
    // var hashPassword = 123456;
     var role_id = req.body.admin_role_type;
    var role_id = req.body.admin_role_type;
    //================================Create Sub Admin==============================// 
    if (req.file == undefined)
        var imgName = '';
    else
        var imgName = req.file.filename;
    fs.readFile(req.file.path, function (err, data) {
        //var imgName = req.file.image.name
        /// If there's an error
        if (!imgName) {
            console.log("There was an error")
            res.redirect("/");
            res.end();
        } else {
            var newPath = __dirname + "/" + IMG_DIR + "/" + imgName;
            var thumbPath = __dirname + "/" + DIR_THUMB + "/" + imgName;
            fs.writeFile(newPath, data, function (err) {
                // write file to uploads/thumbs folder
                im.resize({
                    srcPath: newPath,
                    dstPath: thumbPath,
                    width: 200
                }, function (err, stdout, stderr) {
                    if (err) throw err;
                    console.log('resized image to fit within 200x200px');
                });

            });
        }
    });

    var query = "INSERT INTO admin (first_name,last_name,image,role_id,email,password,status,created_at,updated_at) VALUES ('" + req.body.fname + "','" + req.body.lname + "','" + imgName + "', '" + role_id + "','" + req.body.email + "','" + hashPassword + "','" + req.body.status + "','" + dateTime + "','" + dateTime + "')";
    db_connection.con.query(query, function (err, result) {
        if (!!err) {
            data["msgcode"] = 0;
            data["msgtext"] = 'Error Msg Text';
            data["results"] = err;
            res.send(data);
        }
        else {
            qry = "SELECT * FROM admin_role_permission WHERE role_id ='" + role_id + "'";
            db_connection.con.query(qry, function (er, rs) {
                //console.log(rs);
                if (rs.length > 0) {
                    for (var i = 0; i < rs.length; i++) {
                        pqry = "INSERT INTO admin_menu_permission SET admin_menu_id = " + rs[i].menu_id + ", admin_user_id = " + result.insertId + ", permission_view = " + rs[i].permission_view + ",permission_add = " + rs[i].permission_add + ", permission_edit = " + rs[i].permission_edit + ",permission_delete = " + rs[i].permission_delete;
                        db_connection.con.query(pqry);
                    }
                }
            });

            data["msgcode"] = 1;
            data["msgtext"] = 'Success Msg Text';
            data["results"] = result;
            res.send(data);
        }
    });
});
//router.post('/editAdminUser/', upload.single('image'), function (req, res) {
router.post('/editAdminUser/', upload.single({path: DIR, fieldName: 'image'}), function (req, res) {    
    var data = {};
    // image validation
    if(req.fileValidationError) {
        data["msgcode"] = 0;
        data["msgtext"] = req.fileValidationError;
        res.send(data);
    }
    //====================Date Time============================================//
    var today = new Date();
    var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    var dateTime = date + ' ' + time;
    //================================Edit Sub Admin==============================// 
    if (req.file == undefined)
    {
        var imgName = '';
    }   
    else{
        var imgName = req.file.filename;
        fs.readFile(req.file.path, function (err, data) {
            //var imgName = req.file.image.name
            /// If there's an error
            if (!imgName) {
                console.log("There was an error")
                res.redirect("/");
                res.end();
            } else {
                var newPath = __dirname + "/" + IMG_DIR + "/" + imgName;
                var thumbPath = __dirname + "/" + DIR_THUMB + "/" + imgName;
                fs.writeFile(newPath, data, function (err) {
                    // write file to uploads/thumbs folder
                    im.resize({
                        srcPath: newPath,
                        dstPath: thumbPath,
                        width: 200
                    }, function (err, stdout, stderr) {
                        if (err) throw err;
                        console.log('resized image to fit within 200x200px');
                    });

                });
            }
        });

    }
    var query = "UPDATE admin SET first_name='" + req.body.fname + "',last_name='" + req.body.lname + "',email='" + req.body.email + "',status='" + req.body.status + "', updated_at='" + dateTime + "' ";

    if(imgName != ''){
        query += ", image='" + imgName + "'";
    }
    query += " WHERE id='" + req.body.id + "'";
    
    db_connection.con.query(query, function (err, result) {
        if (!!err) {
            data["msgcode"] = 0;
            data["msgtext"] = 'Error Msg Text';
            data["results"] = err;
            res.send(data);
        }
        else {
            data["msgcode"] = 1;
            data["msgtext"] = 'Success Msg Text';
            data["results"] = result;
            res.send(data);
        }
    });
});
router.get('/deleteAdminUser/:id', function (req, res) {
    var data = {};
    var _get = req.params;
    var adminUserId = _get['id'];
    //console.log(req.query);
    var query = "DELETE FROM admin WHERE admin.id = '" + adminUserId + "'";
    console.log(query);
    db_connection.con.query(query, function (err, result) {
        if (!!err) {
            data["msgcode"] = 0;
            data["msgtext"] = 'Error Msg Text';
            data["results"] = err;
            res.send(data);
        }
        else {
            data["msgcode"] = 1;
            data["msgtext"] = 'Success Msg Text';
            data["results"] = result;
            res.send(data);
        }
    });
});
// =========================List of All sub admin List=====================================//
router.get('/getAllAdminData/', function (req, res) {
    var data = {};
    let uploadPath = config.upload_path;
	let noProfileImage = config.no_profile_image;

    var query = "SELECT AR.admin_type,A.id,A.email,IF(image = '', '" +
	noProfileImage +
	"', CONCAT('" +
	uploadPath +
	"profile/thumb/', image)) as image,A.first_name,A.last_name,A.status,A.created_at FROM admin AS A LEFT JOIN admin_role_type AS AR ON A.role_id= AR.id  WHERE AR.admin_type!='super-admin'";
    db_connection.con.query(query, function(err, result) {
        if (!!err) {
            data["msgcode"] = 0;
            data["msgtext"] = 'Error';
            data["results"] = err;
            res.send(data);
        }
        else {
            data["msgcode"] = 1;
            data["msgtext"] = 'Success';
            data["results"] = result;
            res.send(data);
        }
    });
});
// ========================= Admin Login Start =====================================//
router.post('/login/', function (req, res) {
    var data = {};
    let uploadPath = config.upload_path;
	let noProfileImage = config.no_profile_image;
    var email = req.body.email;
    var password = req.body.password;
    var hashPassword = crypto.createHash('md5').update(password).digest('hex');
    var query = "SELECT A.id,A.email,IF(image = '', '" +
	noProfileImage +
	"', CONCAT('" +
	uploadPath +
	"profile/thumb/', image)) as image,A.first_name,A.last_name,A.status,A.created_at,AR.admin_type FROM admin AS A LEFT JOIN admin_role_type as AR  ON AR.id=A.role_id  WHERE A.email='" + email + "' and A.password = '" + hashPassword + "' and A.status = 'Active'";
    //console.log(query);
    db_connection.con.query(query, function (err, result) {
        if (!!err) {
            data["msgcode"] = 1;
            data["msgtext"] = 'Invalid Credential';
            data["results"] = err;
            res.send(data);
        }
        else {
            if (result == '') {
                data["msgcode"] = 2;
                data["msgtext"] = 'Email or password does not match';
                data["results"] = result;
            }
            else {
                data["msgcode"] = 3;
                data["msgtext"] = 'Successfully login';
                data["results"] = result[0];
                data["token"] = Date.now();
            }
            res.send(data);
        }
    });
});

/**
 * Method to send forgot mail to admin
 */
router.post('/forgotPassword/',async function (req, res) {      
    var user_email = req.body.email;

    if(!user_email){
        return res.status(200).json({
            msgcode: "201",
            msgtext: "Parameter missing.",
        });
    }

    let user = await getAdmin({ email: user_email });

    if(!user){
        return res.status(200).json({
            msgcode: "420",
            msgtext: "Something went wrong!",
        });
    }

    var token = cryptoRandomString({length: 16, type: 'url-safe'});
    
    let submitData = {
        forgot_password_token: token,
        forgot_password_requested_at: moment().format('YYYY-MM-DD HH:mm:ss')
    }
    
    // influencer list fetch by Sequalize ORM
    user
        .update(submitData)
        .then((result) => {
            // console.log(user.get({
            //   plain: true
            // }))
            if(result){
                let forgotEmailLink = config.frontEndBaseUrl + '/reset-password?email='+ user_email + '&verication_token='+ token;
                let keyValues = { 
                    FORGOT_EMAIL_LINK: forgotEmailLink,
                    USER_NAME: result.first_name
                }

                // send sign up mail
                EmailTemplate.sendMailWithTemplate('admin_forgot_password', user_email, keyValues);

                return res.json({msgcode: 200, msgtext: 'A mail has been sent to the email address. Check your inbox!'});
            } else{
                return res.json({msgcode: 200, msgtext: 'A mail has been sent to the email address. Check your inbox!'});
            }
        }).catch(error => {
            // Ooops, do some error-handling
            console.log(error);
            return res.json({msgcode: 420, msgtext: 'Something went wrong!', error: error});
        });
    }
);

/**
 * Method to reset password for admin
 */
router.post('/reset-admin-password',  async function(req, res) {
    let { newPassword, confirmPassword, email, token } = req.body;

    if(!email){
      return res.status(200).json({
            msgcode: "420",
            msgtext: "Something went wrong!",
      });
    }
  
    if (!newPassword || !confirmPassword || !email || !token) {
        return res.status(200).json({
            msgcode: "201",
            msgtext: "Parameter missing.",
        });
    } else if(newPassword  !== confirmPassword){
        return res.status(200).json({
            msgcode: "201",
            msgtext: "New password and confirm passwords must match.",
        });
    }

    let user = await getAdmin({ email: email, forgot_password_token: token });
  
    if(user){
        
        if (!user.forgot_password_requested_at || moment(user.forgot_password_requested_at).add(15, 'm') < moment()) {
            return res.status(200).json({msgcode: 200, msgtext: 'Token Expired.'});
        }
        let hashPassword = crypto.createHash('md5').update(newPassword).digest('hex');     // Password Hashing
        let submitData = {
            password: hashPassword,
            forgot_password_token: ''
        }
    
        // update password
        user
            .update(submitData)
            .then((result) => {
            if(result){
                return res.json({msgcode: 200, msgtext: 'Success! Admin password has been changed.'});
            } else{
                return res.json({msgcode: 420, msgtext: 'Something went wrong!'});
            }
            }).catch(error => {
            // Ooops, do some error-handling
            return res.json({msgcode: 420, msgtext: 'Something went wrong!', error: error});
            });
    } else{
        return res.status(200).json({
            msgcode: 404,
            msgtext: "Old Password not matched.",
        });
    }
  
});

/**
 * Method to get Influencer by given condition
 * @param {*} obj 
 */
const getAdmin = async obj => {
    return await adminObj.findOne({
        where: obj,
    });
};

// ========================= Admin Login End =====================================//
router.get('/getAdminInfo/:id', function (req, res) {
    var data = {};
    let uploadPath = config.upload_path;
	let noProfileImage = config.no_profile_image;
    var id = req.params.id;
    var query = "SELECT A.id,A.email,IF(image = '', '" +
	noProfileImage +
	"', CONCAT('" +
	uploadPath +
	"profile/thumb/', image)) as image,A.first_name,A.last_name,A.status,A.created_at,AR.admin_type FROM admin AS A LEFT JOIN admin_role_type as AR  ON AR.id=A.role_id  WHERE A.id=" + id;
    //console.log(query);
    db_connection.con.query(query, function (err, result) {
        if (!!err) {
            data["msgcode"] = 1;
            data["msgtext"] = 'Invalid Credential';
            data["results"] = err;
            res.send(data);
        }
        else {
            data["msgcode"] = 3;
            data["msgtext"] = 'Successfully display';
            data["results"] = result[0];
            res.send(data);
        }
    });
});

router.get('/getEditData/:id', function (req, res) {
    var data = {};
    let uploadPath = config.upload_path;
	let noProfileImage = config.no_profile_image;
    var _get = req.params;
    var adminUserId = _get['id'];
    //console.log(req.query);
    var query = "SELECT AR.id as roleId,A.id,A.email,IF(image = '', '" +
	noProfileImage +
	"', CONCAT('" +
	uploadPath +
	"profile/thumb/', image)) as image,A.first_name,A.last_name,A.status,A.created_at FROM admin AS A LEFT JOIN admin_role_type as AR  ON AR.id=A.role_id  WHERE A.id=" + adminUserId;
    db_connection.con.query(query, function (err, result) {
        if (!!err) {
            data["msgcode"] = 0;
            data["msgtext"] = 'Error';
            data["results"] = err;
            res.send(data);
        }
        else {
            data["msgcode"] = 1;
            data["msgtext"] = 'Success';
            data["results"] = result;
            res.send(data);
        }
    });
});

router.post('/changePassword/', function (req, res) {
    var data = {};
    var admin_id = req.body.admin_id;
    var old_password = req.body.old_password;
    var new_password = req.body.new_password;
    var old_hashPassword = crypto.createHash('md5').update(old_password).digest('hex');
    var new_hashPassword = crypto.createHash('md5').update(new_password).digest('hex');
    var query = "SELECT id FROM admin WHERE id = " + admin_id + " AND password = '" + old_hashPassword + "'";
    db_connection.con.query(query, function (err, result) {
        if (!!err) {
            data["msgcode"] = 0;
            data["msgtext"] = 'Error';
            data["results"] = err;
            res.send(data);
        }
        else {
            if (result.length > 0) {
                var qry = "UPDATE admin SET password = '" + new_hashPassword + "' WHERE id = " + admin_id;
                db_connection.con.query(qry, function (er, rs) {
                    data["msgcode"] = 1;
                    data["msgtext"] = 'Password Changed Successfully';
                    data["results"] = '';
                    res.send(data);
                });
            }
            else {
                data["msgcode"] = 2;
                data["msgtext"] = 'Invalid Current Password';
                data["results"] = '';
                res.send(data);
            }
        }
    });
});

router.get('/setesettingValue/', function (req, res) {
    var data = {};
    var query = "SELECT * FROM sitesettings";
    db_connection.con.query(query, function (err, result) {
        if (!!err) {
            data["msgcode"] = 0;
            data["msgtext"] = 'Error';
            data["results"] = err;
        }
        else {
            data["msgcode"] = 1;
            data["msgtext"] = 'Success';
            data["results"] = result;
        }
        res.send(data);
    });
})
//=======Admin Role ===========================//
router.get('/getAdminRoleData/', function (req, res) {
    var data = {};
    var query = "SELECT * FROM admin_role_type ORDER BY id DESC";
    console.log(query);
    db_connection.con.query(query, function (err, result) {
        if (!!err) {
            data["msgcode"] = 0;
            data["msgtext"] = 'Error';
            data["results"] = err;
        }
        else {
            data["msgcode"] = 1;
            data["msgtext"] = 'Success';
            data["results"] = result;
        }
        res.send(data);
    });
})
router.post('/saveAdminRole/', function (req, res) {
    var data = {};
    //====================Date Time============================================//
    var today = new Date();
    var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    var dateTime = date + ' ' + time;
    var admin_slag = config.slugify(req.body.role_name);
    //================================Create Sub Admin==============================//   
    var query = "INSERT INTO admin_role_type (role_name ,admin_type,status,created_at,updated_at) VALUES ('" + req.body.role_name + "','" + admin_slag + "','" + req.body.status + "','" + dateTime + "','" + dateTime + "')";
    console.log(query);
    db_connection.con.query(query, function (err, result) {
        if (!!err) {
            data["msgcode"] = 0;
            data["msgtext"] = 'Error Msg Text';
            data["results"] = err;
            res.send(data);
        }
        else {
            qry = "SELECT * FROM admin_menu_master";
            db_connection.con.query(qry, function (er, rs) {
                //console.log(rs);
                if (rs.length > 0) {
                    for (var i = 0; i < rs.length; i++) {
                        pqry = "INSERT INTO admin_role_permission SET menu_id = " + rs[i].id + ",role_id = " + result.insertId;
                        db_connection.con.query(pqry);
                    }
                }
            });

            data["msgcode"] = 1;
            data["msgtext"] = 'Success Msg Text';
            data["results"] = result;
            res.send(data);
        }
    });
});


router.get('/singleSetesettingValue/:id', function (req, res) {
    var data = {};
    var id = req.params.id;
    var query = "SELECT * FROM sitesettings WHERE id=" + id;
    db_connection.con.query(query, function (err, result) {
        if (!!err) {
            data["msgcode"] = 0;
            data["msgtext"] = 'Error';
            data["results"] = err;
        }
        else {
            data["msgcode"] = 1;
            data["msgtext"] = 'Success';
            data["results"] = result[0];
        }
        res.send(data);
    });
})

router.post('/updateSitesetting/', function (req, res) {
    var data = {};
    var query = "UPDATE sitesettings SET setting_name='" + req.body.setting_name + "',setting_value='" + req.body.setting_value + "' WHERE id='" + req.body.id + "'";
    console.log(query);
    db_connection.con.query(query, function (err, result) {
        if (!!err) {
            data["msgcode"] = 0;
            data["msgtext"] = 'Error';
            data["results"] = err;
            res.send(data);
        }
        else {
            data["msgcode"] = 1;
            data["msgtext"] = 'Success';
            data["results"] = result;
            res.send(data);
        }
    });
});

router.get('/getCurrencyName/:id', function (req, res) {
    var data = {};
    var id = req.params.id;
    var query = "SELECT * FROM currency WHERE id=" + id;
    db_connection.con.query(query, function (err, result) {
        if (!!err) {
            data["msgcode"] = 0;
            data["msgtext"] = 'Error';
            data["results"] = err;
        }
        else {
            data["msgcode"] = 1;
            data["msgtext"] = 'Success';
            data["results"] = result[0];
        }
        res.send(data);
    });
})


module.exports = router;