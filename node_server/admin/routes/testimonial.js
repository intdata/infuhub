var config = require('../config/config');
var express = require('express');
var router = express.Router();
var db_connection = require('../config/db_connection');
var path = require("path");
const multer = require('multer');
var im = require('imagemagick');
var fs = require('fs');


const DIR = '../src/assets/uploads/testimonial';
const IMG_DIR = '../../../src/assets/uploads/testimonial';
const DIR_THUMB = '../../../src/assets/uploads/testimonial/thumb';

var productImage;
let storage = multer.diskStorage({
	destination: (req, file, cb) => {
		cb(null, DIR);
	},
	filename: (req, file, cb) => {
		productImage = file.originalname;
		//console.log(file.originalname);
		cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname));
	}
});

let upload = multer({ storage: storage });


router.post('/addEditTestimonial/',upload.single('image'), function (req, res) {
	console.log(req.body);
	var data = {};
	var query = '';
	var msg = '';
	var qry = '';
	var pqry = '';
	var extraquery = '';

	var id = req.body.id;

	var today = new Date();
	var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
	var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
	var dateTime = date + ' ' + time;

	if (req.file == undefined)
		var imgName = '';
	else {
		var imgName = req.file.filename;
		fs.readFile(req.file.path, function (err, data) {
			//var imgName = req.file.image.name
			/// If there's an error
			if (!imgName) {
				console.log("There was an error")
				res.redirect("/");
				res.end();
			} else {
				var newPath = __dirname + "/" + IMG_DIR + "/" + imgName;
				var thumbPath = __dirname + "/" + DIR_THUMB + "/" + imgName;
				fs.writeFile(newPath, data, function (err) {
					// write file to uploads/thumbs folder
					im.resize({
						srcPath: newPath,
						dstPath: thumbPath,
						width: 200
					}, function (err, stdout, stderr) {
						if (err) throw err;
						console.log('resized image to fit within 200x200px');
					});

				});
			}
		});
	}

	if (id > 0) {
		query = " UPDATE ";

		extraquery = ", updated_at  = '" + dateTime + "'";

		msg = 'Record updated successfully';
	} else {
		query = " INSERT INTO ";
		msg = 'Record inserted successfully';
		extraquery = ", updated_at  = '" + dateTime + "', created_at   = '" + dateTime + "'";
	}

	if (imgName != '') {
		extraquery += ", image='" + imgName + "'";
	}

	query += "testimonial SET  	author    = '" + req.body.author_name + "', designation = '" + req.body.designation + "', description  = '" + req.body.description + "', status = '" + req.body.status + "'" + extraquery;



	if (id > 0) {
		query += " WHERE id=" + id;
	}

	console.log(query);

	db_connection.con.query(query, function (err, result) {
		if (!!err) {
			data["msgcode"] = 0;
			data["msgtext"] = 'Error Msg Text';
			data["results"] = err;
			res.send(data);
		} else {
			data["msgcode"] = 1;
			data["msgtext"] = 'Success Msg Text';
			data["results"] = result;
			res.send(data);
		}
	});


});



router.get('/deleteTestimonialData/:id', function (req, res) {
	var data = {};
	var _get = req.params;
	var Id = _get['id'];
	//console.log(req.query);
	var query = "DELETE FROM testimonial WHERE id = '" + Id + "'";;
	db_connection.con.query(query, function (err, result) {
		if (!!err) {
			data["msgcode"] = 0;
			data["msgtext"] = 'Error Msg Text';
			data["results"] = err;
			res.send(data);
		} else {
			data["msgcode"] = 1;
			data["msgtext"] = 'Success Msg Text';
			data["results"] = result;
			res.send(data);
		}
	});
});



// =========================List of All sub admin List=====================================//

router.get('/geAllTestimonialData/', function (req, res) {
	var data = {};

	var query = "SELECT id,author,designation,description,image,status, created_at,updated_at,created_by,updated_by  FROM testimonial ORDER BY  updated_at DESC";
//console.log(query);
	db_connection.con.query(query, function (err, result) {
		if (!!err) {
			data["msgcode"] = 0;
			data["msgtext"] = 'Error Msg Text';
			data["results"] = err;
			res.send(data);
		} else {
			data["msgcode"] = 1;
			data["msgtext"] = 'Success Msg Text';
			data["results"] = result;

			res.send(data);
		}
	});
});



router.get('/getEditData/:id', function (req, res) {
	var data = {};
	var _get = req.params;
	var id = _get['id'];
	//console.log(req.query);
	var query = "SELECT id, author ,designation,description,image,status FROM testimonial  where id='" + id + "'";
	db_connection.con.query(query, function (err, result) {
		if (!!err) {
			data["msgcode"] = 0;
			data["msgtext"] = 'Error Msg Text';
			data["results"] = err;
			res.send(data);
		} else {
			data["msgcode"] = 1;
			data["msgtext"] = 'Success Msg Text';
			data["results"] = result;

			//console.log(data);
			res.send(data);
		}
	});
});

router.post('/ChangeStatus/', function (req, res) {
	var data = {};
	//================================Edit Brand Owner==============================// 

	var id = req.body.id;
	var current_status = req.body.status;
	var status;
	if (current_status == 'Active') {
		status = 'Inactive'
	} else {

		status = 'Active'

	}

	var query = "UPDATE testimonial SET status ='" + status + "' WHERE id='" + req.body.id + "'";
	db_connection.con.query(query, function (err, result) {

		if (!!err) {
			data["msgcode"] = 0;
			data["msgtext"] = 'Error Msg Text';
			data["results"] = err;

			res.send(data);
		} else {
			data["msgcode"] = 1;
			data["msgtext"] = 'Success Msg Text';
			data["results"] = result;

			res.send(data);
		}
	});

});


module.exports = router;

