var config = require('../config/config');
var express = require('express');
var dateFormat = require('dateformat');
var router = express.Router();
var db_connection = require('../config/db_connection');
brand_owner_admin_transaction = require("./../../models/brand_owner_admin_transaction");
//**=============== Sequalize init =============== */
Sequelize = require("sequelize");
Op = Sequelize.Op;

//CampaignAssign = require("./../../models/campaign_assign");
//var campaignObj = campaign(sequelize, Sequelize);


/*router.post('/getEditData', function (req, res) {
	var data = {};
	console.log(req.body);
	query = "UPDATE campaign SET campaign_name = '" + req.body.campaign_name + "', campaign_details = '" + req.body.campaign_details + "', platform_id = '" + req.body.social_media + "', post_date = '" + dateFormat(req.body.campaign_post_date, 'yyyy-mm-dd HH:MM:ss') + "', start_date = '" + dateFormat(req.body.campaign_start_date, 'yyyy-mm-dd HH:MM:ss') + "', end_date = '" + dateFormat(req.body.campaign_end_date, 'yyyy-mm-dd HH:MM:ss') + "', total_amount_invested = '" + req.body.total_investment + "', roi = '" + req.body.roi_of_campaign + "', region = '" + req.body.populer_region + "', impression = '" + req.body.potential_impressions + "', status = '" + req.body.status + "' WHERE id = '" + req.body.id + "' ";
	console.log(query);
	db_connection.con.query(query, function (err, result) {
		if (!!err) {
			data["msgcode"] = 0;
			data["msgtext"] = 'Error Msg Text';
			data["results"] = err;
			res.send(data);
		} else {
			data["msgcode"] = 1;
			data["msgtext"] = 'Success Msg Text';
			data["results"] = result;
			res.send(data);
		}
	});
});*/


/*router.get('/deleteFaqData/:id', function (req, res) {
	var data = {};
	var _get = req.params;
	var Id = _get['id'];
	//console.log(req.query);
	var query = "DELETE FROM faq WHERE faq.id = '" + Id + "'";;
	db_connection.con.query(query, function (err, result) {
		if (!!err) {
			data["msgcode"] = 0;
			data["msgtext"] = 'Error Msg Text';
			data["results"] = err;
			res.send(data);
		} else {
			data["msgcode"] = 1;
			data["msgtext"] = 'Success Msg Text';
			data["results"] = result;
			res.send(data);
		}
	});
});
*/

// =========================List of All sub admin List=====================================//
/*router.get('/getSingleData/:id', function (req, res) {
	var data = {};
	var _get = req.params;
	var id = _get['id'];
	var query = "SELECT * FROM campaign where id='" + id + "'";
	db_connection.con.query(query, function (err, result) {
		if (!!err) {
			data["msgcode"] = 0;
			data["msgtext"] = 'Error Msg Text';
			data["results"] = err;
			res.send(data);
		} else {
			var influencerData = "SELECT CA.status, I.first_name, I.last_name, I.picture  FROM campaign_assign AS CA LEFT JOIN influencer AS I ON CA.influencer_id = I.id WHERE CA.campaign_id = " + id;
			db_connection.con.query(influencerData, function (er, rslt) {
				data["msgcode"] = 1;
				data["msgtext"] = 'Success Msg Text';
				data["results"] = result[0];
				data["influencerResult"] = rslt;
				res.send(data);
			});

		}
	});
});*/

router.get('/getAllData/:id?', function (req, res) {
	var data = {};
	var _get = req.params;
	var id = _get['id'];
	if(id == ''){
		data["msgcode"] = 0;
		data["msgtext"] = 'Error Msg Text';
		data["results"] = 'err';
		res.send(data);
	}


	var query = "SELECT C.campaign_name,C.service_type,C.start_date,C.end_date,BOT.*,CA.*,POP.*,IAT.id as IAT_ID FROM `brand_owner_admin_transaction` AS BOT JOIN campaign_assign AS CA on BOT.campaign_assign_id=CA.id JOIN campaign AS C ON CA.campaign_id = C.id JOIN proposals AS POP on CA.id = POP.invitation_id LEFT JOIN influencer_admin_transaction AS IAT on BOT.id = IAT.brand_owner_admin_transaction_id WHERE BOT.brand_owner_id = "+id;
	//console.log(query);return;
	// //var data  = campaignObj.findAll();
	// campaignObj.findAll().success(function(match) {
	// 	console.log(match)
	// 	res.send(match);
	// });
	// //console.log(data)
	db_connection.con.query(query, function (err, result) {
		if (!!err) {
			data["msgcode"] = 0;
			data["msgtext"] = 'Error Msg Text';
			data["results"] = 'Error';
			res.send(data);
		} else {
			data["msgcode"] = 1;
			data["msgtext"] = 'Success Msg Text';
			data["results"] = result;
			res.send(data);
		}
	});
});


router.post('/getAllExportData', function (req, res) {
	var data = {};
	var id = req.body.id;
	var service_type = req.body.service_type;
	var payment_date = req.body.payment_date;
	var where = " WHERE BOT.brand_owner_id = "+id
	if(service_type != ''){
		
		where = where + " AND C.service_type LIKE '%"+service_type+"%'";
	}
	if(payment_date != ''){
		
		where = where + " AND DATE(BOT.created_by) = "+"'"+payment_date+"'";
	}
	var query = "SELECT C.campaign_name,C.service_type,C.start_date,C.end_date,BOT.*,CA.*,POP.*,IAT.id as IAT_ID FROM `brand_owner_admin_transaction` AS BOT JOIN campaign_assign AS CA on BOT.campaign_assign_id=CA.id JOIN campaign AS C ON CA.campaign_id = C.id JOIN proposals AS POP on CA.id = POP.invitation_id LEFT JOIN influencer_admin_transaction AS IAT on BOT.id = IAT.brand_owner_admin_transaction_id"+where;
	//console.log(query);return;
	// //var data  = campaignObj.findAll();
	// campaignObj.findAll().success(function(match) {
	// 	console.log(match)
	// 	res.send(match);
	// });
	// //console.log(data)
	db_connection.con.query(query, function (err, result) {
		if (!!err) {
			data["msgcode"] = 0;
			data["msgtext"] = 'Error Msg Text';
			data["results"] = 'Error';
			res.send(data);
		} else {
			data["msgcode"] = 1;
			data["msgtext"] = 'Success Msg Text';
			data["results"] = result;
			res.send(data);
		}
	});
});
/*router.get('/statusChange/:id', function (req, res) {
	var data = {};
	var _get = req.params;
	var id = _get['id'];
	var qry = "UPDATE campaign set status = 'Approved' where id='" + id + "'";
	db_connection.con.query(qry, function (er, reslt) {
		if (er) {
			data["msgcode"] = 0;
			data["msgtext"] = 'Error';
			data["results"] = er;
			res.send(data);
		} else {
			var query = "SELECT * FROM campaign ";
			db_connection.con.query(query, function (err, result) {
				if (!!err) {
					data["msgcode"] = 0;
					data["msgtext"] = 'Error Msg Text';
					data["results"] = err;
					res.send(data);
				} else {
					data["msgcode"] = 1;
					data["msgtext"] = 'Success Msg Text';
					data["results"] = result;
					res.send(data);
				}
			});
		}
	});
});*/




module.exports = router;

