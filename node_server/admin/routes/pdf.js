var config = require('../config/config');
var express = require('express');
var dateFormat = require('dateformat');
var router = express.Router();
var db_connection = require('../config/db_connection');
influencer_admin_transaction = require("./../../models/influencer_admin_transaction");
var moment = require('moment');
var im = require('imagemagick');
const upload = require("../middleware/upload");
//**=============== Sequalize init =============== */
Sequelize = require("sequelize");
Op = Sequelize.Op;
const pdfHttpPath = config.inventory_path + "uploads/inventory/";
const pdfPath = config.asset_path + "uploads/inventory/";
//console.log(pdfPath);
const accessurl = config.frontEndBaseUrl;
var pdf = require("pdf-creator-node");
var fs = require('fs');

//var html = fs.readFileSync('htmlData', 'utf8');
var options = {
  format: "A3",
  orientation: "portrait",
  border: "1mm"
};

/**create pdf for influencer **/
router.get('/create/:invoice_number?', function (req, res, next) {
  var html = fs.readFileSync('views/invoice_influencer.html', 'utf8');
  var resultData = {};
  var paymentDate = '';
  var _get = req.params;
  var invoice_number = _get['invoice_number'];
  //console.log(invoice_number);return;
  //console.log(invoice_number);
  var uploadedFilePath = pdfPath+"influencer/"+"invoice"+invoice_number+".pdf";
  var uploadedFileUrl = pdfHttpPath+"influencer/"+"invoice"+invoice_number+".pdf";
	var query = "SELECT C.campaign_name,C.service_type,C.start_date,C.end_date,IAT.*,CA.*,POP.*,BOT.id as BOT_ID FROM `brand_owner_admin_transaction` AS BOT JOIN campaign_assign AS CA on BOT.campaign_assign_id=CA.id JOIN campaign AS C ON CA.campaign_id = C.id JOIN proposals AS POP on CA.id = POP.invitation_id LEFT JOIN influencer_admin_transaction AS IAT on BOT.id = IAT.brand_owner_admin_transaction_id WHERE IAT.invoice_number = "+"'"+invoice_number+"'";
	
	//console.log(query);return;
	db_connection.con.query(query, function (err, result) {
		if (!!err) {
			resultData["msgcode"] = 0;
			resultData["msgtext"] = 'Error Msg Text';
			resultData["results"] = 'Error';
			res.send(resultData);
		} else {
      if(result[0].payout_type=='Auto')
      {
        result[0].paymentDate = dateFormat(result[0].created_at,'yyyy-mm-dd');
      }else{
        result[0].paymentDate = dateFormat(result[0].manual_payout_on,'yyyy-mm-dd');
      }

      //console.log(result[0].paymentDate);return;
      var document = {
        html: html,
        data: {
          selectedInfluncersData: result,
          accessurl:accessurl,
          
        },
        path: uploadedFilePath
      };
      //console.log(document);
      pdf.create(document, options)
			resultData["msgcode"] = 1;
			resultData["msgtext"] = 'Success Msg Text';
			resultData["results"] = uploadedFileUrl;
			res.send(resultData);
		}
  });
  


});

/**create pdf for brandowner **/
router.get('/createBrand/:invoice_number?', function (req, res, next) {
  var resultData = {};
  var html = fs.readFileSync('views/invoice_brandowner.html', 'utf8');
  var _get = req.params;
  var invoice_number = _get['invoice_number'];
  //console.log(invoice_number);return;
  //console.log(invoice_number);
  var uploadedFilePath = pdfPath+"brandowner/"+"invoice"+invoice_number+".pdf";
  var uploadedFileUrl = pdfHttpPath+"brandowner/"+"invoice"+invoice_number+".pdf";
  var query = "SELECT C.campaign_name,C.service_type,C.start_date,C.end_date,BOT.*,CA.*,POP.*,IAT.id as IAT_ID FROM `brand_owner_admin_transaction` AS BOT JOIN campaign_assign AS CA on BOT.campaign_assign_id=CA.id JOIN campaign AS C ON CA.campaign_id = C.id JOIN proposals AS POP on CA.id = POP.invitation_id LEFT JOIN influencer_admin_transaction AS IAT on BOT.id = IAT.brand_owner_admin_transaction_id WHERE BOT.invoice_number = "+"'"+invoice_number+"'";

	
	//console.log(query);return;
	db_connection.con.query(query, function (err, result) {
		if (!!err) {
			resultData["msgcode"] = 0;
			resultData["msgtext"] = 'Error Msg Text';
			resultData["results"] = 'Error';
			res.send(resultData);
		} else {
      result[0].paymentDate = dateFormat(result[0].created_at,'yyyy-mm-dd');
      if(result[0].payment_status == "COMPLETED")
      {
        result[0].payment_status = 'Paid';
      }else{
        result[0].payment_status = 'Unpaid';
      }
      //console.log(result[0].paymentDate);return;
      var document = {
        html: html,
        data: {
          selectedInfluncersData: result,
          accessurl:accessurl,
          
        },
        path: uploadedFilePath
      };
      //console.log(document);
      pdf.create(document, options)
			resultData["msgcode"] = 1;
			resultData["msgtext"] = 'Success Msg Text';
			resultData["results"] = uploadedFileUrl;
			res.send(resultData);
		}
  });
  


});


module.exports = router;
