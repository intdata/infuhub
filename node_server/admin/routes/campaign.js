var config = require('../config/config');
var express = require('express');
var dateFormat = require('dateformat');
var router = express.Router();
var db_connection = require('../config/db_connection');

//CampaignAssign = require("./../../models/campaign_assign");
//**=============== Sequalize init =============== */
Sequelize = require("sequelize");
Op = Sequelize.Op;
db = require("./../../models/associations");
const brandOwnerImagePath = config.adminBaseUrl + "uploads/brand_owner/";
const influencerImagePath = config.adminBaseUrl + "uploads/influencer/";
const messageFilePath = config.adminBaseUrl + "uploads/message_files/";
const allPaths = {
	brandOwnerImagePath: brandOwnerImagePath,
	influencerImagePath: influencerImagePath,
	messageFilePath: messageFilePath
}
const filePath = config.adminBaseUrl + "uploads/message_files/";


router.post('/getEditData', function (req, res) {
	var data = {};
	console.log(req.body);
	query = "UPDATE campaign SET campaign_name = '" + req.body.campaign_name + "', campaign_details = '" + req.body.campaign_details + "', platform_id = '" + req.body.social_media + "', post_date = '" + dateFormat(req.body.campaign_post_date, 'yyyy-mm-dd HH:MM:ss') + "', start_date = '" + dateFormat(req.body.campaign_start_date, 'yyyy-mm-dd HH:MM:ss') + "', end_date = '" + dateFormat(req.body.campaign_end_date, 'yyyy-mm-dd HH:MM:ss') + "', total_amount_invested = '" + req.body.total_investment + "', roi = '" + req.body.roi_of_campaign + "', region = '" + req.body.populer_region + "', impression = '" + req.body.potential_impressions + "', status = '" + req.body.status + "' WHERE id = '" + req.body.id + "' ";
	console.log(query);
	db_connection.con.query(query, function (err, result) {
		if (!!err) {
			data["msgcode"] = 0;
			data["msgtext"] = 'Error Msg Text';
			data["results"] = err;
			res.send(data);
		} else {
			data["msgcode"] = 1;
			data["msgtext"] = 'Success Msg Text';
			data["results"] = result;
			res.send(data);
		}
	});
});


router.get('/deleteFaqData/:id', function (req, res) {
	var data = {};
	var _get = req.params;
	var Id = _get['id'];
	//console.log(req.query);
	var query = "DELETE FROM faq WHERE faq.id = '" + Id + "'";;
	db_connection.con.query(query, function (err, result) {
		if (!!err) {
			data["msgcode"] = 0;
			data["msgtext"] = 'Error Msg Text';
			data["results"] = err;
			res.send(data);
		} else {
			data["msgcode"] = 1;
			data["msgtext"] = 'Success Msg Text';
			data["results"] = result;
			res.send(data);
		}
	});
});


// =========================List of All sub admin List=====================================//
router.get('/getSingleData/:id', function (req, res) {
	var data = {};
	var _get = req.params;
	var id = _get['id'];
	var query = "SELECT * FROM campaign where id='" + id + "'";
	db_connection.con.query(query, function (err, result) {
		if (!!err) {
			data["msgcode"] = 0;
			data["msgtext"] = 'Error Msg Text';
			data["results"] = err;
			res.send(data);
		} else {
			var influencerData = "SELECT CA.status, I.first_name, I.last_name, I.picture  FROM campaign_assign AS CA LEFT JOIN influencer AS I ON CA.influencer_id = I.id WHERE CA.campaign_id = " + id;
			db_connection.con.query(influencerData, function (er, rslt) {
				data["msgcode"] = 1;
				data["msgtext"] = 'Success Msg Text';
				data["results"] = result[0];
				data["influencerResult"] = rslt;
				res.send(data);
			});

		}
	});
});

/**
 * Get detail of a job by sequalize
 */
router.post('/getJobDetail', function (req, res) {
	job_id = req.body.id
	//res.send([1]);

	influncer_id = req.body.influencerId
	return db.campaign.findOne({
		where: { id: job_id },
		order: [['id', 'DESC']],
		include: [{ model: db.brandowner }, { model: db.CampaignPlatformMap, include: [{ model: db.platform, attributes: ['name', 'class_name'] }] }, { model: db.campaignAssign, include: [{ model: db.influencer, where: { id: influncer_id } }] }]
	})
		.then(data => {
			//console.log(data)
			return res.status(200).json({ responseCode: 200, responseDetails: 'Get Past campaign data.', data: data });
		})
		.catch(reason => {
			//console.log(reason)
			return res.status(200).json({ responseCode: 420, responseDetails: 'Something went wrong!.', "error": reason });
		});
});

/**
 * Get message of a infulcer brandowner by sequalize
*/
router.post('/getInfluncerMessage', function (req, res) {
	var campaignId = req.body.id
	var influncerId = req.body.influencerId;
	return db.CampaignMessageConversation.findAll({
		where: { campaign_id: campaignId, influencer_id: influncerId },
		order: [['id', 'ASC']],
		include: [{ model: db.influencer }, { model: db.brandowner }],

	})
		.then(data => {
			db.campaignHistory.findAll(
				{
					where: {
						campaign_id: campaignId,
						influencer_id: influncerId
					}
				}
			).then(campaignHistory => {
				return res.status(200).json({
					responseCode: 200,
					responseDetails: 'Get Message data.',
					data: data,
					campaignHistory: campaignHistory,
					//brandOwnerImagePath:brandOwnerImagePath,
					//filePath:filePath, 
					//influencerImagePath:influencerImagePath
				});
			})
				.catch(reason => {
					return res.status(200).json({ responseCode: 420, responseDetails: 'Something went wrong!.', "error": reason });
				});
		});
});

/**
 * Get Job By Id by sequalize
*/
router.post('/campaignById', function (req, res) {
	return db.campaign.findOne({
		where: { id: req.body.id },
		order: [['id', 'DESC']],
		include: [{ model: db.brandowner },
		{ model: db.CampaignPlatformMap, include: [{ model: db.platform, attributes: ['name', 'class_name'] }] },
		{
			model: db.campaignAssign,
			include: [{
				model: db.influencer,
				include: [{ model: db.influencerPlatformMap, include: [{ model: db.platform, attributes: ['name', 'class_name'] }] }]
			}]
		}]
	})
		.then(data => {
			return res.status(200).json({ responseCode: 200, responseDetails: 'Get Past campaign data.', data: data, allPaths: allPaths });
		})
		.catch(reason => {
			return res.status(200).json({ responseCode: 420, responseDetails: 'Something went wrong!.', "error": reason });
		});
});

/**
 * Get message of a influncer for the campaign
*/
router.post('/getInfluncerDetailMessage', function (req, res) {
	campaignId = req.body.campaignId
	influncerId = req.body.influncerId
	//let campainDetail = await getCampaign({id:campaignId});
	//let brandCurrencyDetail = await getBrandOwnerCurrency({id:campainDetail.brand_owner_id});
	//let brandCurrencyPertg = await getBrandOwnerCurrencyPertg({id:brandCurrencyDetail.currency});


	return db.CampaignMessageConversation.findAll({
		where: { campaign_id: campaignId, influencer_id: influncerId },
		include: [{ model: db.influencer }, { model: db.brandowner }]
	})
		.then(data => {
			db.campaignHistory.findAll(
				{
					where: {
						campaign_id: campaignId,
						influencer_id: influncerId
					}
				}
			).then(campaignHistory => {
				db.campaignAssign.findOne(
					{
						where: {
							campaign_id: campaignId,
							influencer_id: influncerId,
						},
						include: [{
							model: db.proposal,
							attributes: ['campaign_id', 'influencer_id', 'invitation_id', 'price', 'currency'],
							//where: {invitation_id: campaignAssign.id}
						},
						{
							model: db.campaign,
							attributes: ['campaign_id', 'brand_owner_id']
						},
						{
							model: db.BrandOwnerAdminTransaction,
							attributes: ['id', 'brand_owner_id', 'campaign_assign_id', 'total_amount'],
							include: [
								{
									model: db.InfluencerAdminTransaction,
									attributes: ['id', 'brand_owner_admin_transaction_id', 'payout_type', 'payout_status'],
								}],
						}]
					}
				).then(campaignAssign => {
					return res.status(200).json({
						responseCode: 200,
						responseDetails: 'Get message data.',
						data: data,
						brandOwnerImagePath: "",
						campaignHistory: campaignHistory,
						campaignAssign: campaignAssign,
						filePath: filePath,
						influencerImagePath: "",
						//campainDetail:campainDetail,
						//brandCurrencyDetail:brandCurrencyDetail,
						//brandCurrencyPertg:brandCurrencyPertg
					});
				}).catch(reason => {
					return res.status(200).json({ responseCode: 420, responseDetails: 'Something went wrong!.', "error": reason });
				});
			}).catch(reason => {
				return res.status(200).json({ responseCode: 420, responseDetails: 'Something went wrong!.', "error": reason });
			});
		}).catch(reason => {
			return res.status(200).json({ responseCode: 420, responseDetails: 'Something went wrong!.', "error": reason });
		});

});

router.get('/getAllData/:id?', function (req, res) {
	var data = {};
	if (req.params.id) {
		//let earningDetail = await getCampaign({id:campaignId});
		db.campaign.findAll({
			include: [
				{
					model: db.campaignAssign,
					where: { 'influencer_id': req.params.id },
					include: [
						{
							model: db.BrandOwnerAdminTransaction,
							attributes:['id','brand_owner_id','default_currency_id'],
							include: [
								{ 	model: db.InfluencerAdminTransaction, }
							]
						}
					]
				},
				{ model: db.brandowner,
					attributes:['id','title','fname','lname','email'],
				 }]
		})
		.then(function (result) {
			//console.log(result)
			data["msgcode"] = 1;
			data["msgtext"] = 'Success Msg Text';
			data["results"] = result;
			res.send(data);
		})

	} else {
		db.campaign.findAll({ include: [{ model: db.campaignAssign }, { model: db.brandowner }] }).then(function (result) {
			//console.log(result)
			data["msgcode"] = 1;
			data["msgtext"] = 'Success Msg Text';
			data["results"] = result;
			res.send(data);
		})
	}

});

router.get('/getBrandOwner/:id?', function (req, res) {
	var data = {};
	var query = "SELECT C.*, BO.fname, BO.lname FROM campaign AS C LEFT JOIN brand_owner AS BO ON C.brand_owner_id = BO.id WHERE C.brand_owner_id=" + req.params.id

	// //var data  = campaignObj.findAll();
	// campaignObj.findAll().success(function(match) {
	// 	console.log(match)
	// 	res.send(match);
	// });
	console.log(query)
	db_connection.con.query(query, function (err, result) {
		if (!!err) {
			data["msgcode"] = 0;
			data["msgtext"] = 'Error Msg Text';
			data["results"] = err;
			res.send(data);
		} else {
			data["msgcode"] = 1;
			data["msgtext"] = 'Success Msg Text';
			data["results"] = result;
			res.send(data);
		}
	});
});

router.get('/statusChange/:id', function (req, res) {
	var data = {};
	var _get = req.params;
	var id = _get['id'];
	var qry = "UPDATE campaign set status = 'Approved' where id='" + id + "'";
	db_connection.con.query(qry, function (er, reslt) {
		if (er) {
			data["msgcode"] = 0;
			data["msgtext"] = 'Error';
			data["results"] = er;
			res.send(data);
		} else {
			var query = "SELECT * FROM campaign ";
			db_connection.con.query(query, function (err, result) {
				if (!!err) {
					data["msgcode"] = 0;
					data["msgtext"] = 'Error Msg Text';
					data["results"] = err;
					res.send(data);
				} else {
					data["msgcode"] = 1;
					data["msgtext"] = 'Success Msg Text';
					data["results"] = result;
					res.send(data);
				}
			});
		}
	});
});


// =========================List Assign Influencer Data By Campaign id=====================================//
router.get('/assignInfluencerByCampaign/:id', function (req, res) {
	var data = {};
	var newData = {};
	var testData = [];
	var inc = 0;
	var mydbdata;
	var menu_id;
	var id = req.params.id;


	var query = "SELECT * FROM (SELECT CMC.influencer_id,CMC.campaign_id,I.picture,I.first_name ,I.last_name FROM campaign_message_conversation AS CMC  LEFT JOIN influencer AS I on CMC.influencer_id=I.id WHERE CMC.campaign_id='" + id + "') AS T GROUP BY T.influencer_id";

	db_connection.con.query(query, function (err, result) {

		if (!!err) {
			data["msgcode"] = 0;
			data["msgtext"] = 'Error';
			data["results"] = err;
		}
		else {
			if (result.length > 0) {
				var influencer_id;

				for (i = 0; i < result.length; i++) {
					var influencerData = {};

					influencer_id = result[i].influencer_id;
					campaign_id = result[i].campaign_id;

					influencerData.influencer_id = result[i].influencer_id;
					influencerData.campaign_id = result[i].campaign_id;
					influencerData.image = result[i].picture;
					influencerData.name = result[i].first_name + ' ' + result[i].last_name;

					(function (menu_id, newData, influencer_id, influencerData) {
						var qry = "SELECT CMC.message,I.picture as infulencer_profile_img,I.first_name as infuler_first_name, I.last_name as infulencer_last_name, BO.fname as brand_owner_fname,BO.lname as brand_owner_lname,BO.company_logo,BO.company_name,CMC.message_from,CMC.created_at   FROM campaign_message_conversation AS CMC  LEFT JOIN influencer AS I on CMC.influencer_id=I.id LEFT JOIN brand_owner AS BO ON CMC.brand_owner_id =BO.id  WHERE CMC.campaign_id='" + id + "' AND CMC.influencer_id='" + influencerData.influencer_id + "' ORDER BY CMC.created_at ASC";

						db_connection.con.query(qry, function (err, resl) {

							messageData(result.length, resl, influencer_id, influencerData, i);
						});
					})(menu_id, newData, influencer_id, influencerData);
				}
			}
		}
	});
	function messageData(length, resl, influencer_id, influencerData, i) {

		mydata = {};
		mydata.influencer_id = {};
		mydata.messageList = {};

		inc++;
		mydata.influencer_id = influencerData.influencer_id;
		mydata.campaign_id = influencerData.campaign_id;
		mydata.image = influencerData.image;
		mydata.name = influencerData.name;
		mydata.messageList = resl;
		newData[inc] = mydata;

		if (length == inc) {
			data["msgcode"] = 1;
			data["msgtext"] = 'Success';
			data["results"] = newData;
			res.send(data);
		}
	}

});

//get campaign Data by campaign id //

router.get('/getCampaignDataById/:id', function (req, res) {
	var data = {};
	var _get = req.params;
	var id = _get['id'];

	var query = "SELECT * FROM campaign Where id='" + id + "'";
	db_connection.con.query(query, function (err, result) {
		if (!!err) {
			data["msgcode"] = 0;
			data["msgtext"] = 'Error Msg Text';
			data["results"] = err;
			res.send(data);
		} else {
			data["msgcode"] = 1;
			data["msgtext"] = 'Success Msg Text';
			data["results"] = result;
			res.send(data);
		}
	});
});

/**
 * Method to get campaign details
 * @param {*} obj 
 */
const getCampaign = async obj => {
	return await db.campaign.findOne({
		where: obj,
		attributes: ['id', 'brand_owner_id']
	});
};

module.exports = router;

