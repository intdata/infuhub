var config = require("../config/config");
var express = require("express");
var router = express.Router();
var db_connection = require("../config/db_connection");
var config = require("./../../admin/config/config");
var crypto = require("crypto");
var path = require("path");
const multer = require("multer");
var im = require("imagemagick");
var fs = require("fs");

const DIR = "./public/assets/uploads/brand_owner";
const IMG_DIR = "./../../public/assets/uploads/brand_owner";
const DIR_THUMB = "./../../public/assets/uploads/brand_owner/thumb";

var productImage;
let storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, DIR);
  },
  filename: (req, file, cb) => {
    productImage = file.originalname;
    //console.log(file.originalname);
    cb(
      null,
      file.fieldname + "-" + Date.now() + path.extname(file.originalname)
    );
  }
});
let upload = multer({ storage: storage });

router.post("/saveBrandOwner/", upload.single("image"), function(req, res) {
  var data = {};
  //====================Date Time============================================//
  var today = new Date();
  var date =
    today.getFullYear() + "-" + (today.getMonth() + 1) + "-" + today.getDate();
  var time =
    today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
  var dateTime = date + " " + time;

  //================Password Hashing=================================//

  var password = req.body.password;
  var hashPassword = crypto
    .createHash("md5")
    .update(password)
    .digest("hex");
  if (req.file == undefined) var imgName = "";
  else var imgName = req.file.filename;
  fs.readFile(req.file.path, function(err, data) {
    //var imgName = req.file.image.name
    /// If there's an error
    if (!imgName) {
      console.log("There was an error");
      res.redirect("/");
      res.end();
    } else {
      var newPath = __dirname + "/" + IMG_DIR + "/" + imgName;
      var thumbPath = __dirname + "/" + DIR_THUMB + "/" + imgName;
      fs.writeFile(newPath, data, function(err) {
        // write file to uploads/thumbs folder
        im.resize(
          {
            srcPath: newPath,
            dstPath: thumbPath,
            width: 200
          },
          function(err, stdout, stderr) {
            if (err) throw err;
            console.log("resized image to fit within 200x200px");
          }
        );
      });
    }
  });

  //================================Create Sub Admin==============================//

  var query =
    "INSERT INTO brand_owner (company_name,company_ph_no,fname,lname,industry,company_type,country_code,company_logo,email,password,status,created_at,updated_at) VALUES ('" +
    req.body.comapany_name +
    "','" +
    req.body.company_ph_no +
    "', '" +
    req.body.fname +
    "', '" +
    req.body.lname +
    "', '" +
    req.body.industry +
    "', '" +
    req.body.company_type +
    "', '" +
    req.body.country +
    "','" +
    imgName +
    "','" +
    req.body.email +
    "','" +
    hashPassword +
    "','" +
    req.body.status +
    "','" +
    dateTime +
    "','" +
    dateTime +
    "')";

  db_connection.con.query(query, function(err, result) {
    if (!!err) {
      data["msgcode"] = 0;
      data["msgtext"] = "Error Msg Text";
      data["results"] = err;
      res.send(data);
    } else {
      data["msgcode"] = 1;
      data["msgtext"] = "Success Msg Text";
      data["results"] = result;
      res.send(data);
    }
  });
});

router.post("/editBrandOwner/", upload.single("image"), function(req, res) {
  var data = {};

  //====================Date Time============================================//
  var today = new Date();
  var date =
    today.getFullYear() + "-" + (today.getMonth() + 1) + "-" + today.getDate();
  var time =
    today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
  var dateTime = date + " " + time;

  //================================Edit Brand Owner==============================//

  if (req.file == undefined) var imgName = "";
  else {
    var imgName = req.file.filename;
    fs.readFile(req.file.path, function(err, data) {
      //var imgName = req.file.image.name
      /// If there's an error
      if (!imgName) {
        console.log("There was an error");
        res.redirect("/");
        res.end();
      } else {
        var newPath = __dirname + "/" + IMG_DIR + "/" + imgName;
        var thumbPath = __dirname + "/" + DIR_THUMB + "/" + imgName;
        fs.writeFile(newPath, data, function(err) {
          // write file to uploads/thumbs folder
          im.resize(
            {
              srcPath: newPath,
              dstPath: thumbPath,
              width: 200
            },
            function(err, stdout, stderr) {
              if (err) throw err;
              console.log("resized image to fit within 200x200px");
            }
          );
        });
      }
    });
  }

  /*var query =
    "UPDATE brand_owner SET company_name ='" +
    req.body.comapany_name +
    "',company_ph_no ='" +
    req.body.company_ph_no +
    "',fname ='" +
    req.body.fname +
    "',lname ='" +
    req.body.lname +
    "',industry ='" +
    req.body.industry +
    "',company_type='" +
    req.body.company_type +
    "',country_code='" +
    req.body.country +
    "',email='" +
    req.body.email +
    "',status='" +
    req.body.status +
    "' ,updated_at='" +
    dateTime +
    "'";*/

    var query =
    "UPDATE brand_owner SET company_name ='" +
    req.body.comapany_name +
    "',company_ph_no ='" +
    req.body.company_ph_no +
    "',fname ='" +
    req.body.fname +
    "',lname ='" +
    req.body.lname +
    "',industry ='" +
    req.body.industry +
    "',company_type='" +
    req.body.company_type +
    "',country_code='" +
    req.body.country +
    "',email='" +
    "' ,updated_at='" +
    dateTime +
    "'";

  if (imgName != "") {
    query += ", company_logo='" + imgName + "'";
  }
  query += " WHERE id='" + req.body.id + "'";
  db_connection.con.query(query, function(err, result) {
    if (!!err) {
      data["msgcode"] = 0;
      data["msgtext"] = "Error Msg Text";
      data["results"] = err;
      res.send(data);
    } else {
      data["msgcode"] = 1;
      data["msgtext"] = "Success Msg Text";
      data["results"] = result;
      res.send(data);
    }
  });
});

router.get("/deleteBrandOwner/:id", function(req, res) {
  var data = {};
  var _get = req.params;
  var UserId = _get["id"];
  //console.log(req.query);
  var query = "DELETE FROM brand_owner WHERE brand_owner.id = '" + UserId + "'";
  db_connection.con.query(query, function(err, result) {
    if (!!err) {
      data["msgcode"] = 0;
      data["msgtext"] = "Error Msg Text";
      data["results"] = err;
      res.send(data);
    } else {
      data["msgcode"] = 1;
      data["msgtext"] = "Success Msg Text";
      data["results"] = result;
      res.send(data);
    }
  });
});

// =========================List of All sub admin List=====================================//

router.get("/getAllBrandOwnerData/", function(req, res) {
  var data = {};

  let uploadPath = config.upload_path;
  let companyDefaultLogo = config.company_default_logo;

  var query =
    "SELECT id,fname,lname,industry,IF(company_logo = '', '" +
    companyDefaultLogo +
    "', CONCAT('" +
    uploadPath +
    "brand_owner/thumb/', company_logo)) as company_logo,country_code as countryname,company_type,company_name,company_ph_no,email,is_email_verified,B.status,created_at FROM brand_owner B LEFT JOIN country C ON B.country_code =C.code Order By B.updated_at Desc";
  console.log(query);
  db_connection.con.query(query, function(err, result) {
    if (!!err) {
      data["msgcode"] = 0;
      data["msgtext"] = "Error Msg Text";
      data["results"] = err;
      res.send(data);
    } else {
      data["msgcode"] = 1;
      data["msgtext"] = "Success Msg Text";
      data["results"] = result;
      res.send(data);
    }
  });
});

router.get("/getEditData/:id", function(req, res) {
  var data = {};
  var _get = req.params;
  var UserId = _get["id"];
  //console.log(req.query);
  let uploadPath = config.upload_path;
  let companyDefaultLogo = config.company_default_logo;

  var query =
    "SELECT id,email,IF(company_logo = '', '" +
    companyDefaultLogo +
    "', CONCAT('" +
    uploadPath +
    "brand_owner/thumb/', company_logo)) as company_logo,company_name,company_ph_no,fname,lname,industry,company_type,country_code,status,created_at FROM brand_owner WHERE id='" +
    UserId +
    "'";
  db_connection.con.query(query, function(err, result) {
    if (!!err) {
      data["msgcode"] = 0;
      data["msgtext"] = "Error Msg Text";
      data["results"] = err;
      res.send(data);
    } else {
      data["msgcode"] = 1;
      data["msgtext"] = "Success Msg Text";
      data["results"] = result;
      res.send(data);
    }
  });
});

//===============check Email Id Exit Or Not========================================//
router.post("/checkEmailExitsOrNot/", function(req, res) {
  var data = {};
  var custParams = "";
  var TableName = "";
  if (req.body.id > 0) {
    custParams = " AND id!=" + req.body.id;
  }

  TableName = req.body.tablename;
  var query =
    "SELECT email FROM " +
    TableName +
    " WHERE email='" +
    req.body.email +
    "'" +
    custParams;
  db_connection.con.query(query, function(err, result) {
    if (!!err) {
      data["msgcode"] = 0;
      data["msgtext"] = "Error Msg Text";
      data["results"] = err;
    } else {
      if (result.length > 0) {
        data["statusCode"] = 400;
        data["error"] = "Bad Request";
        data["message"] = "Email address already registerd";

        res.status(400).send(data);
      } else {
        res.status(200).send("null");
      }

      //console.log(data);
    }
  });
});

router.post("/ChangeStatus/", function(req, res) {
  var data = {};
  //================================Edit Brand Owner==============================//

  var id = req.body.id;
  var current_status = req.body.status;
  var status;
  if (current_status == "Active") {
    status = "Inactive";
  } else {
    status = "Active";
  }

  var query =
    "UPDATE brand_owner SET status ='" +
    status +
    "' WHERE id='" +
    req.body.id +
    "'";

  db_connection.con.query(query, function(err, result) {
    if (!!err) {
      data["msgcode"] = 0;
      data["msgtext"] = "Error Msg Text";
      data["results"] = err;

      res.send(data);
    } else {
      data["msgcode"] = 1;
      data["msgtext"] = "Success Msg Text";
      data["results"] = result;

      res.send(data);
    }
  });
});


router.post("/UpdateSignupStatus/", function(req, res) {
  var data = {};
  //================================Edit Brand Owner==============================//
  var id = req.body.id;
  var current_status = req.body.is_email_verified;
  //console.log(current_status);return;
  var query =
    "UPDATE brand_owner SET is_email_verified ='" +
    current_status +
    "' WHERE id='" +
    req.body.id +
    "'";
  db_connection.con.query(query, function(err, result) {
    if (!!err) {
      data["msgcode"] = 0;
      data["msgtext"] = "Error Msg Text";
      data["results"] = err;
      res.send(data);
    } else {
      data["msgcode"] = 1;
      data["msgtext"] = "Success Msg Text";
      data["results"] = result;
      res.send(data);
    }
  });
});

module.exports = router;
