'use strict';
let multer = require('multer');
let path = require('path');

const upload = {};

// Set The Storage Engine
const storage = (data) => multer.diskStorage({
    destination: data.path,
    // destination: function (req, file, cb) {
    //   cb(null, './../influnaire-admin/node_server/public/assets/uploads/brand_owner')
    // },
    filename: function(req, file, cb){
      console.log(file.fieldname, file.originalname);
      
      cb(null,file.fieldname + '-' + Date.now() + path.extname(file.originalname));
    }
});
  
// Init Upload
upload.single = (data, req, res) => {
  return multer({
      storage: storage(data),
      limits:{fileSize: data.fileSize || 1000000},
      fileFilter: function(req, file, cb){
        checkFileType(req, file, cb);
      }
  }).single(data.fieldName);
  
} 

upload.multiple = (data) => multer({
  storage: storage(data),
  limits:{fileSize: 1000000},
  fileFilter: function(req, file, cb){
    checkFileType(req, file, cb);
  }
}).array(data.fieldName, data.maxFiles || 10);

//Upload All type file
upload.allType = (data) => {
  return multer({
    storage: storage(data),
    limits:{fileSize: data.fileSize || 1000000}
  }).single(data.fieldName);
}

// Check File Type
function checkFileType(req, file, cb){
    // Allowed ext
    const filetypes = /jpeg|jpg|png/;
    // Check ext
    const extname = filetypes.test(path.extname(file.originalname).toLowerCase());
    // Check mime
    const mimetype = filetypes.test(file.mimetype);
  
    if(mimetype && extname){
      return cb(null,true);
    } else {
      //cb('Error: Images Only!');
      req.fileValidationError = 'Error: Images Only!';
      return cb(null,false, new Error('Error: Images Only!'));
    }
}


module.exports = upload; 