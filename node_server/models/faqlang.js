'use strict';
module.exports = (sequelize, DataTypes) => {
  const FaqLang = sequelize.define('FaqLang', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    faq_id: DataTypes.INTEGER,
    lang_code: DataTypes.STRING,
    question: DataTypes.TEXT,
    answer: DataTypes.TEXT,
    createdAt: {type: Sequelize.DATE, field: 'created_at'},
    updatedAt: {type: Sequelize.DATE, field: 'updated_at'},
  }, {
    // define the table's name
    tableName: 'faq_lang',
    //freezeTableName: true,
    //underscored: true,
    // don't add the timestamp attributes (updatedAt, createdAt)
    timestamps: true,
  });
  FaqLang.associate = function(models) {
    // associations can be defined here
  };
  return FaqLang;
};