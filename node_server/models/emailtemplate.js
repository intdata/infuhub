'use strict';

let nodemailer = require('nodemailer');
Sequelize = require("sequelize");

const config = require("./../admin/config/config");

let transporter = nodemailer.createTransport(config.smtpConfig);

let db = (sequelize, DataTypes) => {
  const EmailTemplate = sequelize.define('EmailTemplate', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    template_name: DataTypes.STRING,
    template_subject: DataTypes.STRING,
    template_content: DataTypes.STRING,
    from_name: DataTypes.STRING,
    from_email: DataTypes.STRING,
    status: DataTypes.STRING,
    createdAt: {type: Sequelize.DATE, field: 'created_at'},
    updatedAt: {type: Sequelize.DATE, field: 'updated_at'},
    created_by: DataTypes.INTEGER,
    updated_by: DataTypes.INTEGER,
  }, {
    // define the table's name
    tableName: 'email_template',
    // don't add the timestamp attributes (updatedAt, createdAt)
    timestamps: true,
  });
  EmailTemplate.associate = function(models) {
    // associations can be defined here
  };
  return EmailTemplate;
};

/**
 * Method to send mail by email template
 * @param {*} mailTemplateName 
 * @param {*} mailTo 
 * @param {*} keyValues 
 */
let getMailContent = (mailTemplateName, mailTo, keyValues) => {
  db(sequelize, Sequelize).findOne({
    where: { template_name: mailTemplateName }
  })
  .then(emailContent => { 
    if(emailContent){
      let emailContentReplaced = Object.keys(keyValues).reduce((p,c) => {
          return p.split("[" + c + "]").join(keyValues[c])
      }, emailContent.template_content);

      let mailcontent = {
        to: mailTo,
        subject: emailContent.template_subject,
        html: emailContentReplaced
      };
    
      send(mailcontent); // send mail
    }
  })
  .catch(reason => {
    return res.json({responseCode: 420, responseDetails: 'Something went wrong!', error: reason});
  });
}

/**
 * Method to send mail
 */
let send = (mailcontent) => {
  transporter.sendMail(mailcontent, function(error, info) {
    if (error) {
      console.log(error);
      // res.send(error);
      res.json({
          "message": "Message not sent",
          "responseMessage": "201",
          "success": false,
          "result": error
      });
    } else {
      res.send('Email sent: ' + info.response);
      res.json({
          "message": "Successfully send the message",
          "responseMessage": "200",
          "success": true,
          "result": req.token
      });
    }
  });
}

module.exports = {
  db: db,
  sendMailWithContent: (mailcontent) => send(mailcontent),
  sendMailWithTemplate: (mailTemplateName, mailTo, keyValues) => getMailContent(mailTemplateName, mailTo, keyValues)
}