'use strict';
module.exports = (sequelize, DataTypes) => {
  const Cms = sequelize.define('Cms', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    slug: DataTypes.STRING,
    meta_tag: DataTypes.STRING,
    meta_title: DataTypes.STRING,
    meta_description: DataTypes.STRING,
    status: DataTypes.INTEGER,
    createdAt: {type: Sequelize.DATE, field: 'created_at'},
    updatedAt: {type: Sequelize.DATE, field: 'updated_at'},
  }, {
    // define the table's name
    tableName: 'cms',
    // don't add the timestamp attributes (updatedAt, createdAt)
    timestamps: false,
  });
  Cms.associate = function(models) {
    // associations can be defined here
  };
  return Cms;
};