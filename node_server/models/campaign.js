'use strict';
module.exports = (sequelize, DataTypes) => {
  const Campaign = sequelize.define('Campaign', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    brand_owner_id: DataTypes.INTEGER,
    brand_id: DataTypes.INTEGER,
    campaign_name: DataTypes.STRING,
    campaign_details: DataTypes.STRING,
    country_id: DataTypes.STRING,
    campaign_id: DataTypes.STRING,
    platform_id: DataTypes.INTEGER,
    post_date: DataTypes.DATE,
    start_date: DataTypes.DATE,
    proposals_due_date: DataTypes.DATE,
    tentative_launch_date: DataTypes.DATE,
    end_date: DataTypes.DATE,
    total_amount_invested: DataTypes.INTEGER,
    service_type: DataTypes.STRING,
    objective: DataTypes.STRING,
    objective_tentative_launch_date: DataTypes.DATE,
    roi: DataTypes.INTEGER,
    tag: DataTypes.INTEGER,
    followers: DataTypes.INTEGER,
    likes: DataTypes.INTEGER,
    region: DataTypes.STRING,
    impression: DataTypes.STRING,
    inovation_message: DataTypes.STRING,
    campaign_objective: DataTypes.STRING,
    content_concept: DataTypes.STRING,
    product_url: DataTypes.STRING,
    uploaded_file: DataTypes.STRING,
    invitation_expiery_date: DataTypes.STRING,
    total_amount_invested: DataTypes.INTEGER,
    created_at: DataTypes.DATE,
    updated_at: DataTypes.DATE,
    status: {
      type: DataTypes.ENUM('DRAFT', 'IN_PROGRESS', 'PUBLISHED'),
      defaultValue: 'DRAFT'
    },
  }, {
    // define the table's name
    tableName: 'campaign',
    // don't add the timestamp attributes (updatedAt, createdAt)
    timestamps: false,
  });
  return Campaign;
};