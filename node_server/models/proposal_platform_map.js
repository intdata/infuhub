'use strict';
module.exports = (sequelize, DataTypes) => {
  const proposal_platform_map = sequelize.define('proposal_platform_map', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    proposal_id: DataTypes.INTEGER,
    platform_id: DataTypes.INTEGER,
    createdAt: {type: Sequelize.DATE, field: 'created_at'},
    updatedAt: {type: Sequelize.DATE, field: 'updated_at'},
  }, {
    // define the table's name
    tableName: 'proposal_platform_map',
    // don't add the timestamp attributes (updatedAt, createdAt)
    timestamps: true,
  });
  proposal_platform_map.associate = function(models) {
    // associations can be defined here
  };
  return proposal_platform_map;
};