'use strict';
const db = {};
Sequelize = require("sequelize");

db.Sequelize = Sequelize;  
db.sequelize = sequelize;

/**
 * Include models
*/
db.page = require("./../models/page")(sequelize, Sequelize);
db.cmsLang = require("./../models/cms_lang")(sequelize, Sequelize);
db.testimonial = require("./../models/testimonial")(sequelize, Sequelize);
db.campaign = require("./../models/campaign")(sequelize, Sequelize);
db.campaignAssign = require("./../models/campaign_assign")(sequelize, Sequelize);
db.brand = require("./../models/brand")(sequelize, Sequelize);
db.brandowner = require("./../models/brandowner")(sequelize, Sequelize);
db.influencer = require("./../models/influencer")(sequelize, Sequelize);
db.platform = require("./../models/platform")(sequelize, Sequelize);
db.CampaignMessageConversation = require("./../models/campaign_message_conversation")(sequelize, Sequelize);
db.CampaignPlatformMap = require("./../models/campaign_platform_map")(sequelize, Sequelize);
db.industries = require("./../models/industries")(sequelize, Sequelize);
db.platform = require("./../models/platform")(sequelize, Sequelize);
//db.emailTemplate = require("./../models/emailtemplate")(sequelize, Sequelize);
db.faq = require("./../models/faq")(sequelize, Sequelize);
db.faqLang = require("./../models/faqlang")(sequelize, Sequelize);
db.contactUs = require("./../models/contactus")(sequelize, Sequelize);
db.siteSettings = require("./../models/sitesettings")(sequelize, Sequelize);
db.notification = require("./../models/notification")(sequelize, Sequelize);
db.notificationTemplate = require("./../models/notification_template")(sequelize, Sequelize);
db.proposal = require("./../models/proposals")(sequelize, Sequelize);
db.proposalPlatformMap = require("./../models/proposal_platform_map")(sequelize, Sequelize);
db.influencerPlatformMap = require("./../models/influencer_platform_map")(sequelize, Sequelize);
db.campaignHistory = require("./../models/campaign_history")(sequelize, Sequelize);
db.SavedInfluncer = require("./../models/saved_influencers")(sequelize, Sequelize);
db.BrandOwnerAdminTransaction = require("./../models/brand_owner_admin_transaction")(sequelize, Sequelize);
db.InfluencerAdminTransaction = require("./../models/influencer_admin_transaction")(sequelize, Sequelize);
db.currency = require("./../models/currencies")(sequelize, Sequelize);

/**
 * Create Campaign Association with models
*/
db.campaign.hasMany(db.campaignAssign, { foreignKey: 'campaign_id' });
db.campaignAssign.belongsTo(db.campaign, { foreignKey: 'campaign_id' });
db.brandowner.hasMany(db.campaign, { foreignKey: 'brand_owner_id' });
db.campaign.belongsTo(db.brandowner, { foreignKey: 'brand_owner_id' });
db.campaignAssign.belongsTo(db.influencer, { foreignKey: 'influencer_id' });
db.influencer.hasMany(db.campaignAssign, { foreignKey: 'influencer_id' });
db.influencer.hasMany(db.SavedInfluncer, { foreignKey: 'influencer_id' });
db.influencer.hasMany(db.influencerPlatformMap, { foreignKey: 'influencer_id' });
db.influencerPlatformMap.belongsTo(db.influencer, { foreignKey: 'influencer_id'});
db.influencerPlatformMap.belongsTo(db.platform, { foreignKey: 'platform_id' });
/**
 * For paypal integration
 */
db.campaignAssign.hasOne(db.proposal, { foreignKey: 'invitation_id' });
db.campaignAssign.hasOne(db.BrandOwnerAdminTransaction, { foreignKey: 'campaign_assign_id' });
db.BrandOwnerAdminTransaction.hasOne(db.InfluencerAdminTransaction, { foreignKey: 'brand_owner_admin_transaction_id' });
db.InfluencerAdminTransaction.belongsTo(db.influencer, { foreignKey: 'influencer_id'});
db.proposal.belongsTo(db.campaignAssign, { foreignKey: 'invitation_id' });
db.BrandOwnerAdminTransaction.belongsTo(db.campaignAssign, { foreignKey: 'campaign_assign_id' });
/**
 * Message conversation Association
 */
db.campaign.hasMany(db.CampaignMessageConversation, { foreignKey: 'campaign_id' });
db.CampaignMessageConversation.belongsTo(db.campaign, {foreignKey: 'campaign_id'})
db.CampaignMessageConversation.belongsTo(db.influencer, {foreignKey: 'influencer_id'})
db.CampaignMessageConversation.belongsTo(db.brandowner, {foreignKey: 'brand_owner_id'})

db.campaign.belongsTo(db.brandowner, { foreignKey: 'brand_owner_id' });
db.page.hasMany(db.cmsLang, { foreignKey: 'page_id' });

db.industries.hasMany(db.brandowner, { foreignKey: 'industry', as: 'brand_owners' });
db.brandowner.belongsTo(db.industries, { foreignKey: 'industry', as: 'industries' });

db.faq.hasMany(db.faqLang, { foreignKey: 'faq_id', as: 'question_answer' });
db.faqLang.belongsTo(db.faq, { foreignKey: 'faq_id', as: 'faq' });

db.influencer.hasMany(db.proposal, { foreignKey: 'influencer_id' });
db.proposal.belongsTo(db.influencer, { foreignKey: 'influencer_id' });

db.campaign.hasMany(db.proposal, { foreignKey: 'campaign_id' });
db.proposal.belongsTo(db.campaign, { foreignKey: 'campaign_id' });

db.proposal.hasMany(db.proposalPlatformMap, { foreignKey: 'proposal_id' });
db.platform.belongsTo(db.proposal, { foreignKey: 'proposal_id' });

db.campaign.hasMany(db.CampaignPlatformMap, { foreignKey: 'campaign_id' });
db.CampaignPlatformMap.belongsTo(db.campaign, { foreignKey: 'campaign_id' });

db.platform.hasMany(db.CampaignPlatformMap, { foreignKey: 'platform_id' });
db.CampaignPlatformMap.belongsTo(db.platform, { foreignKey: 'platform_id' });

db.influencer.hasMany(db.notification, { foreignKey: 'sender_id' });
db.notification.belongsTo(db.influencer, { foreignKey: 'sender_id' });

db.brandowner.hasMany(db.notification, { foreignKey: 'sender_id' });
db.notification.belongsTo(db.brandowner, { foreignKey: 'sender_id' });

db.CampaignPlatformMap.belongsTo(db.platform, { foreignKey: 'platform_id' });
db.platform.hasMany(db.CampaignPlatformMap, { foreignKey: 'platform_id' });

db.brand.belongsTo(db.brandowner, { foreignKey: 'brand_owner_id' });
db.brand.belongsTo(db.platform, { foreignKey: 'platform_id' });
db.brand.belongsTo(db.industries, { foreignKey: 'industry_id' });
db.campaign.belongsTo(db.brand, { foreignKey: 'brand_id' });

module.exports = db; 