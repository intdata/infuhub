'use strict';
module.exports = (sequelize, DataTypes) => {
  const influncerPaltfrom = sequelize.define('influncerPaltfrom', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    platform_id: DataTypes.INTEGER,
    influencer_id: DataTypes.INTEGER,
    field_type: DataTypes.STRING,
    details: DataTypes.STRING,
    created_at: { 
      type: DataTypes.DATE
    },
    updated_at: { 
      type: DataTypes.DATE
    }
  }, {
    // define the table's name
    tableName: ' influncer_platforms',
    // don't add the timestamp attributes (updatedAt, createdAt)
    timestamps: false,
  });
  return influncerPaltfrom;
};