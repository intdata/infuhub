'use strict';
module.exports = (sequelize, DataTypes) => {
  const Admin = sequelize.define('Admin', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    first_name: DataTypes.STRING,
    last_name: DataTypes.STRING,
    role_id: DataTypes.INTEGER,
    email: DataTypes.STRING,
    password: DataTypes.STRING,
    image: DataTypes.STRING,
    forgot_password_token: DataTypes.STRING,
    forgot_password_requested_at: Sequelize.DATE,
    status: DataTypes.STRING,
    created_at: { 
      type: DataTypes.DATE
    }
  }, {
    // define the table's name
    tableName: 'admin',
    // don't add the timestamp attributes (updatedAt, createdAt)
    timestamps: false,
  });
  Admin.associate = function(models) {
    // associations can be defined here
  };
  return Admin;
};