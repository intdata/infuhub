'use strict';
module.exports = (sequelize, DataTypes) => {
  const proposals = sequelize.define('proposals', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    campaign_id: DataTypes.INTEGER,
    influencer_id: DataTypes.INTEGER,
    invitation_id: DataTypes.INTEGER, 
    subject: DataTypes.TEXT,
    brief: DataTypes.TEXT,
    description: DataTypes.TEXT,
    delivery_date: DataTypes.DATE,
    currency: DataTypes.STRING,
    price: DataTypes.FLOAT,
    required_resources: DataTypes.TEXT,
    status: {
      type: DataTypes.ENUM('SUBMITTED', 'CONFIRM'),
      defaultValue: 'SUBMITTED'
    },
    createdAt: {type: Sequelize.DATE, field: 'created_at'},
    updatedAt: {type: Sequelize.DATE, field: 'updated_at'},
  }, {
    // define the table's name
    tableName: 'proposals',
    // don't add the timestamp attributes (updatedAt, createdAt)
    timestamps: true,
  });
  proposals.associate = function(models) {
    // associations can be defined here
  };
  return proposals;
};