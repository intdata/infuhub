'use strict';
module.exports = (sequelize, DataTypes) => {
  const BrandOwnerAdminTransaction = sequelize.define('BrandOwnerAdminTransaction', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    brand_owner_id: DataTypes.INTEGER,
    campaign_assign_id: DataTypes.INTEGER,
    invoice_number: DataTypes.STRING,
    total_amount: DataTypes.DECIMAL,
    net_amount: DataTypes.DECIMAL,
    gross_amount: DataTypes.DECIMAL,
    admin_commission: DataTypes.INTEGER,
    admin_commission_pertg: DataTypes.STRING,
    paypal_charge_brandowner_amt: DataTypes.DECIMAL,
    paypal_charge_brandowner_pertg: DataTypes.STRING,
    currency_fee_pertg: DataTypes.STRING,
    currency_fee_amt: DataTypes.DECIMAL,
    default_currency_id: DataTypes.INTEGER,
    brand_currency_id: DataTypes.INTEGER,
    payment_type: DataTypes.STRING,
    payment_status: DataTypes.STRING,
    transaction_id: DataTypes.STRING,
    transaction_details: DataTypes.JSON,
    createdAt: {type: Sequelize.DATE, field: 'created_at'},
    updatedAt: {type: Sequelize.DATE, field: 'updated_at'},
  }, {
    // define the table's name
    tableName: 'brand_owner_admin_transaction',
    // don't add the timestamp attributes (updatedAt, createdAt)
    timestamps: false,
  });

  BrandOwnerAdminTransaction.associate = function(models) {
    // BrandOwnerAdminTransaction.belongsTo(models.CampaignAssign, {
    //   foreignKey : 'id',
    //   targetKey: 'campaign_assign_id'
    // })
  };

  return BrandOwnerAdminTransaction;
};