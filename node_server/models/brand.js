'use strict';
module.exports = (sequelize, DataTypes) => {
  const Brand = sequelize.define('Brand', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    brand_owner_id: DataTypes.INTEGER,
    industry_id: DataTypes.INTEGER,
    country: DataTypes.STRING,
    brand_name: DataTypes.STRING,
    platform_id: DataTypes.INTEGER,
    logo: DataTypes.STRING,
    createdAt: {type: Sequelize.DATE, field: 'created_at'},
    updatedAt: {type: Sequelize.DATE, field: 'updated_at'},
  }, {
    // define the table's name
    tableName: 'brand',
    // don't add the timestamp attributes (updatedAt, createdAt)
    timestamps: false,
  });
  Brand.associate = function(models) {
    // associations can be defined here
  };
  return Brand;
};