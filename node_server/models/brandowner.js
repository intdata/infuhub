'use strict';
module.exports = (sequelize, DataTypes) => {
  const BrandOwner = sequelize.define('BrandOwner', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    title: DataTypes.STRING,
    fname: DataTypes.STRING,
    lname: DataTypes.STRING,
    title: DataTypes.STRING,
    company_name: DataTypes.STRING,
    country_code: DataTypes.STRING,
    currency: DataTypes.INTEGER,
    email: DataTypes.STRING,
    password: DataTypes.STRING,
    profile_image: DataTypes.STRING,
    company_logo: DataTypes.STRING,
    industry: DataTypes.INTEGER,
    company_type: DataTypes.INTEGER,
    company_ph_no: DataTypes.STRING,
    verication_token: DataTypes.STRING,
    is_email_verified: DataTypes.BOOLEAN,
    forgot_password_token: DataTypes.STRING,
    forgot_password_requested_at: Sequelize.DATE,
    status: DataTypes.STRING,
    createdAt: {type: Sequelize.DATE, field: 'created_at'},
    updatedAt: {type: Sequelize.DATE, field: 'updated_at'},
  }, {
    // define the table's name
    tableName: 'brand_owner',
    //freezeTableName: true,
    //underscored: true,
    // don't add the timestamp attributes (updatedAt, createdAt)
    timestamps: true,
  });
  BrandOwner.associate = function(models) {
    // associations can be defined here
    // models.BrandOwner.belongsTo(models.Industries, {
    //   //onDelete: "CASCADE",
    //   // foreignKey: {
    //   //   allowNull: false
    //   // }
    //   foreignKey: 'industry'
    // });
    // BrandOwner.belongsTo(models.Industries, {
    //   foreignKey: 'industry', // this should match
    //   targetKey: 'id',
    //   as: 'aa'
    //   //onDelete: 'CASCADE'
    // })
  };
  BrandOwner.test = (c) => a(c);
  let a = (c) => {
    console.log(c);
  }
  return BrandOwner;
};