'use strict';
module.exports = (sequelize, DataTypes) => {
  const Currencies = sequelize.define('Currencies', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    name: DataTypes.STRING,
    code: DataTypes.STRING,
    symbol: DataTypes.INTEGER,
    paypal_fee: DataTypes.TEXT
  }, {
    // define the table's name
    tableName: 'currency',
    // don't add the timestamp attributes (updatedAt, createdAt)
    timestamps: false,
  });
  Currencies.associate = function(models) {
    // associations can be defined here
  };
  return Currencies;
};