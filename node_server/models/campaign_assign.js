'use strict';
module.exports = (sequelize, DataTypes) => {
  const CampaignAssign = sequelize.define('CampaignAssign', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    campaign_id: DataTypes.INTEGER,
    influencer_id: DataTypes.INTEGER,
    message: DataTypes.TEXT,
    assign_date: { 
      type: DataTypes.DATE
    },
    expiry_date: { 
      type: DataTypes.DATE
    },
    proposal_due_date: { 
      type: DataTypes.DATE
    },
    accept_date_time: { 
      type: DataTypes.DATE
    },
    reject_date_time: { 
      type: DataTypes.DATE
    },
    createdAt: {type: Sequelize.DATE, field: 'created_at'},
    updatedAt: {type: Sequelize.DATE, field: 'updated_at'},
    status: {
      type: DataTypes.ENUM('PENDING', 'ACCEPT', 'REJECT', 'SUBMITTED', 'CONFIRM', 'CONTENT_SUBMITTED', 'CONTENT_APPROVED', 'PAID'),
      defaultValue: 'PENDING'
    },
  }, {
    // define the table's name
    tableName: 'campaign_assign',
    // don't add the timestamp attributes (updatedAt, createdAt)
    timestamps: true,
  });
  CampaignAssign.associate = function(models) {
    CampaignAssign.belongsTo(models.Campaign, {
      foreignKey : 'id',
      targetKey: 'campaign_id'
  })
  };
  return CampaignAssign;
};