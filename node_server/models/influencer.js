'use strict';
module.exports = (sequelize, DataTypes) => {
  const Influencer = sequelize.define('Influencer', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    first_name: DataTypes.STRING,
    last_name: DataTypes.STRING,
    platform_id: DataTypes.INTEGER,
    description: DataTypes.STRING,
    email: DataTypes.STRING,
    password: DataTypes.STRING,
    industry: DataTypes.STRING,
    contact_number: DataTypes.STRING,
    gender: {
      type: DataTypes.ENUM('M', 'F', 'O'),
      defaultValue: null
    },
    dob: Sequelize.DATE,
    country: DataTypes.STRING,
    nationality: DataTypes.STRING,
    picture: DataTypes.STRING,
    verication_token: DataTypes.STRING,
    is_email_verified: DataTypes.BOOLEAN,
    forgot_password_token: DataTypes.STRING,
    forgot_password_requested_at: Sequelize.DATE,
    is_agree_to_perform: {
      type: DataTypes.ENUM('0', '1'),
      defaultValue: null
    },
    payout_type: {
      type: DataTypes.ENUM('Auto', 'Manual'),
      defaultValue: 'Auto'
    },
    payout_mail: DataTypes.STRING,
    total_earning: DataTypes.DECIMAL,
    status: DataTypes.STRING,
    createdAt: {type: Sequelize.DATE, field: 'created_at'},
    updatedAt: {type: Sequelize.DATE, field: 'updated_at'},
  }, {
    // define the table's name
    tableName: 'influencer',
    // don't add the timestamp attributes (updatedAt, createdAt)
    timestamps: true,
  });
  // Influencer.associate = function(models) {
  //   // associations can be defined here
  //   Influencer.hasOne(models.VerificationToken, {
  //         as: 'verificationtoken',
  //         foreignKey: 'userId',
  //         foreignKeyConstraint: true,
  //       });
  // };
  return Influencer;
};