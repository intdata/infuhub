'use strict';
module.exports = (sequelize, DataTypes) => {
  const siteSettings = sequelize.define('siteSettings', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    setting_name: DataTypes.STRING,
    setting_label: DataTypes.STRING,
    setting_value: DataTypes.STRING,
    createdAt: {type: Sequelize.DATE, field: 'created_at'},
    updatedAt: {type: Sequelize.DATE, field: 'updated_at'},
  }, {
    // define the table's name
    tableName: 'sitesettings',
    // don't add the timestamp attributes (updatedAt, createdAt)
    timestamps: true,
  });
  siteSettings.associate = function(models) {
    // associations can be defined here
  };
  return siteSettings;
};