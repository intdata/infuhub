'use strict';
module.exports = (sequelize, DataTypes) => {
  const Platform = sequelize.define('Platform', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    name: DataTypes.STRING,
    icon: DataTypes.STRING,
    status: DataTypes.STRING,
    class_name: DataTypes.STRING
  }, {
    // define the table's name
    tableName: 'platforms',
    // don't add the timestamp attributes (updatedAt, createdAt)
    timestamps: false,
  });
  Platform.associate = function(models) {
    // associations can be defined here
  };
  return Platform;
};