'use strict';
module.exports = (sequelize, DataTypes) => {
  const Newsletter = sequelize.define('Newsletter', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    email: DataTypes.STRING,   
    status: DataTypes.STRING,
    createdAt: {type: Sequelize.DATE, field: 'created_at'},
    updatedAt: {type: Sequelize.DATE, field: 'updated_at'},
  }, {
    // define the table's name
    tableName: 'newsletter_subscriptions',
    // don't add the timestamp attributes (updatedAt, createdAt)
    timestamps: true,
  });
  // Influencer.associate = function(models) {
  //   // associations can be defined here
  //   Influencer.hasOne(models.VerificationToken, {
  //         as: 'verificationtoken',
  //         foreignKey: 'userId',
  //         foreignKeyConstraint: true,
  //       });
  // };
  return Newsletter;
};