var dbConfig = require('./config.json');
let config = {
  host    : dbConfig.development.host,
  user    : dbConfig.development.username,
  password: dbConfig.development.password,
  database: dbConfig.development.database
};

module.exports = config;
