import { BrowserModule } from '@angular/platform-browser';
import { NgModule,APP_INITIALIZER } from '@angular/core';
import { HttpModule } from '@angular/http';
import { Constant } from './constant';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgFlashMessagesModule } from 'ng-flash-messages';
import { PermissionService } from './admin/services/permission.service';
import { NgxPermissionsModule } from 'ngx-permissions';
import { ModalModule } from 'ngx-bootstrap/modal';
import { ToastrModule } from 'ngx-toastr';

export function init_app(permissionService: PermissionService) {

  return () => permissionService.initializeApp();
}

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpModule,
    NgFlashMessagesModule.forRoot(),
    NgxPermissionsModule.forRoot(),
    ModalModule.forRoot(),
    ToastrModule.forRoot()
  ],
  providers: [
    Constant,
    PermissionService,
    {
      provide: APP_INITIALIZER,
      useFactory: init_app,
      deps: [PermissionService],
      multi: true
    },   
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }