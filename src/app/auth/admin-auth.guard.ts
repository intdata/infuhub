import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AdminService } from '../admin/services/admin.service';
import {Constant} from '../constant';

@Injectable({
  providedIn: 'root'
})
export class AdminAuthGuard implements CanActivate {
  constructor(private constant:Constant, private adminservice:AdminService, private router: Router) {} 

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if(this.adminservice.isLogin()){
    	return true;
    }else{		
		this.router.navigate([this.constant.ADMIN_PATH+'login']);
		this.adminservice.deleteToken();
        return false;
    }
  }
} 
