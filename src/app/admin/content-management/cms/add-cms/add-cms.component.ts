import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, ControlContainer, FormGroup, FormControl,AsyncValidatorFn, AbstractControl, ValidationErrors  } from '@angular/forms';
import {Http,Response, Headers, RequestOptions } from '@angular/http';
import {Router,ActivatedRoute} from "@angular/router";
import { CommonService } from '../../../services/common.service';
import { AdmincmsService } from '../../../services/admincms.service';

@Component({
  selector: 'app-add-cms',
  templateUrl: './add-cms.component.html',
  styleUrls: ['./add-cms.component.scss']
})
export class AddCmsComponent implements OnInit {

	public tinyMceSettings = {
  		
	  skin_url: '/assets/tinymce/skins/lightgray',
	  inline: false,
	  statusbar: false,
	  browser_spellcheck: true,
	  height: 120,
	  plugins: 'fullscreen',
	};


  Form: FormGroup;
	submitted = false;
	
	id;
	title_en;
	description_en;
	shortdescription_en;
	title_ar;
	description_ar;
	shortdescription_ar;
	meta_title;
	meta_tag;
	meta_description;
	status ; 

  constructor(private admincmsService:AdmincmsService, private commonService:CommonService, private router:Router,private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder){}

  ngOnInit() {

    this.status='Active';
    this.activatedRoute.params.subscribe(params => {
    this.id = params['id'];
    }); 

        if(this.id>0)
        {
          this.admincmsService.getSingleCmsData(this.id).subscribe(data => {     
           
          if(data.msgcode==1 &&  data.results.length >0 )
            {

      				this.title_en  = data.results[0].title;
      				this.title_ar  = data.results[1].title;
      				this.description_en  = data.results[0].description;
      				this.description_ar  = data.results[1].description;
      				this.shortdescription_en  = data.results[0].short_description;
      				this.shortdescription_ar  = data.results[1].short_description;

      				this.meta_title  = data.results[0].meta_title;
      				this.meta_tag  = data.results[0].meta_tag;
      				this.meta_description  = data.results[0].meta_description;
      				this.status  = data.results[0].status;

            }
            
          });

      }
         
      if(this.id>0)
      {
      this.Form = new FormGroup({
        id: new FormControl(''),
        title_en: new FormControl('',<any>Validators.required),
        description_en: new FormControl(''),
        Short_desc_en:new FormControl(''),
        title_ar: new FormControl(''),
        description_ar: new FormControl(''),
        Short_desc_ar:new FormControl(''),
        meta_title: new FormControl(''),
        meta_tag: new FormControl(''),
        meta_description:new FormControl(''),
        status  : new FormControl('',<any>Validators.required)
      });

    }else{
      this.Form = new FormGroup({
        id: new FormControl(''),
        title_en: new FormControl('',<any>Validators.required),
        description_en: new FormControl(''),
        Short_desc_en:new FormControl(''),
        title_ar: new FormControl(''),
        description_ar: new FormControl(''),
        Short_desc_ar:new FormControl(''),
        meta_title: new FormControl(''),
        meta_tag: new FormControl(''),
        meta_description:new FormControl(''),
        status  : new FormControl('',<any>Validators.required)

      });
    }
  }

  get f() { return this.Form.controls; }

  onSubmit() {
         this.submitted = true;
        if (this.Form.invalid) {
            return;
        }else
        {
             if(this.id>0)
             {     


              this.admincmsService.addeditCmsData(this.Form.value).subscribe(data=>{ 
        
               if(data.msgcode==1 &&  data.results.affectedRows > 0 )
               {
                 this.router.navigate(['/admin/cms']);
               }
              
              });

           }else
           {

            
           this.admincmsService.addeditCmsData(this.Form.value).subscribe(data=>{ 
             if(data.msgcode==1 &&  data.results.affectedRows > 0 )
             {
               this.router.navigate(['/admin/cms']);
             }
              
            });

           }           

        } 
    }


   cancelForm = function(){
    this.router.navigate(['/admin/cms']);
  } 

}
