import { Component, OnInit ,OnDestroy  ,AfterViewInit,ViewChild } from '@angular/core';
import { FormBuilder, Validators, ControlContainer, FormGroup, FormControl } from '@angular/forms';
import {Http,Response, Headers, RequestOptions } from '@angular/http';
import {Router} from "@angular/router";
import { Subject } from 'rxjs'; 
import { DataTableDirective } from 'angular-datatables';
import { AdmincmsService } from '../../services/admincms.service';

@Component({
  selector: 'app-cms',
  templateUrl: './cms.component.html',
  styleUrls: ['./cms.component.scss']
})

export class CmsComponent implements OnInit ,OnDestroy,AfterViewInit {
   
   cmsData :any[]=[];
   @ViewChild(DataTableDirective)
   dtElement: DataTableDirective;
   dtOptions: any = {};
   dtTrigger: Subject<any> = new Subject();

  constructor(private admincmsService:AdmincmsService, private router:Router) { }
   
  ngOnInit() {

    this.dtOptions = {
    pageLength: 10,   
        columnDefs : [
            {
                // Target the actions column
                targets           : [2],
                responsivePriority: 1,
                filterable        : false,
                sortable          : false,
                orderable: false
            }                
        ]
	  };     
  }

  ngAfterViewInit(): void {    
      
      this.admincmsService.getAllCmsData().subscribe(data=>{
        console.log(JSON.stringify(data.results));
             if(data.msgcode==1)
               {
                 this.cmsData = data.results;
                 this.dtTrigger.next();
               }                                 
          });
 	 }

   ngOnDestroy(): void {
   this.dtTrigger.unsubscribe();
  }

  deleteCms = function(id){

  this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
          if(confirm("Are you sure to delete? ")) {
          this.admincmsService.deleteCmsData(id).subscribe(data => { 

          if(data.msgcode==1 && data.results.affectedRows >0)
          {
       
           dtInstance.destroy();
           this.ngAfterViewInit();
         }
          
        });

       }
    
    });

  } 

}
