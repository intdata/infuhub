import { Component, OnInit ,OnDestroy  ,AfterViewInit,ViewChild } from '@angular/core';
import { FormBuilder, Validators, ControlContainer, FormGroup, FormControl } from '@angular/forms';
import {Http,Response, Headers, RequestOptions } from '@angular/http';
import {Router} from "@angular/router";
import { Subject } from 'rxjs'; 
import { DataTableDirective } from 'angular-datatables';
import { EmailTemplateService } from '../../services/email-template.service';

@Component({
  selector: 'app-email-template',
  templateUrl: './email-template.component.html',
  styleUrls: ['./email-template.component.scss']
})

export class EmailTemplateComponent implements OnInit ,OnDestroy,AfterViewInit {

   emailTemplateData:any[]=[];
   statusData={};
   @ViewChild(DataTableDirective)
   dtElement: DataTableDirective;
   dtOptions: any = {};
   dtTrigger: Subject<any> = new Subject();

  constructor(private emailTemplateService:EmailTemplateService, private router:Router) { }
   
  ngOnInit() {
    this.dtOptions = {
    pageLength: 10,   
        columnDefs : [
            {
                // Target the actions column
                targets           : [2],
                responsivePriority: 1,
                filterable        : false,
                sortable          : false,
                orderable: false
            }                
        ]
	  };     
  }

  ngAfterViewInit(): void {    
    
    this.emailTemplateService.getAllEmailTemplateData().subscribe(data=>{
          
      if(data.msgcode==1)
      {
         this.emailTemplateData = data.results;     
         this.dtTrigger.next(); 
      }                 
              
   });
}
    
 	 

   ngOnDestroy(): void {
   this.dtTrigger.unsubscribe();
  }

  delete = function(id){

  this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
          if(confirm("Are you sure want to delete email template? ")) {
          this.emailTemplateService.deleteEmailData(id).subscribe(data => { 

          if(data.msgcode==1 && data.results.affectedRows >0)
          {
       
           dtInstance.destroy();
           this.ngAfterViewInit();
         }
          
        });

       }
    
    });

  } 

  changeStatus = function(id,status){

    this.statusData['id'] =id;
    this.statusData['status'] =status;

    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
    this.emailTemplateService.ChangeStatus(this.statusData).subscribe(data => { 
      if(data.msgcode==1 && data.results.affectedRows >0)
       {         
           dtInstance.destroy();
          this.ngAfterViewInit();
        
      }
    });
  });
  
  } 



}

