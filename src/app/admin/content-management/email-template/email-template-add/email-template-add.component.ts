import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, ControlContainer, FormGroup, FormControl,AsyncValidatorFn, AbstractControl, ValidationErrors  } from '@angular/forms';
import {Http,Response, Headers, RequestOptions } from '@angular/http';
import {Router,ActivatedRoute} from "@angular/router";
import { Observable } from "rxjs/Observable";
import { EmailTemplateService } from '../../../services/email-template.service';


@Component({
  selector: 'app-email-template-add',
  templateUrl: './email-template-add.component.html',
  styleUrls: ['./email-template-add.component.scss']
})
export class EmailTemplateAddComponent implements OnInit {

	public tinyMceSettings = {
  		
	  skin_url: '/assets/tinymce/skins/lightgray',
	  inline: false,
	  statusbar: false,
	  browser_spellcheck: true,
	  height: 120,
	  plugins: 'fullscreen',
	};


  Form: FormGroup;
	submitted = false;
	
	id;
	// template_name;
	description;
	subject;
	// from_name;
	// from_email;

	//status ; 
  actionName;
  constructor(private EmailTemplateService:EmailTemplateService, private router:Router,private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder){}

  ngOnInit() {

    //this.status='Active';
    this.activatedRoute.params.subscribe(params => {
    this.id = params['id'];
    }); 


    if (this.id > 0) {
      this.EmailTemplateService.getEditDataById(this.id).subscribe(data => {

        if (data.msgcode == 1 && data.results.length > 0) {
          this.id = data.results[0].id;
          // this.template_name = data.results[0].template_name;
          this.subject = data.results[0].template_subject;
          this.description = data.results[0].template_content;
          // this.from_name = data.results[0].from_name;
          // this.from_email = data.results[0].from_email;
          //this.status = data.results[0].status;
        }
      });
    }

                 
      if(this.id>0)
      {
        this.actionName = 'Edit';
        this.Form = new FormGroup({
        id: new FormControl(''),
        // template_name: new FormControl('',<any>Validators.required),
        description: new FormControl('',<any>Validators.required),
        subject:new FormControl('',<any>Validators.required),
        // from_name: new FormControl('',<any>Validators.required),
        // from_email: new FormControl('',<any>Validators.required),
        //status  : new FormControl('',<any>Validators.required)
      });

    }else{
      this.actionName = 'Add';
      this.Form = new FormGroup({
        id: new FormControl(''),
        // template_name: new FormControl('',<any>Validators.required),
        
        description: new FormControl('',<any>Validators.required),
        subject:new FormControl('',<any>Validators.required),
        // from_name: new FormControl('',<any>Validators.required),
        // from_email: new FormControl('',<any>Validators.required),
        //status  : new FormControl('',<any>Validators.required)

      });
    }
  }

  get f() { return this.Form.controls; }

  onSubmit() {
         this.submitted = true;
        if (this.Form.invalid) {
            return;
        }else
        {
            this.EmailTemplateService.addEditEmailTemplate(this.Form.value).subscribe(data => {
              
              if (data.msgcode == 1 && data.results.affectedRows > 0) {
                  this.router.navigate(['/admin/email-template']);
              }
          });

          //this.router.navigate(['/admin/email-template']);
        } 
    }


   cancelForm = function(){
    this.router.navigate(['/admin/email-template']);
  } 

}

