import {
  Component,
  OnInit,
  OnDestroy,
  AfterViewInit,
  ViewChild
} from "@angular/core";
import {
  FormBuilder,
  Validators,
  ControlContainer,
  FormGroup,
  FormControl
} from "@angular/forms";
import { Http, Response, Headers, RequestOptions } from "@angular/http";
import { Router } from "@angular/router";
import { Subject } from "rxjs";
import { DataTableDirective } from "angular-datatables";
import { NotificationTemplatesService } from "../../services/notification-templates.service";
@Component({
  selector: "app-notification-template",
  templateUrl: "./notification.component.html",
  styleUrls: ["./notification.component.scss"]
})
export class NotificationTemplatesComponent
  implements OnInit, OnDestroy, AfterViewInit {
  notificationTemplates: any[] = [];
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: any = {};
  dtTrigger: Subject<any> = new Subject();

  constructor(
    private notificationService: NotificationTemplatesService,
    private router: Router
  ) {}

  ngOnInit() {
    this.dtOptions = {
      pageLength: 10,
      columnDefs: [
        {
          // Target the actions column
          targets: [2],
          responsivePriority: 1,
          filterable: false,
          sortable: false,
          orderable: false
        }
      ]
    };
  }

  ngAfterViewInit(): void {
    this.notificationService.getAllNotificationTemplates().subscribe(data => {
      if (data.msgcode == 1) {
        this.notificationTemplates = data.results;
        this.dtTrigger.next();
      }
    });
  }

  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }

}
