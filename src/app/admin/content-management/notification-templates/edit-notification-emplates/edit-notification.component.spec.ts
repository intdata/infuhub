import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditNotificationTemplatesComponent } from './edit-notification.component';

describe('EditNotificationComponent', () => {
  let component: EditNotificationTemplatesComponent;
  let fixture: ComponentFixture<EditNotificationTemplatesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditNotificationTemplatesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditNotificationTemplatesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
