import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, ControlContainer, FormGroup, FormControl, AsyncValidatorFn, AbstractControl, ValidationErrors } from '@angular/forms';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Router, ActivatedRoute } from "@angular/router";
import { NotificationTemplatesService } from "./../../../services/notification-templates.service";

@Component({
  selector: 'app-edit-notification',
  templateUrl: './edit-notification.component.html',
  styleUrls: ['./edit-notification.component.scss']
})

export class EditNotificationTemplatesComponent implements OnInit {

  public tinyMceSettings = {

    skin_url: '/assets/tinymce/skins/lightgray',
    inline: false,
    statusbar: false,
    browser_spellcheck: true,
    height: 120,
    plugins: 'fullscreen',
  };

  id: any = 0;
  notificationTemplates: FormGroup;
  submitted = false;


  constructor(
    private notificationTemplatesService: NotificationTemplatesService,
    private router: Router, 
    private activatedRoute: ActivatedRoute, 
    private formBuilder: FormBuilder, 
  ) {}

  ngOnInit() {

    this.activatedRoute.params.subscribe(params => {
      this.id = params['id'];
    });

    this.notificationTemplates = this.formBuilder.group({
        id: ['', [Validators.required]],
        title: ['', [Validators.required]],
        content: ['', [Validators.required]]
    }); 

    if (this.id > 0) {
      this.notificationTemplatesService.getSingleNotificationTemplate(this.id).subscribe(data => {
        if (data.msgcode == 1) {
          this.notificationTemplates.controls['id'].setValue(this.id);
          this.notificationTemplates.controls['title'].setValue(data.results.title);
          this.notificationTemplates.controls['content'].setValue(data.results.content);
        }
      });

    }
  }

  get f() { return this.notificationTemplates.controls; }

  onSubmit() {
    this.submitted = true;
    if (this.notificationTemplates.invalid) {
      return;
    }

      this.notificationTemplatesService.editNotificationTemplate(this.notificationTemplates.value).subscribe(data => {
          this.router.navigate(['/admin/notification-templates']);
      });
  }


  cancelForm = function () {
    this.router.navigate(['/admin/notification-templates']);
  }

}
