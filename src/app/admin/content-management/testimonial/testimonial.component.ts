
import { Component, OnInit ,OnDestroy  ,AfterViewInit,ViewChild } from '@angular/core';
import { FormBuilder, Validators, ControlContainer, FormGroup, FormControl } from '@angular/forms';
import {Http,Response, Headers, RequestOptions } from '@angular/http';
import {Router} from "@angular/router";
import { Subject } from 'rxjs'; 
import { DataTableDirective } from 'angular-datatables';
import { TestimonialService } from '../../services/testimonial.service';

@Component({
  selector: 'app-testimonial',
  templateUrl: './testimonial.component.html',
  styleUrls: ['./testimonial.component.scss']
})

export class TestimonialComponent implements OnInit ,OnDestroy,AfterViewInit {
   
   testimonialData :any[]=[];
   statusData={};
   @ViewChild(DataTableDirective)
   dtElement: DataTableDirective;
   dtOptions: any = {};
   dtTrigger: Subject<any> = new Subject();

  constructor(private testimonialService:TestimonialService, private router:Router) { }
   
  ngOnInit() {
      this.dtOptions = {
    pageLength: 10,   
        columnDefs : [
            {
                // Target the actions column
                targets           : [5],
                responsivePriority: 1,
                filterable        : false,
                sortable          : false,
                orderable: false
            }                
        ]
	  };     
  }

  ngAfterViewInit(): void {    
       
    this.testimonialService.geAllTestimonialData().subscribe(data=>{
          
      if(data.msgcode==1)
      {
         this.testimonialData = data.results;     
         this.dtTrigger.next(); 
      }                 
              
   });
    
 	 }

   ngOnDestroy(): void {
   this.dtTrigger.unsubscribe();
  }

  delete = function(id){

  this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
          if(confirm("Are you sure want to delete testimonial? ")) {
          this.testimonialService.deleteTestimonialData(id).subscribe(data => { 

          if(data.msgcode==1 && data.results.affectedRows >0)
          {
       
           dtInstance.destroy();
           this.ngAfterViewInit();
         }
          
        });

       }
    
    });

  } 

  changeStatus = function(id,status){

    this.statusData['id'] =id;
    this.statusData['status'] =status;

    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
    this.testimonialService.ChangeStatus(this.statusData).subscribe(data => { 
      if(data.msgcode==1 && data.results.affectedRows >0)
       {         
           dtInstance.destroy();
          this.ngAfterViewInit();
        
      }
    });
  });
  
  }

}


