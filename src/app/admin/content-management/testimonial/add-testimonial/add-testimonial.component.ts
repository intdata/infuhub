import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, ControlContainer, FormGroup, FormControl,AsyncValidatorFn, AbstractControl, ValidationErrors  } from '@angular/forms';
import {Http,Response, Headers, RequestOptions } from '@angular/http';
import {Router,ActivatedRoute} from "@angular/router";
import { TestimonialService } from '../../../services/testimonial.service';
import { Observable } from "rxjs/Observable";


@Component({
  selector: 'app-add-testimonial',
  templateUrl: './add-testimonial.component.html',
  styleUrls: ['./add-testimonial.component.scss']
})
export class AddTestimonialComponent implements OnInit {

	public tinyMceSettings = {
  		
	  skin_url: '/assets/tinymce/skins/lightgray',
	  inline: false,
	  statusbar: false,
	  browser_spellcheck: true,
	  height: 120,
	  plugins: 'fullscreen',
	};


  Form: FormGroup;
	submitted = false;
	
	id;
  author_name;
  author_img;
	description;
	designation;

	status ; 
  actionName;
  constructor(private testimonialService:TestimonialService, private router:Router,private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder){}
 
  filesToUpload: Array<File> = [];
  fileChangeEvent(fileInput: any) {
    this.filesToUpload = <Array<File>>fileInput.target.files;
  }

  ngOnInit() {

    this.status='Active';
    this.activatedRoute.params.subscribe(params => {
    this.id = params['id'];
    }); 

    if (this.id > 0) {
      this.testimonialService.getEditDataById(this.id).subscribe(data => {

        if (data.msgcode == 1 && data.results.length > 0) {
          this.id = data.results[0].id;
          this.author_name = data.results[0].author
          this.author_img = data.results[0].image;
          this.designation = data.results[0].designation;         
          this.description = data.results[0].description;
          this.status = data.results[0].status;
        }
      });
    }

        
         
      if(this.id>0)
      {
        this.actionName = 'Edit';
        this.Form = new FormGroup({
        id: new FormControl(''),
        author_name: new FormControl('',<any>Validators.required),
        description: new FormControl('',<any>Validators.required),
        author_img: new FormControl(''),
        designation: new FormControl('',<any>Validators.required),
        status  : new FormControl('',<any>Validators.required)
      });

    }else{
      this.actionName = 'Add';
      this.Form = new FormGroup({
        id: new FormControl(''),
        author_name: new FormControl('',<any>Validators.required),
        description: new FormControl('',<any>Validators.required),
        author_img: new FormControl('',<any>Validators.required),
        designation: new FormControl('',<any>Validators.required),
        status  : new FormControl('',<any>Validators.required)

      });
    }
  }

  get f() { return this.Form.controls; }

  onSubmit() {
         this.submitted = true;
        if (this.Form.invalid) {
            return;
        }else
        {

          const imageFiles: Array<File> = this.filesToUpload;
          const formData = new FormData();

          if(imageFiles.length > 0)
             {
              formData.append("image", imageFiles[0], imageFiles[0]['name']);
             }

             var templateData = this.Form.controls;
             Object.keys(templateData).forEach(key => {
               var formVal = this.Form.value[key];
               formData.append(key, formVal);
             });

          
             this.testimonialService.addEditTestimonial(formData).subscribe(data => {
                  
              if (data.msgcode == 1 && data.results.affectedRows > 0) {
                 this.router.navigate(['/admin/testimonial']);
              }
          });


        

        } 
    }


   cancelForm = function(){
    this.router.navigate(['/admin/testimonial']);
  } 

}


