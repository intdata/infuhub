import { Component, OnInit ,OnDestroy  ,AfterViewInit,ViewChild} from '@angular/core';
import { FormBuilder, Validators, ControlContainer, FormGroup, FormControl} from '@angular/forms';
import {Http,Response, Headers, RequestOptions } from '@angular/http';
import {Router} from "@angular/router";
import { Subject } from 'rxjs'; 
import { DataTableDirective } from 'angular-datatables';
import { FaqService } from '../../services/faq.service';
@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.scss']
})

export class FaqComponent implements OnInit ,OnDestroy,AfterViewInit {
   
  faqData :any[]=[];
   @ViewChild(DataTableDirective)
   dtElement: DataTableDirective;
   dtOptions: any = {};
   dtTrigger: Subject<any> = new Subject();

  constructor(private faqService:FaqService, private router:Router) { }
   
  ngOnInit() {

    this.dtOptions = {
    pageLength: 10,   
        columnDefs : [
            {
                // Target the actions column
                targets           : [3],
                responsivePriority: 1,
                filterable        : false,
                sortable          : false,
                orderable: false
            }                
        ]
	  };     
  }

  ngAfterViewInit(): void {    
     
      this.faqService.getAllFaqData().subscribe(data=>{
           console.log(JSON.stringify(data.results));
             if(data.msgcode==1)
               {
                 this.faqData = data.results;
                 this.dtTrigger.next();
               }                                 
          });
 	 }

   ngOnDestroy(): void {
   this.dtTrigger.unsubscribe();
  }

  deleteFaq = function(id){
  this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
          if(confirm("Are you sure to delete? ")) {
          this.faqService.deleteFaqData(id).subscribe(data => { 

          if(data.msgcode==1 && data.results.affectedRows >0)
          {
       
           dtInstance.destroy();
           this.ngAfterViewInit();
         }
          
        });

       }
    
    });

  } 

}

