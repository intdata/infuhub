import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, ControlContainer, FormGroup, FormControl, AsyncValidatorFn, AbstractControl, ValidationErrors } from '@angular/forms';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Router, ActivatedRoute } from "@angular/router";
import { CommonService } from '../../../services/common.service';
import { FaqService } from '../../../services/faq.service';

@Component({
  selector: 'app-add-faq',
  templateUrl: './add-faq.component.html',
  styleUrls: ['./add-faq.component.scss']
})

export class AddFaqComponent implements OnInit {

  public tinyMceSettings = {

    skin_url: '/assets/tinymce/skins/lightgray',
    inline: false,
    statusbar: false,
    browser_spellcheck: true,
    height: 120,
    plugins: 'fullscreen',
  };


  Form: FormGroup;
  submitted = false;

  id: number;
  quesn_en: any;
  ans_en: any;
  quesn_ar: any;
  ans_ar: any;
  status: string;

  constructor(private faqService: FaqService, private commonService: CommonService, private router: Router, private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder) { }

  ngOnInit() {

    this.status = 'Active';
    this.activatedRoute.params.subscribe(params => {
      this.id = params['id'];
    });

    if (this.id > 0) {
      this.faqService.getSingleFaqData(this.id).subscribe(data => {

        if (data.msgcode == 1 && data.results.length > 0) {
          this.quesn_en = data.results[0].question;
          this.quesn_ar = data.results[1].question;
          this.ans_en = data.results[0].answer;
          this.ans_ar = data.results[1].answer;
          this.status = data.results[0].status;
        }

      });

    }

    if (this.id > 0) {
      this.Form = new FormGroup({
        id: new FormControl(''),
        quesn_en: new FormControl('', <any>Validators.required),
        ans_en: new FormControl(''),
        quesn_ar: new FormControl(''),
        ans_ar: new FormControl(''),
        status: new FormControl('', <any>Validators.required)
      });

    } else {
      this.Form = new FormGroup({
        id: new FormControl(''),
        quesn_en: new FormControl('', <any>Validators.required),
        ans_en: new FormControl(''),
        quesn_ar: new FormControl(''),
        ans_ar: new FormControl(''),
        status: new FormControl('', <any>Validators.required)

      });
    }
  }

  get f() { return this.Form.controls; }

  onSubmit() {
    this.submitted = true;
    if (this.Form.invalid) {
      return;
    } else {
      if (this.id > 0) {


        this.faqService.addeditFaqData(this.Form.value).subscribe(data => {

          if (data.msgcode == 1 && data.results.affectedRows > 0) {
            this.router.navigate(['/admin/faq-list']);
          }

        });

      } else {


        this.faqService.addeditFaqData(this.Form.value).subscribe(data => {
          if (data.msgcode == 1 && data.results.affectedRows > 0) {
            this.router.navigate(['/admin/faq']);
          }

        });

      }

    }
  }


  cancelForm = function () {
    this.router.navigate(['/admin/faq']);
  }

}
