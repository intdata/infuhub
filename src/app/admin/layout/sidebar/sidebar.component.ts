import { Component, OnInit } from '@angular/core';
import { AdminService } from '../../services/admin.service';
import { AdminMenuService } from '../../services/admin-menu.service';
@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  menuData={};
  admin_name;
  admin_profile_img;
 
  constructor(private adminService: AdminService, private adminMenuService: AdminMenuService) { }
  ngOnInit() {
    this.adminMenuService.getSidebarMenuData().subscribe(data => {
      this.menuData = data.results;
      //console.log(JSON.stringify(this.menuData));
    });
    
    var adminInfo = this.adminService.getAdminInfo();
    if (adminInfo) {
      adminInfo = JSON.parse(adminInfo);
      this.admin_name = adminInfo['first_name'] + ' ' + adminInfo['last_name'];
      this.admin_profile_img = adminInfo['image'];
    }
    
    
  }

  
}
