import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { NgxPermissionsGuard } from 'ngx-permissions';
import { InnerLayoutComponent } from './layout/inner-layout/inner-layout.component';
import { LoginLayoutComponent } from './layout/login-layout/login-layout.component';
import { LoginComponent } from './accounts/login/login.component';
import { DashboardComponent } from './accounts/dashboard/dashboard.component';
import { AdminAuthGuard } from '../auth/admin-auth.guard';
import { AdminUserComponent } from './users/admin-user/admin-user.component';
import { AddAdminComponent } from './users/admin-user/add-admin/add-admin.component';
import { AddInfluencerComponent } from './users/influencer/add-influencer/add-influencer.component';
import { InfluencerComponent } from './users/influencer/influencer.component';
import { InfluencerDetailsComponent } from './users/influencer/influencer-details/influencer-details.component';
import { JobDetailsComponent } from './users/influencer/job-details/job-details.component';
import { AddBrandOwnerComponent } from './users/brand-owner/add-brand-owner/add-brand-owner.component';
import { BrandOwnerComponent } from './users/brand-owner/brand-owner.component';
import { CmsComponent } from './content-management/cms/cms.component';
import { AddCmsComponent } from './content-management/cms/add-cms/add-cms.component';
import { PermissionComponent } from './users/admin-user/permission/permission.component';
import { MenuAddComponent } from './settings/admin-menu/menu-add/menu-add.component';
import { AdminMenuComponent } from './settings/admin-menu/admin-menu.component';
import { NoPermissionComponent } from './settings/no-permission/no-permission.component';
import { ChangePasswordComponent } from './accounts/change-password/change-password.component';
import { SitesettingsComponent } from './settings/sitesettings/sitesettings.component';
import { AddFaqComponent } from './content-management/faq/add-faq/add-faq.component'
import { FaqComponent } from './content-management/faq/faq.component';
import { SitesettingsEditComponent } from './settings/sitesettings/sitesettings-edit/sitesettings-edit.component';
import { AdminRoleComponent } from './settings/admin-role/admin-role.component';
import { AdminRolePermissionComponent } from './settings/admin-role-permission/admin-role-permission.component';
import { AddeditAdminRoleComponent } from './settings/admin-role/addedit-admin-role/addedit-admin-role.component';
import { ProfileComponent } from './accounts/profile/profile.component';
import { FrontEndMenuManagementComponent } from './settings/front-end-menu-management/front-end-menu-management.component';
import { AddMenuComponent } from './settings/front-end-menu-management/add-menu/add-menu.component';
import { CampaignComponent } from './campaign/campaign.component';
import { CampaignListComponent } from './campaign-list/campaign-list.component';
import { CampaignDetailsComponent } from './campaign/campaign-details/campaign-details.component';
import { CampaignAddEditComponent } from './campaign/campaign-add-edit/campaign-add-edit.component';
import { EmailTemplateComponent } from './content-management/email-template/email-template.component';
import { TestimonialComponent } from './content-management/testimonial/testimonial.component';
import { EmailTemplateAddComponent } from './content-management/email-template/email-template-add/email-template-add.component';
import { AddTestimonialComponent } from './content-management/testimonial/add-testimonial/add-testimonial.component';
import { MessageConversationComponent } from './campaign/message-conversation/message-conversation.component';
import { ForgotPasswordComponent } from './accounts/forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './accounts/reset-password/reset-password.component';
import { NotificationTemplatesComponent } from './content-management/notification-templates/notification.component';
import { EditNotificationTemplatesComponent } from './content-management/notification-templates/edit-notification-emplates/edit-notification.component';
import { BrandOwnerTransactionComponent } from './users/brand-owner/brand-owner-transaction/brand-owner-transaction.component';
import { InfluencerTransactionComponent } from './users/influencer/influencer-transaction/influencer-transaction.component';
import { BrandJobDetailsComponent } from './users/brand-owner/brand-job-details/brand-job-details.component';


const routes: Routes = [
  { path: '', redirectTo: '/admin/login', pathMatch: 'full' },

  {
    path: '', component: LoginLayoutComponent,
    children: [
      { path: 'login', component: LoginComponent },
      { path: 'forgot-password', component: ForgotPasswordComponent},
      { path: 'reset-password', component: ResetPasswordComponent},
    ]
  },
  {
    path: '', component: InnerLayoutComponent, canActivate: [AdminAuthGuard],
    children: [
      { path: 'no-permission', component: NoPermissionComponent },
      { path: 'dashboard', component: DashboardComponent },
      {
        path: 'add-admin', component: AddAdminComponent,
        canActivate: [NgxPermissionsGuard],
        data: {
          permissions: {
            only: 'admin-user_add',
            redirectTo: 'admin/no-permission'
          }
        }
      },
      {
        path: 'editadmin/:id', component: AddAdminComponent,
        canActivate: [NgxPermissionsGuard],
        data: {
          permissions: {
            only: 'admin-user_edit',
            redirectTo: 'admin/no-permission'
          }
        }
      },
      {
        path: 'admin-user', component: AdminUserComponent,
        canActivate: [NgxPermissionsGuard],
        data: {
          permissions: {
            only: 'admin-user_view',
            redirectTo: 'admin/no-permission'
          }
        }
      },
      {
        path: 'influencer-add', component: AddInfluencerComponent,
        canActivate: [NgxPermissionsGuard],
        data: {
          permissions: {
            only: 'influencer_add',
            redirectTo: 'admin/no-permission'
          }
        }
      },
      {
        path: 'influencer', component: InfluencerComponent,
        canActivate: [NgxPermissionsGuard],
        data: {
          permissions: {
            only: ['influencer_view'],
            redirectTo: 'admin/no-permission'
          }
        }
      },
      {
        path: 'influencer-transaction-details/:id', component: InfluencerTransactionComponent,
        canActivate: [NgxPermissionsGuard],
        data: {
          permissions: {
            only: 'influencer_view',
            redirectTo: 'admin/no-permission'

          }
        }
      },
      {
        path: 'influencer-edit/:id', component: AddInfluencerComponent,
        canActivate: [NgxPermissionsGuard],
        data: {
          permissions: {
            only: 'influencer_edit',
            redirectTo: 'admin/no-permission'
          }
        }
      },
      {
        path: 'add-brand-owner', component: AddBrandOwnerComponent,
        canActivate: [NgxPermissionsGuard],
        data: {
          permissions: {
            only: 'brand-owner_add',
            redirectTo: 'admin/no-permission'

          }
        }
      },
      {
        path: 'editbrand-owner/:id', component: AddBrandOwnerComponent,
        canActivate: [NgxPermissionsGuard],
        data: {
          permissions: {
            only: 'brand-owner_edit',
            redirectTo: 'admin/no-permission'
          }
        }
      },
      {
        path: 'brand-owner', component: BrandOwnerComponent,
        canActivate: [NgxPermissionsGuard],
        data: {
          permissions: {
            only: ['brand-owner_view'],
            redirectTo: 'admin/no-permission'
          }
        }
      },
      {
        path: 'brand-transaction-details/:id', component: BrandOwnerTransactionComponent,
        canActivate: [NgxPermissionsGuard],
        data: {
          permissions: {
            only: 'brand-owner_view',
            redirectTo: 'admin/no-permission'

          }
        }
      },
      { path: 'permission/:id', component: PermissionComponent },
      {
        path: 'menu-add', component: MenuAddComponent,
        canActivate: [NgxPermissionsGuard],
        data: {
          permissions: {
            only: ['menu_add'],
            redirectTo: 'admin/no-permission'
          }
        }
      },
      {
        path: 'menu', component: AdminMenuComponent,
        canActivate: [NgxPermissionsGuard],
        data: {
          permissions: {
            only: ['menu_view'],
            redirectTo: 'admin/no-permission'
          }
        }
      },
      {
        path: 'menu-edit/:id', component: MenuAddComponent,
        canActivate: [NgxPermissionsGuard],
        data: {
          permissions: {
            only: ['menu_edit'],
            redirectTo: 'admin/no-permission'
          }
        }
      },
      {
        path: 'cms', component: CmsComponent,
        canActivate: [NgxPermissionsGuard],
        data: {
          permissions: {
            only: ['cms_view'],
            redirectTo: 'admin/no-permission'
          }
        }
      },
      {
        path: 'add-cms', component: AddCmsComponent,
        canActivate: [NgxPermissionsGuard],
        data: {
          permissions: {
            only: ['cms_add'],
            redirectTo: 'admin/no-permission'
          }
        }
      },
      {
        path: 'edit-cms/:id', component: AddCmsComponent,
        canActivate: [NgxPermissionsGuard],
        data: {
          permissions: {
            only: ['cms_edit'],
            redirectTo: 'admin/no-permission'
          }
        }
      },
      {
        path: 'faq', component: FaqComponent,
        canActivate: [NgxPermissionsGuard],
        data: {
          permissions: {
            only: ['faq_view'],
            redirectTo: 'admin/no-permission'
          }
        }
      },
      {
        path: 'add-faq', component: AddFaqComponent,
        canActivate: [NgxPermissionsGuard],
        data: {
          permissions: {
            only: ['faq_add'],
            redirectTo: 'admin/no-permission'
          }
        }
      },
      {
        path: 'edit-faq/:id', component: AddFaqComponent,
        canActivate: [NgxPermissionsGuard],
        data: {
          permissions: {
            only: ['faq_edit'],
            redirectTo: 'admin/no-permission'
          }
        }
      },
      {
        path: 'notification-templates', component: NotificationTemplatesComponent,
        canActivate: [NgxPermissionsGuard],
        data: {
          permissions: {
            only: ['notification-templates_view'],
            redirectTo: 'admin/no-permission'
          }
        }
      },
      {
        path: 'edit-notification-templates/:id', component: EditNotificationTemplatesComponent,
        canActivate: [NgxPermissionsGuard],
        data: {
          permissions: {
            only: ['notification-templates_edit'],
            redirectTo: 'admin/no-permission'
          }
        }
      },
      { path: 'change-password', component: ChangePasswordComponent },
      {
        path: 'sitesettings', component: SitesettingsComponent,
        canActivate: [NgxPermissionsGuard],
        data: {
          permissions: {
            only: ['sitesettings_view'],
            redirectTo: 'admin/no-permission'
          }
        }
      },
      {
        path: 'sitesettings-edit/:id', component: SitesettingsEditComponent,
        canActivate: [NgxPermissionsGuard],
        data: {
          permissions: {
            only: ['sitesettings_edit'],
            redirectTo: 'admin/no-permission'
          }
        }
      },
      { path: 'admin-role', component: AdminRoleComponent },
      { path: 'add-admin-role', component: AddeditAdminRoleComponent },
      { path: 'admin-role-permission/:id', component: AdminRolePermissionComponent },
      { path: 'profile', component: ProfileComponent },
      { path: 'front-menu', component: FrontEndMenuManagementComponent },
      { path: 'add-front-menu', component: AddMenuComponent },
      { path: 'edit-front-menu/:id', component: AddMenuComponent },
      { path: 'influencer-details/:id', component: InfluencerDetailsComponent },
      { path: 'influencer-job-details/:id/:influencer', component: JobDetailsComponent},
      { path: 'campaign/:id', component: CampaignComponent},
      { path: 'campaign', component: CampaignComponent},
      { path: 'campaign-list/:id', component: CampaignListComponent},
      { path: 'campaign-details/:id', component: CampaignDetailsComponent},
      { path: 'add-campaign', component: CampaignAddEditComponent},
      { path: 'edit-campaign/:id', component: CampaignAddEditComponent},
      { path: 'email-template', component: EmailTemplateComponent},
      { path: 'testimonial', component: TestimonialComponent},
      { path: 'add-email-template', component: EmailTemplateAddComponent},
      { path: 'add-testimonial', component: AddTestimonialComponent},
      { path: 'edit-email-template/:id', component:EmailTemplateAddComponent},
      { path: 'edit-testimonial/:id', component: AddTestimonialComponent},
      { path: 'message-conversation/:id', component: MessageConversationComponent},
      { path: 'brand-job-details/:id/:brandowner', component: BrandJobDetailsComponent},
      
      


    ]
  },
];
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule],
  declarations: []
})
export class AdminRoutingModule { }
