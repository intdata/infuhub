import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, ControlContainer, FormGroup, FormControl, AsyncValidatorFn, AbstractControl, ValidationErrors } from '@angular/forms';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Router, ActivatedRoute } from "@angular/router";
import { CampaignService } from './../../services/campaign.service';
import { CommonService } from './../../services/common.service';
@Component({
  selector: 'app-message-conversation',
  templateUrl: './message-conversation.component.html',
  styleUrls: ['./message-conversation.component.scss']
})
export class MessageConversationComponent implements OnInit {

  constructor(private campaignService: CampaignService, private commonService: CommonService, private router: Router, private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder) { }

  id;
  messageList={};
  campaignData={};
  campain_name: any ;

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      this.id = params['id'];
    });

    this.campaignService.getCampaignData(this.id).subscribe(data => {
      this.campaignData = data.results;
     
      this.campain_name=this.campaignData[0].campaign_name;
    });

    this.campaignService.assignInfluencerByCampaign(this.id).subscribe(data => {
      this.messageList = data.results;
     
    });
  }

 

  show_hide_message = function (event,i) {  
    if( document.getElementById("viewMessage_"+i).style.display == "block")
    {
      document.getElementById("viewMessage_"+i).style.display = "none";
    }else{
      document.getElementById("viewMessage_"+i).style.display = "block";
    }
   
   
}

}
