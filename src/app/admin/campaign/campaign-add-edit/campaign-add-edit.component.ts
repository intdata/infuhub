import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, ControlContainer, FormGroup, FormControl, AsyncValidatorFn, AbstractControl, ValidationErrors } from '@angular/forms';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Router, ActivatedRoute } from "@angular/router";
import { CampaignService } from './../../services/campaign.service';
import { CommonService } from './../../services/common.service';
@Component({
  selector: 'app-campaign-add-edit',
  templateUrl: './campaign-add-edit.component.html',
  styleUrls: ['./campaign-add-edit.component.scss']
})
export class CampaignAddEditComponent implements OnInit {
  campaignForm: FormGroup;
  submitted = false;
  campaignData;
  id;
  campaign_name;
  social_media;
  campaign_details;
  campaign_post_date;
  campaign_start_date;
  campaign_end_date;
  total_investment;
  roi_of_campaign;
  populer_region;
  potential_impressions;
  status;
  action_name;

  debouncer: any;
  constructor(private campaignService: CampaignService, private commonService: CommonService, private router: Router, private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder) {
  }
  ngOnInit() {
    this.status = 'Active';
    this.activatedRoute.params.subscribe(params => {
      this.id = params['id'];
    });
    if (this.id > 0) {
      this.campaignService.singleData(this.id).subscribe(data => {
        if (data.msgcode == 1) {
            this.id = data.results.id;
            this.campaign_name = data.results.campaign_name;
            this.campaign_details = data.results.campaign_details;
            this.social_media = data.results.platform_id;
            this.campaign_post_date = data.results.post_date;
            this.campaign_start_date = data.results.start_date;
            this.campaign_end_date = data.results.end_date;
            this.total_investment = data.results.total_amount_invested;
            this.roi_of_campaign = data.results.roi;
            this.populer_region = data.results.region;
            this.potential_impressions = data.results.impression;
            this.status = data.results.status;            
        }
    });


      this.action_name = 'Edit';
      this.campaignForm = new FormGroup({
        id: new FormControl(''),
        campaign_name: new FormControl('', <any>Validators.required),
        campaign_details: new FormControl('', <any>Validators.required),
        campaign_post_date: new FormControl('', <any>Validators.required),
        campaign_start_date: new FormControl('', <any>Validators.required),
        campaign_end_date: new FormControl('', <any>Validators.required),
        total_investment: new FormControl('', <any>Validators.required),
        roi_of_campaign: new FormControl('', <any>Validators.required),
        social_media: new FormControl(''),
        no_of_tag: new FormControl(''),
        num_follwer: new FormControl(''),
        no_like_comnt: new FormControl(''),
        populer_region: new FormControl('', <any>Validators.required),
        potential_impressions: new FormControl(''),
        status: new FormControl('', <any>Validators.required)
      });
    } else {
      this.action_name = 'Add';
      this.campaignForm = new FormGroup({
        id: new FormControl(''),
        campaign_name: new FormControl('', <any>Validators.required),
        campaign_details: new FormControl('', <any>Validators.required),
        campaign_post_date: new FormControl('', <any>Validators.required),
        campaign_start_date: new FormControl('', <any>Validators.required),
        campaign_end_date: new FormControl('', <any>Validators.required),
        total_investment: new FormControl('', <any>Validators.required),
        roi_of_campaign: new FormControl('', <any>Validators.required),
        social_media: new FormControl(''),
        no_of_tag: new FormControl(''),
        num_follwer: new FormControl(''),
        no_like_comnt: new FormControl(''),
        populer_region: new FormControl('', <any>Validators.required),
        potential_impressions: new FormControl(''),
        status: new FormControl('', <any>Validators.required)
      });
    }
  }
  get f() { return this.campaignForm.controls; }
  onSubmit() {
    this.submitted = true;
    if (this.campaignForm.invalid) {
      return;
    } else {

      this.campaignService.editData(this.campaignForm.value).subscribe(data => {
        if (data.msgcode == 1 && data.results.affectedRows > 0) {
          this.router.navigate(['/admin/campaign']);
        }
      });
      //this.router.navigate(['/admin/campaign-list']);
    }
  }
  cancelForm = function () {
    this.router.navigate(['/admin/campaign']);
  }
}
