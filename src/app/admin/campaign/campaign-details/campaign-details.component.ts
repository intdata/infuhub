import { Component, OnInit, Pipe, PipeTransform } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { CampaignService } from './../../services/campaign.service';

@Component({
  selector: 'app-campaign-details',
  templateUrl: './campaign-details.component.html',
  styleUrls: ['./campaign-details.component.scss']
})
export class CampaignDetailsComponent implements OnInit {

  constructor(private campaignService : CampaignService, private router: Router, private activatedRoute: ActivatedRoute) { }
  id;
  record;
  influencerData;
  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      this.id = params['id'];
    });
    this.campaignService.singleData(this.id).subscribe(data => {
      this.record = data.results;
      this.influencerData = data.influencerResult;
      //console.log(data);
      console.log(data.influencerResult);
    });
  }

}


/*@Pipe({ name: 'dateSuffix' })*/
export class DateSuffix implements PipeTransform {
    transform(value: string): string {

    let suffix = 'th',
        day = value;

        if (day === '1' || day === '21' || day === '31') {
            suffix = 'st'
        } else if (day === '2' || day === '22') {
            suffix = 'nd';
        } else if (day === '3' || day === '23') {
           suffix = 'rd';
        }

        return suffix;

    }
}
