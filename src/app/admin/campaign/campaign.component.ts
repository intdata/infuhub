import { Component, OnInit, OnDestroy, AfterViewInit, ViewChild } from '@angular/core';
import { FormBuilder, Validators, ControlContainer, FormGroup, FormControl } from '@angular/forms';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Router, ActivatedRoute } from "@angular/router";
import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';
import { jsonpFactory } from '@angular/http/src/http_module';
import { CampaignService } from '../services/campaign.service';
@Component({
  selector: 'app-campaign',
  templateUrl: './campaign.component.html',
  styleUrls: ['./campaign.component.scss']
})
export class CampaignComponent implements OnInit, OnDestroy, AfterViewInit {
  console = console;
  brandOwnerData: any[] = [];
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: any = {};
  dtTrigger: Subject<any> = new Subject();
  id: any;
  brand_owner_serach_field = false;
  sucees_message = "";
  appove_button = true;
  brandOwnerList;
  campaign_status;
  campaignData: any[] = [];
  constructor(private campaignService: CampaignService, private router: Router, private activatedRoute: ActivatedRoute) { }
  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      this.id = params['id'];
    });
    this.dtOptions = {
      pagingType: 'full_numbers',
      //pageLength: 10,
      //lengthMenu: [10, 50, 100],
      // order: [[ 4, 'desc' ]],
      columnDefs: [
        {
          // Target the actions column
          // targets: [4],
          // responsivePriority: 1,
          // filterable: false,
          // sortable: true,
          // orderable: true,
          targets: [5],
          responsivePriority: 1,
          filterable: false,
          sortable: false,
          orderable: false
        }
      ]
    };
  }
  ngAfterViewInit(): void {
    setTimeout(() => {
      if (this.id > 0) {
        this.campaignService.influncerData(this.id).subscribe(data => {
         
          if (data.msgcode == 1) {
            this.campaignData = data.results;
            //console.log(this.campaignData);
            this.dtTrigger.next();
            this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
              dtInstance.columns().every(function () {
                const that = this;
                $('input', this.footer()).on('keyup change', function () {
                  if (that.search() !== this['value']) {
                    that.search(this['value']).draw();
                  }
                });
              });
            });
          }
        });
        //this.brand_owner_serach_field = false;
        //this.brandOwnerData = JSON.parse(
        // '[{ "id": 31, "company_name":"samsung India Pvt Ltd", "campaign_name": "Website Build", "industry": "04-02-2019", "country": "Facebook | Twitter","company_ph_no": "04-03-2019 ", "email": "04-05-2019", "status": "Published", "created_at": "2019-02-28T10:01:12.000Z" }, { "id": 31,"company_name":"samsung India Pvt Ltd", "campaign_name": "Advertisement", "industry": "04-02-2019", "country": "Youtube","company_ph_no": "04-03-2019", "email": "04-05-2019", "status": "Unapproved", "created_at": "2019-02-28T10:01:12.000Z" },{ "id": 31, "company_name":"samsung India Pvt Ltd", "campaign_name": "Image Creation For Social Network", "industry": "04-01-2019", "country": "Youtube","company_ph_no": "04-03-2019", "email": "14-03-2019", "status": "Announced", "created_at": "2019-02-28T10:01:12.000Z" }, { "id": 31, "company_name":"samsung India Pvt Ltd", "campaign_name": "Video Creation For Youtube", "industry": "04-02-2018", "country": "Youtube|Twitter","company_ph_no": "04-03-2019", "email": "04-05-2019", "status": "Accepted", "created_at": "2019-02-28T10:01:12.000Z" }, { "id": 31, "company_name":"samsung India Pvt Ltd", "campaign_name": "Website Development", "industry": "01-02-2019", "country": "Facebook","company_ph_no": "04-03-2019", "email": "04-05-2019", "status": "Approved", "created_at": "2019-02-28T10:01:12.000Z" }, { "id": 31, "company_name":"samsung India Pvt Ltd", "campaign_name": "Data Manupulation", "industry": "04-02-2018", "country": "Twitter","company_ph_no": "04-03-2019", "email": "04-05-2019", "status": "Ready To Launch", "created_at": "2019-02-28T10:01:12.000Z" }]');
      } else {
        //this.brand_owner_serach_field =true;
        this.campaignService.allData().subscribe(data => {
          
          if (data.msgcode == 1) {
            this.campaignData = data.results;
            this.dtTrigger.next();
            this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
              dtInstance.columns().every(function () {
                const that = this;
                $('input', this.footer()).on('keyup change', function () {
                  if (that.search() !== this['value']) {
                    that.search(this['value']).draw();
                  }
                });
              });
            });
          }
        });
      }
    });
    //this.dtTrigger.next();
    // this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
    //   dtInstance.columns().every(function () {
    //     const that = this;
    //     $('input', this.footer()).on('keyup change', function () {
    //       if (that.search() !== this['value']) {
    //         that.search(this['value']).draw();
    //       }
    //     });
    //   });
    // });
  }
  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }
  changeStatus = function (id) {
    if (confirm("Are you sure want to appove campaign? ")) {
      
      this.campaignService.changeStatus(id).subscribe(data => {
        if (data.msgcode == 1) {
          this.campaignData = data.results;
          this.sucees_message = "Campaign has been successfully announced";
          //this.appove_button = false;
          //this.dtTrigger.next();
        }
      });
  
    } else {
      this.brand_owner_serach_field = true;
      this.brandOwnerData = JSON.parse(
        '[{ "id": 31, "company_name":"samsung India Pvt Ltd123456", "campaign_name": "Website Build", "industry": "04-02-2019", "country": "Facebook | Twitter","company_ph_no": "04-03-2019 ", "email": "04-05-2019", "status": "Published", "created_at": "2019-02-28T10:01:12.000Z" }, { "id": 31,"company_name":"Tcs Services Pvt Ltd", "campaign_name": "Advertisement", "industry": "04-02-2019", "country": "Youtube","company_ph_no": "04-03-2019", "email": "04-05-2019", "status": "Announced", "created_at": "2019-02-28T10:01:12.000Z" },{ "id": 31, "company_name":"samsung India Pvt Ltd", "campaign_name": "Image Creation For Social Network", "industry": "04-01-2019", "country": "Youtube","company_ph_no": "04-03-2019", "email": "14-03-2019", "status": "Announced", "created_at": "2019-02-28T10:01:12.000Z" }, { "id": 31, "company_name":"cognizant Services Pvt Ltd", "campaign_name": "Video Creation For Youtube", "industry": "04-02-2018", "country": "Youtube|Twitter","company_ph_no": "04-03-2019", "email": "04-05-2019", "status": "Accepted", "created_at": "2019-02-28T10:01:12.000Z" }, { "id": 31, "company_name":"indusnet Technology Pvt Ltd", "campaign_name": "Website Development", "industry": "01-02-2019", "country": "Facebook","company_ph_no": "04-03-2019", "email": "04-05-2019", "status": "Approved", "created_at": "2019-02-28T10:01:12.000Z" }, { "id": 31, "company_name":"samsung India Pvt Ltd", "campaign_name": "Data Manupulation", "industry": "04-02-2018", "country": "Twitter","company_ph_no": "04-03-2019", "email": "04-05-2019", "status": "Ready To Launch", "created_at": "2019-02-28T10:01:12.000Z" }]');
    }
    //this. brandOwnerList.status ='Appp';
    //var x=   document.getElementById("campaign_status_"+id);
  }
}
