import { DatePipe } from '@angular/common';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'dateTimeFormat'
})
export class DateTimeFormatPipe extends DatePipe implements PipeTransform {
  transform(value: any, args?: any, args1?: any): any {
    if(value){
      if(args1 == 'date-month'){
        //return super.transform(this.convertUTCDateToLocalDate(value, args), 'MMM d');
        return super.transform(value, 'MMM d');
      } else if(args1 == 'date-month-year'){
        //return super.transform(this.convertUTCDateToLocalDate(value, args), 'MMM d');
        return super.transform(value, 'd MMM y');
      } else{
        return super.transform(value, 'MMM d, y, hh:mm a');
        //return super.transform(this.convertUTCDateToLocalDate(value, args), 'MMM d, y, hh:mm a');
        //return this.convertUTCDateToLocalDate(value, args).toLocaleString();
      }
    }
  }

  convertUTCDateToLocalDate(timestamp, timeOffset) {
    var date = new Date(Number(timestamp));

    //var newDate = new Date(date.getTime()+date.getTimezoneOffset()*60*1000);
    var newDate = new Date(Number(timestamp)+Number(timeOffset)*60*1000);

    var offset = Number(timeOffset) / 60;
    var hours = date.getHours();

    if (this.isDstObserved(date)) { 
      hours--;
    }

    newDate.setHours(hours - offset);

    return newDate;   
  }

  stdTimezoneOffset (date) {
    var jan = new Date(date.getFullYear(), 0, 1);
    var jul = new Date(date.getFullYear(), 6, 1);
    return Math.max(jan.getTimezoneOffset(), jul.getTimezoneOffset());
  }

  isDstObserved(date) {
    return date.getTimezoneOffset() < this.stdTimezoneOffset(date);
  }
}