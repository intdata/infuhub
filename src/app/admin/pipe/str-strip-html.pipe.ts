import { Pipe, PipeTransform } from '@angular/core';
@Pipe({
  name: 'strStripHtml'
})
export class StrStripHtmlPipe implements PipeTransform {
  transform(value: string): any {
    return value.replace(/<.*?>/g, ''); // replace tags
  }
}
