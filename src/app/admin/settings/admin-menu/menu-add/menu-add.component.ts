import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, ControlContainer, FormGroup, FormControl } from '@angular/forms';
import { Http,Response, Headers, RequestOptions } from '@angular/http';
import { Router,ActivatedRoute} from "@angular/router";
import { AdminMenuService } from '../../../services/admin-menu.service';

@Component({
  selector: 'app-menu-add',
  templateUrl: './menu-add.component.html',
  styleUrls: ['./menu-add.component.scss']
})
export class MenuAddComponent implements OnInit {

  submitted = false;	
  addmenuForm: FormGroup;
  id;
  menu_name;
  menu_slug;
  menu_order;
  menu_group_name;
  status="";
  isReadOnly;
  parent_id=0;
  menuGroupData;
  actionName;

  constructor(private adminMenuService:AdminMenuService, private router:Router,private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder) { }

  ngOnInit() {
  		
  		this.addmenuForm = new FormGroup({
  			    id: new FormControl(''),
            parent_id: new FormControl('',< any > Validators.required),
            menu_name: new FormControl('', < any > Validators.required),
            menu_slug: new FormControl('', < any > Validators.required),
            menu_order: new FormControl('', < any > Validators.required),
            menu_group_name: new FormControl(''),
            status: new FormControl('', < any > Validators.required)
        });
        this.activatedRoute.params.subscribe(params => {
            this.id = params['id'];
        });
        if(this.id > 0){
          this.actionName  = "Edit";
        	this.isReadOnly = true;               
	        this.adminMenuService.getSingleMenuData(this.id).subscribe(data => {
            this.parent_id      = data.results.parent_id;
	        	this.menu_name 			= data.results.menu_name;
	        	this.menu_slug 			= data.results.menu_slug;
	        	this.menu_group_name 	= data.results.menu_group_name;
	        	this.menu_order 		= data.results.menu_order;
	        	this.status 			  = data.results.status;
	        });
        }
        else{
          this.actionName = "Add";
        	this.id = 0; 
        }

      this.adminMenuService.getMenuGroupData().subscribe(data=>{
        if(data.msgcode==1 && data.results.length >0){
            this.menuGroupData = data.results;
        }        
      });
  }

  onSubmit() {
  		this.submitted = true;
  		if (this.addmenuForm.invalid) {
            return false;
        }
        else {
		  	this.adminMenuService.addMenuData(this.addmenuForm.value).subscribe(data => {
		        if (data.msgcode == 1 && data.results.affectedRows > 0) {
		            this.router.navigate(['/admin/menu']);
		        }
		    });
	  	}
  }

  get f() { return this.addmenuForm.controls; }

}
