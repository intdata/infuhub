import { Component, OnInit, ViewChild } from '@angular/core';
import { Http,Response, Headers, RequestOptions } from '@angular/http';
import { Router,ActivatedRoute} from "@angular/router";
import { Subject } from 'rxjs'; 
import { DataTableDirective } from 'angular-datatables';
import { AdminMenuService } from '../../services/admin-menu.service';

@Component({
  selector: 'app-admin-menu',
  templateUrl: './admin-menu.component.html',
  styleUrls: ['./admin-menu.component.scss']
})
export class AdminMenuComponent implements OnInit {

	menuData;
	menuGroupData;
	@ViewChild(DataTableDirective)
	dtElement: DataTableDirective;
	dtOptions: any = {};
	dtTrigger: Subject<any> = new Subject();

	constructor(private adminMenuService:AdminMenuService, private router:Router) { }

	ngOnInit() {
		this.dtOptions = {
			pageLength: 10,   
			columnDefs : [
			    {
			        // Target the actions column
			        targets           : [4],
			        responsivePriority: 1,
			        filterable        : false,
			        sortable          : false,
			        orderable: false
			    }                
			]
		};
	}

	ngAfterViewInit(): void {
		this.adminMenuService.getMenuData().subscribe(data=>{
		    if(data.msgcode==1 && data.results.length >0){
		        this.menuData = data.results;
		        this.dtTrigger.next();
		    }		     
		});
	}	

	ngOnDestroy(): void {
		this.dtTrigger.unsubscribe();
	}
}
