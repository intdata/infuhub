import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FrontEndMenuManagementComponent } from './front-end-menu-management.component';

describe('FrontEndMenuManagementComponent', () => {
  let component: FrontEndMenuManagementComponent;
  let fixture: ComponentFixture<FrontEndMenuManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FrontEndMenuManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FrontEndMenuManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
