import { Component, OnInit,OnDestroy  ,AfterViewInit,ViewChild } from '@angular/core';
import { Http,Response, Headers, RequestOptions } from '@angular/http';
import { Router,ActivatedRoute} from "@angular/router";
import { Subject } from 'rxjs'; 
import { DataTableDirective } from 'angular-datatables';
import { FrontMenuService } from '../../services/front-menu.service';

@Component({
  selector: 'app-front-end-menu-management',
  templateUrl: './front-end-menu-management.component.html',
  styleUrls: ['./front-end-menu-management.component.scss']
})
export class FrontEndMenuManagementComponent implements OnInit,OnDestroy,AfterViewInit {
  menuData:any[]=[];
	menuGroupData:any[]=[];
	@ViewChild(DataTableDirective)
	dtElement: DataTableDirective;
	dtOptions: any = {};
	dtTrigger: Subject<any> = new Subject();
	
	constructor(private frontMenuService:FrontMenuService, private router:Router) { }

	ngOnInit() {
		this.dtOptions = {
			pageLength: 10,	  
			columnDefs : [
			    {
			        // Target the actions column
			        targets           : [5],
			        responsivePriority: 1,
			        filterable        : false,
			        sortable          : false,
			        orderable: false
			    }                
			]
		};
	}

	ngAfterViewInit(): void {
		this.frontMenuService.getMenuData().subscribe(data=>{
		    if(data.msgcode==1 ){
					//console.log(JSON.stringify(data.results));
		        this.menuData = data.results;
		        this.dtTrigger.next();
		    }		     
		});
	}	

	ngOnDestroy(): void {
		this.dtTrigger.unsubscribe();
	}

	
	deleteMenu = function(id){
		this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
		if(confirm("Are you sure to delete Menu? ")) {
		   this.frontMenuService.deleteMenu(id).subscribe(data => { 
			   if(data.msgcode==1 && data.results.affectedRows >0)
				{         
					dtInstance.destroy();
					this.ngAfterViewInit();
			   }
			 });
		   }
		});
		} 

		changeStatus = function(status){
    
			jQuery('#chnageStatusId').text('Inactive');
			// this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
			// if(confirm("Are you sure to delete Menu? ")) {
			// 	 this.frontMenuService.deleteMenu(id).subscribe(data => { 
			// 		 if(data.msgcode==1 && data.results.affectedRows >0)
			// 		{         
			// 			dtInstance.destroy();
			// 			this.ngAfterViewInit();
			// 		 }
			// 	 });
			// 	 }
			// });



			} 
		
		

}

