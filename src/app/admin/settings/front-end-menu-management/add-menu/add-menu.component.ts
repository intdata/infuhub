import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, ControlContainer, FormGroup, FormControl } from '@angular/forms';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Router, ActivatedRoute } from "@angular/router";
import { FrontMenuService } from '../../../services/front-menu.service';
@Component({
  selector: 'app-add-menu',
  templateUrl: './add-menu.component.html',
  styleUrls: ['./add-menu.component.scss']
})
export class AddMenuComponent implements OnInit {
  isMenuLinkOpen = false;
  isPageListOpen = false;
  submitted = false;
  addmenuForm: FormGroup;
  id: number;
  menu_position;
  menu_name: any;
  menu_link: any;
  page_list_name = "";
  link_type: any;
  menu_order: any;
  status = "";
  parent_id = 0;
  menu_icon_class: any;
  isReadOnly: boolean;
  menuGroupData: any;
  allPagesData: any;
  header_position = 0;
  footer_position = 0;
  link_type_external = 0;
  link_type_internal = 0;
  constructor(private frontMenuService: FrontMenuService, private router: Router, private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder) { }
  ngOnInit() {
    if (this.id > 0) {
      this.addmenuForm = this.formBuilder.group({
        id: new FormControl(''),
        menu_position: new FormControl('', [Validators.required]),
        parent_id: new FormControl(''),
        menu_name: new FormControl('', <any>Validators.required),
        menu_link: new FormControl(''),
        page_list_name: new FormControl(''),
        link_type: new FormControl('', <any>Validators.required),
        menu_order: new FormControl('', <any>Validators.required),
        menu_icon_class: new FormControl(''),
        status: new FormControl('', <any>Validators.required)
      });
    } else {
      this.addmenuForm = this.formBuilder.group({
        id: new FormControl(''),
        menu_position: new FormControl('', [Validators.required]),
        parent_id: new FormControl(''),
        menu_name: new FormControl('', <any>Validators.required),
        menu_link: new FormControl('', <any>Validators.required),
        page_list_name: new FormControl('', <any>Validators.required),
        link_type: new FormControl('', <any>Validators.required),
        menu_order: new FormControl('', <any>Validators.required),
        menu_icon_class: new FormControl(''),
        status: new FormControl('', <any>Validators.required)
      });
    }
    this.activatedRoute.params.subscribe(params => {
      this.id = params['id'];
    });
    //==================== get all CMS Pages======================//
    this.frontMenuService.getPageList('').subscribe((data: { msgcode: number; results: { length: number; }; }) => {
      if (data.msgcode == 1 && data.results.length > 0) {
        this.allPagesData = data.results;
        //this.router.navigate(['/admin/menu']);
      }
    });
    if (this.id > 0) {
      this.isReadOnly = true;
      this.frontMenuService.getSingleMenuData(this.id).subscribe(data => {
        if (data.results.menu_position == 0) {
          this.header_position = 1;
        } else if (data.results.menu_position == 1) {
          this.footer_position = 1;
        } else {
          this.header_position = 0;
          this.footer_position = 0
        }
        if (data.results.menu_link_type == 1) {
          this.link_type_external = 1;
          this.isPageListOpen = false;
          this.isMenuLinkOpen = true;
        } else if (data.results.menu_link_type == 2) {
          this.link_type_internal = 1;
          this.isPageListOpen = true;
          this.isMenuLinkOpen = false;
        } else {
          this.link_type_external = 0;
          this.link_type_internal = 0;
          this.isPageListOpen = false;
          this.isMenuLinkOpen = false;
        }
        this.menu_position = data.results.menu_position;
        this.parent_id = data.results.parent_id;
        this.menu_name = data.results.menu_label;
        this.menu_link = data.results.menu_link;
        this.link_type = data.results.menu_link_type;
        this.menu_order = data.results.menu_order;
        this.menu_icon_class = data.results.menu_icon;
        this.status = data.results.status;
      });
    }
    else {
      this.id = 0;
    }
    this.frontMenuService.getFrontParentMenuList().subscribe(data => {
      if (data.msgcode == 1 && data.results.length > 0) {
        this.menuGroupData = data.results;
      }
    });
  }
  get f() {
    return this.addmenuForm.controls;
  };
  onSubmit() {

    console.log(this.addmenuForm.value);
    if (this.addmenuForm.get('link_type').value == 2) {
      this.addmenuForm.controls['menu_link'].clearValidators();
      this.addmenuForm.get('menu_link').updateValueAndValidity();
    } else {
      this.addmenuForm.controls['page_list_name'].clearValidators();
      this.addmenuForm.get('page_list_name').updateValueAndValidity();
    }
    this.submitted = true;
    if (this.addmenuForm.invalid) {
      return false;
    }
    else {
      this.frontMenuService.addFrontMenuData(this.addmenuForm.value).subscribe(data => {
        if (data.msgcode == 1 && data.results.affectedRows > 0) {
          this.router.navigate(['/admin/front-menu']);
        }
      });
    }
  }
  Cancel = function () {
    this.router.navigate(['/admin/front-menu']);
  }
  checkLinkType = function (value: number) {
    if (value == 1) {
      this.isPageListOpen = false;
      this.isMenuLinkOpen = true;
    } else if (value == 2) {
      this.isPageListOpen = true;
      this.isMenuLinkOpen = false;
    } else {
      this.isPageListOpen = false;
      this.isMenuLinkOpen = false;
    }
  }
}
