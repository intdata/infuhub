import { Component, OnInit, OnDestroy, AfterViewInit, ViewChild } from '@angular/core';
import { FormBuilder, Validators, ControlContainer, FormGroup, FormControl } from '@angular/forms';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Router } from "@angular/router";
import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';
import { AdminService } from '../../services/admin.service';

@Component({
  selector: 'app-admin-role',
  templateUrl: './admin-role.component.html',
  styleUrls: ['./admin-role.component.scss']
})
export class AdminRoleComponent implements OnInit, OnDestroy, AfterViewInit {
  adminRoleData: any[] = [];
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: any = {};
  dtTrigger: Subject<any> = new Subject();
  constructor(private adminService: AdminService, private router: Router) { }
  ngOnInit() {
    this.dtOptions = {
      //pagingType: 'full_numbers',
      pageLength: 10,
      columnDefs: [
        {
          // Target the actions column
          targets: [2],
          responsivePriority: 1,
          filterable: false,
          sortable: false,
          orderable: false
        }
      ]
      //processing: true
    };


  }


  ngAfterViewInit(): void {
    
    this.adminService.getAdminRoleData().subscribe(data => {
      if (data.msgcode == 1) {
        //console.log(JSON.stringify(data.results));
        this.adminRoleData = data.results;
        this.dtTrigger.next();
      }
    });
  }
  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }
  deleteAdmin = function (id) {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      if (confirm("Are you sure to delete? ")) {
        this.adminService.deleteAdminData(id).subscribe(data => {
          if (data.msgcode == 1 && data.results.affectedRows > 0) {
            dtInstance.destroy();
            this.ngAfterViewInit();
          }
        });
      }
    });
  }
}
