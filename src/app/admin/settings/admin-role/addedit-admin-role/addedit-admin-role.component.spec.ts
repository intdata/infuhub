import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddeditAdminRoleComponent } from './addedit-admin-role.component';

describe('AddeditAdminRoleComponent', () => {
  let component: AddeditAdminRoleComponent;
  let fixture: ComponentFixture<AddeditAdminRoleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddeditAdminRoleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddeditAdminRoleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
