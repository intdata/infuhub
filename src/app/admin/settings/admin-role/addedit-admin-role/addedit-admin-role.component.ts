import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, ControlContainer, FormGroup, FormControl, AsyncValidatorFn, AbstractControl, ValidationErrors } from '@angular/forms';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Router, ActivatedRoute } from "@angular/router";
import { AdminService } from '../../../services/admin.service';
import { Observable } from "rxjs/Observable";
@Component({
  selector: 'app-addedit-admin-role',
  templateUrl: './addedit-admin-role.component.html',
  styleUrls: ['./addedit-admin-role.component.scss']
})
export class AddeditAdminRoleComponent implements OnInit {
  allRoleData;
  addadminForm: FormGroup;
  submitted = false;
  id;
  role_name;
  status;

  constructor(private adminService: AdminService, private router: Router, private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder) { }
  ngOnInit() {
    this.status = 'Active';
    this.activatedRoute.params.subscribe(params => {
      this.id = params['id'];
    });

    
    this.addadminForm = new FormGroup({
      id: new FormControl(''),
      role_name: new FormControl('', <any>Validators.required),
      status: new FormControl('', <any>Validators.required)
    });

  } 
  // validateAreEqual('password');
  get f() { return this.addadminForm.controls; }
  onSubmit() {
    this.submitted = true;
    if (this.addadminForm.invalid) {
      return;
    } {
      
      this.adminService.saveAdminRole(this.addadminForm.value).subscribe(data => {
        if (data.msgcode == 1 && data.results.affectedRows > 0) {
          this.router.navigate(['/admin/admin-role']);
        }
      });
    }
  }
   cancelForm = function () {
    this.router.navigate(['/admin/admin-role']);
  }

}

