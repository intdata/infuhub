import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, ControlContainer, FormGroup, FormControl } from '@angular/forms';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Router, ActivatedRoute } from "@angular/router";
import { AdminService } from '../../services/admin.service';

@Component({
  selector: 'app-sitesettings',
  templateUrl: './sitesettings.component.html',
  styleUrls: ['./sitesettings.component.scss']
})
export class SitesettingsComponent implements OnInit {
  sitesetting;
  public currencyName: string;
  currencyId: number;

  constructor(private adminService: AdminService, private router: Router, private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.adminService.setesettingValue().subscribe(data => {
      //console.log(JSON.stringify(data.results))
      this.sitesetting = data.results;
      data.results.forEach(element => {
        //console.log(element.id);
        if (element.id == '3') {
          this.currencyId = element.setting_value;
          this.adminService.getCurrencyName(this.currencyId).subscribe(data => {
            //console.log(JSON.stringify(data.results))
            this.currencyName = data.results.name;
            console.log(this.currencyName);
            
          });
        }
        
      });
    });
    console.log(this.currencyName);
    
  }

}
