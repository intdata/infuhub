import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SitesettingsEditComponent } from './sitesettings-edit.component';

describe('SitesettingsEditComponent', () => {
  let component: SitesettingsEditComponent;
  let fixture: ComponentFixture<SitesettingsEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SitesettingsEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SitesettingsEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
