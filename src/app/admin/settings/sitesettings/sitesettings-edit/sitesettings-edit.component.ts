import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, ControlContainer, FormGroup, FormControl, AsyncValidatorFn, AbstractControl, ValidationErrors } from '@angular/forms';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Router, ActivatedRoute } from "@angular/router";
import { AdminService } from '../../../services/admin.service';
import { CommonService } from '../../../services/common.service';

@Component({
  selector: 'app-sitesettings-edit',
  templateUrl: './sitesettings-edit.component.html',
  styleUrls: ['./sitesettings-edit.component.scss']
})
export class SitesettingsEditComponent implements OnInit {
  id;
  setting_name;
  setting_value;
  sitesettingForm;
  submitted = false;
  public allCurrencies: any[] = [];
  constructor(private adminService: AdminService,private commonService: CommonService,  private router: Router, private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      this.id = params['id'];
    });
    this.sitesettingForm = new FormGroup({
      id: new FormControl(),
      setting_name: new FormControl('', Validators.required),
      setting_value: new FormControl('', Validators.required)
    });
    this.adminService.getSitesettingData(this.id).subscribe(data => {
      this.setting_name = data.results.setting_name;
      this.setting_value = data.results.setting_value;
    });

     // get all currencies
     this.getAllCurrencies();
  }
  get f() { return this.sitesettingForm.controls; }
  onSubmit() {
    this.submitted = true;
    if (this.sitesettingForm.invalid) {
      return;
    } else {
      this.adminService.updateSitesetting(this.sitesettingForm.value).subscribe(data => {
        this.router.navigate(['/admin/sitesettings']);
      });
    }
  }

  cancelForm = function () {
    this.router.navigate(['/admin/sitesettings']);
  }

    /**
   * Method to get all currencies
   */
  getAllCurrencies(){
    // call service to get currency list
    this.commonService.getAllCurrencies().subscribe(
        (response: any) => {
            //console.log(response);
            if (response.msgcode === 1) {
                this.allCurrencies = response.results;
                //console.log(this.allCurrencies);
            } else {
                console.log('get currency list error: ', response.responseDetails);
            }
        },
        error => {
            console.log('get currency list error: ', error);
        }
    );
  }


}
