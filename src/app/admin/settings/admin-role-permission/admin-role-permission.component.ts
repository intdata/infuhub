import { Component, OnInit } from '@angular/core';
import { Http,Response, Headers, RequestOptions } from '@angular/http';
import { Router,ActivatedRoute} from "@angular/router";
import { AdminMenuService } from '../../services/admin-menu.service';

@Component({
  selector: 'app-admin-role-permission',
  templateUrl: './admin-role-permission.component.html',
  styleUrls: ['./admin-role-permission.component.scss']
})
export class AdminRolePermissionComponent implements OnInit {

  constructor(private adminMenuService:AdminMenuService, private activatedRoute: ActivatedRoute) { }
  data = {};
  permissionData = [];
  menuPermissionData;
  roleId;
  roleTypeName: any;

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
            this.roleId = params['id'];
        });     
    this.adminMenuService.getMenuPermissionDataByRoleId(this.roleId).subscribe(data=>{
      //console.log(JSON.stringify(data.results));
        this.menuPermissionData = data.results;                
    });

    this.adminMenuService.getAdminRoleTypeName(this.roleId).subscribe(data=>{
      //console.log(data.results.role_name);
      this.roleTypeName = data.results.role_name;                
    });
  }
  //====================================//
  permission(menu_id,action,event){
  	this.data['role_id']      = this.roleId;
  	this.data['menu_id']       = menu_id;
    this.data['permission_action']  = action;
    if(event.target.checked == true){
      this.data['is_check'] = 1;
    }
    else{
      this.data['is_check'] = 0;
    }
  	this.adminMenuService.rolePermission(this.data).subscribe(data=>{});  	
   
  }

}

