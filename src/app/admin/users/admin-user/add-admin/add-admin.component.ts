import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, ControlContainer, FormGroup, FormControl, AsyncValidatorFn, AbstractControl, ValidationErrors } from '@angular/forms';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Router, ActivatedRoute } from "@angular/router";
import { AdminService } from '../../../services/admin.service';
import { CommonService } from '../../../services/common.service';
import { Observable } from "rxjs/Observable";
@Component({
    selector: 'app-add-admin',
    templateUrl: './add-admin.component.html',
    styleUrls: ['./add-admin.component.scss']
})
export class AddAdminComponent implements OnInit {
    //var status ="Active" ;
    //nrSelect:string = "Active" 
    adminEmailData={};
    allRoleData;
    addadminForm: FormGroup;
    submitted = false;
    isPassword = true;
    isCon_Password = true;

    id;
    admin_role_type="";
    fname;
    lname;
    profile_img;
    email;
    status;
  
    constructor(private adminService: AdminService,private commonService: CommonService, private router: Router, private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder) { }
   
    filesToUpload: Array<File> = [];
    fileChangeEvent(fileInput: any) {
      this.filesToUpload = <Array<File>>fileInput.target.files;
    }
    ngOnInit() {
        this.status = 'Active';
        this.activatedRoute.params.subscribe(params => {
            this.id = params['id'];
        });
        
        this.adminService.getAllRoleType().subscribe(data => {
            //console.log(JSON.stringify(data.results));
            if (data.msgcode == 1) {
                this.allRoleData = data.results;              
            }
        });

        if (this.id > 0) {
            this.adminService.getSingleAdminData(this.id).subscribe(data => {
                if (data.msgcode == 1) {
                    this.id = data.results[0].id;
                    this.admin_role_type =data.results[0].roleId;
                    this.fname = data.results[0].first_name;
                    this.lname = data.results[0].last_name;
                    this.profile_img = data.results[0].image;
                    this.email = data.results[0].email;
                    this.status = data.results[0].status;
                }
            });
        }
        if (this.id > 0) {
            this.isPassword = false;
            this.isCon_Password = false;
            this.addadminForm = new FormGroup({
                id: new FormControl(''),
                admin_role_type: new FormControl(''),                
                fname: new FormControl('', <any>Validators.required),
                lname: new FormControl('', <any>Validators.required),
                profile_img:new FormControl(''),
                password: new FormControl(''),
                cpassword: new FormControl(''),
                email: new FormControl('', <any>[Validators.required, Validators.email],[this.isEmailUnique.bind(this)]),
                status: new FormControl('', <any>Validators.required)
            });
        } else {
            this.isPassword = true;
            this.isCon_Password = true;
            this.addadminForm = new FormGroup({
                id: new FormControl(''),
                admin_role_type: new FormControl('',<any>Validators.required), 
                fname: new FormControl('', <any>Validators.required),
                lname: new FormControl('', <any>Validators.required),
                profile_img:new FormControl('', <any>Validators.required),
                password: new FormControl('', <any>Validators.required),
                cpassword: new FormControl('', <any>[Validators.required]),
                email: new FormControl('', <any>[Validators.required, Validators.email],[this.isEmailUnique.bind(this)]),
                status: new FormControl('', <any>Validators.required)
            });
        }
    }
    // validateAreEqual('password');
    get f() { return this.addadminForm.controls; }
    get password() { return this.addadminForm.get('password'); }
    get cpassword() { return this.addadminForm.get('cpassword'); }
    onSubmit() {
        this.submitted = true;
        if (this.addadminForm.invalid) {
            return;
        } else {
            if (this.id > 0) {

                const imageFiles: Array<File> = this.filesToUpload;
                const formData = new FormData();
        
                if(imageFiles.length > 0)
                   {
                    formData.append("image", imageFiles[0], imageFiles[0]['name']);
                   }

                   var allFromData = this.addadminForm.controls;
                   Object.keys(allFromData).forEach(key => {
                     var formVal = this.addadminForm.value[key];
                     formData.append(key, formVal);
           
                   });

                this.adminService.editdminData(formData).subscribe(data => {
                  
                    if (data.msgcode == 1 && data.results.affectedRows > 0) {
                        this.router.navigate(['/admin/admin-user']);
                    }
                });
            } else {
                const imageFiles: Array<File> = this.filesToUpload;
                const formData = new FormData();
        
                if(imageFiles.length > 0)
                   {
                    formData.append("image", imageFiles[0], imageFiles[0]['name']);
                   }

                   var allFromData = this.addadminForm.controls;
                   Object.keys(allFromData).forEach(key => {
                     var formVal = this.addadminForm.value[key];
                     formData.append(key, formVal);
           
                   });

                this.adminService.saveadminData(formData).subscribe(data => {
                    if (data.msgcode == 1 && data.results.affectedRows > 0) {
                        this.router.navigate(['/admin/admin-user']);
                    }
                });
            }
        }
    }
    cancelForm = function () {
        this.router.navigate(['/admin/admin-user']);
    }
    
    isEmailUnique(control: FormControl) {
        let admin_user_id;
        if (control.value != undefined) {
          if (this.id === undefined) {
            admin_user_id = '';
    
          } else {
            admin_user_id = this.id;
          }

          this.adminEmailData['email']=control.value;
          this.adminEmailData['id']=admin_user_id;
          this.adminEmailData['tablename']='admin';
    
          const q = new Promise((resolve, reject) => {
            setTimeout(() => {
              this.commonService.checkEmailExitsOrNot( this.adminEmailData).subscribe((rs) => {
                resolve(null);
              }, () => { resolve({ 'isEmailUnique': true }); });
            }, 1000);
          });
          return q;
        }
      }
}
