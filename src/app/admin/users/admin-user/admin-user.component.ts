import { Component, OnInit, OnDestroy, AfterViewInit, ViewChild } from '@angular/core';
import { FormBuilder, Validators, ControlContainer, FormGroup, FormControl } from '@angular/forms';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Router } from "@angular/router";
import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';
import { AdminService } from '../../services/admin.service';
@Component({
    selector: 'app-admin-user-list',
    templateUrl: './admin-user.component.html',
    styleUrls: ['./admin-user.component.scss']
})
export class AdminUserComponent implements OnInit, OnDestroy, AfterViewInit {
    adminUserData: any[] = [];
    adminId;
    @ViewChild(DataTableDirective)
    dtElement: DataTableDirective;
    dtOptions: any = {};
    dtTrigger: Subject<any> = new Subject();
    constructor(private adminService: AdminService, private router: Router) { }
    ngOnInit() {
        this.adminId = this.adminService.getAdminId();
        this.dtOptions = {
            //pagingType: 'full_numbers',
            pageLength: 10,
            columnDefs: [
                {
                    // Target the actions column
                    targets: [4],
                    responsivePriority: 1,
                    filterable: false,
                    sortable: false,
                    orderable: false
                }
            ]
            //processing: true
        };
    }
    ngAfterViewInit(): void {
        this.adminService.getAdminUserData().subscribe(data => {
            if (data.msgcode == 1) {             
                this.adminUserData = data.results;
                this.dtTrigger.next();
            }
            this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                dtInstance.columns().every(function () {
                    const that = this;
                    $('input', this.footer()).on('keyup change', function () {
                        if (that.search() !== this['value']) {
                            that.search(this['value']).draw();
                        }
                    });
                });
            });
        });
    }
    ngOnDestroy(): void {
        this.dtTrigger.unsubscribe();
    }
    deleteAdmin = function (id) {           
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
            if (confirm("Are you sure want to delete admin user? ")) {
                this.adminService.deleteAdminData(id).subscribe(data => {
                    if (data.msgcode == 1 && data.results.affectedRows > 0) {
                        dtInstance.destroy();
                        this.ngAfterViewInit();
                    }
                });
            }
        });
    }
}
