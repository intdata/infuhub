import { Component, OnInit } from '@angular/core';
import { Http,Response, Headers, RequestOptions } from '@angular/http';
import { Router,ActivatedRoute} from "@angular/router";
import { AdminMenuService } from '../../../services/admin-menu.service';

@Component({
  selector: 'app-permission',
  templateUrl: './permission.component.html',
  styleUrls: ['./permission.component.scss']
})
export class PermissionComponent implements OnInit {

  constructor(private adminMenuService:AdminMenuService, private activatedRoute: ActivatedRoute) { }
  data = {};
  permissionData = [];
  menuPermissionData;
  adminId;

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
            this.adminId = params['id'];
        });
     
    this.adminMenuService.getMenuPermissionDataByAdminId(this.adminId).subscribe(data=>{    
        this.menuPermissionData = data.results;               
    });
  }

  permission(menu_id,action,event){
  	this.data['admin_id']      = this.adminId;
  	this.data['menu_id']       = menu_id;
    this.data['permission_action']  = action;
    if(event.target.checked == true){
      this.data['is_check'] = 1;
    }
    else{
      this.data['is_check'] = 0;
    }
   
  	this.adminMenuService.permission(this.data).subscribe(data=>{});  	
   
  }

}
