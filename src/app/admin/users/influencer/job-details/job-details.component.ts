import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute, RoutesRecognized } from '@angular/router';
import { CampaignService } from '../../../services/campaign.service';
//import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-job-details',
  templateUrl: './job-details.component.html',
  styleUrls: ['./job-details.component.scss']
})
export class JobDetailsComponent implements OnInit {

  private jobId: any;
  public influencerId:any;
  public jobDetail:any =[];
  public campaignHistory: any[] = [];
  public messages: any = [];
  public imageFile = ['jpg', 'jpeg', 'png'];
  public docFile = ['txt', 'csv', 'xls', 'doc', 'docx'];
  public pdfFile = ['pdf'];
  public filePath: any;
  public allPaths: any={};

  constructor(
   // private toastr: ToastrService,
    private activatedRoute: ActivatedRoute,
    private CampaignService: CampaignService
    ) { }

  ngOnInit() {
    this.activatedRoute
    .params
    .subscribe(params => {
        this.jobId = params['id'];
        this.influencerId = params['influencer'];
    });
    this.getJobDetails(this.jobId, this.influencerId);
    this.getMessage(this.jobId,this.influencerId)
  }

   /**
   * Get message by influncer
   */
  getMessage(campaign_id,influencerId){
    this.CampaignService.getInfluncerMessage(campaign_id, influencerId).subscribe(
      (response: any) => {
                  
          if (response.responseCode === 200) {
            this.messages = response.data;
            this.campaignHistory = response.campaignHistory;
            this.filePath = response.filePath;
          } else {
              console.log('Campaign create error: ', response.responseDetails);
          }
      },
      error => {
          console.log('Campaign Create error: ', error);
      }
     );
  }

  /**
   * get campaign details by campaign Id
   * @param campaign_id 
   */
  getJobDetails(campaign_id, influencerId){
    this.CampaignService.jobDetail(campaign_id, influencerId).subscribe(
      (response: any) => {
          //console.log(response);
          
          if (response.responseCode === 200) {
            this.jobDetail = response.data;
            this.allPaths = (response.allPaths) ? response.allPaths: {};
          } else {
            console.info(response.responseDetails)
            //this.toastr.info(response.responseDetails, 'Info!');
        }
    },
    error => {
      console.info('Something went wrong.')
       // this.toastr.error('Something went wrong.', 'Error!');
    }  
    ); 
  }

}
