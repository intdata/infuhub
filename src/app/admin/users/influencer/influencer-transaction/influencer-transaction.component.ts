import { Component, OnInit ,OnDestroy ,AfterViewInit,ViewChild, TemplateRef, ɵConsole } from '@angular/core';
import { FormBuilder, Validators, ControlContainer, FormGroup, FormControl } from '@angular/forms';
import {Http,Response, Headers, RequestOptions } from '@angular/http';
import { Router, ActivatedRoute } from "@angular/router";
import { Subject } from 'rxjs';
import {InfluencerTransactionService} from '../../../services/influencer-transaction.service';
import { DataTableDirective } from 'angular-datatables'; 
import { formatDate } from "@angular/common";
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { ToastrService } from 'ngx-toastr';
import * as jsPDF from 'jspdf'
import 'jspdf-autotable';

@Component({
  selector: 'app-influencer-transaction',
  templateUrl: './influencer-transaction.component.html',
  styleUrls: ['./influencer-transaction.component.css'],
})
export class InfluencerTransactionComponent implements OnInit ,OnDestroy,AfterViewInit {
  brandOwnerTransactionData:any[]=[];
  inventoryForm: FormGroup;
  public paymentRDate: string;
  submitted = false;
  loading = false;
  statusData={};
  public dataArr: any={};
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: any = {};
  dtTrigger: Subject<any> = new Subject();
  id: any;
  paymentId: any;
  influencerId: any;
  modalRef: BsModalRef;
  configPopup = {
    animated: true,
    keyboard: true,
    backdrop: true,
    ignoreBackdropClick: false,
    class: "foodPopup"
  };
  
  constructor(
    private InfluencerTransactionService : InfluencerTransactionService,
    private router: Router, private activatedRoute: ActivatedRoute,
    private modalService: BsModalService,
    private formBuilder: FormBuilder,
    private toastr: ToastrService,
  ) { }

  ngOnInit() {
    this.inventoryForm = this.formBuilder.group({
      paymentRDate: ['', [Validators.required]],
      paymentId: ['', [Validators.required]],
      influencerId: ['', [Validators.required]],
    });
    this.activatedRoute.params.subscribe(params => {
      this.id = params['id'];
      this.influencerId = params['id'];
      //console.log(this.id);
      
    });
    this.dtOptions = {
      //pagingType: 'full_numbers',
      pageLength: 10,
      lengthMenu:[10,20,30], 
      ordering: false,
      sDom: '<"top"l>rt<"bottom"ip><"clear">',
      paging: true,
      columnDefs : [
              {
                responsivePriority: 1,
                filterable        : false,
                sortable          : false,
                orderable: false,
                  
              } ,
              { type: 'date-uk', targets: 3 }               
          ],
  
        };
         
  }
  ngAfterViewInit(): void {
    this.InfluencerTransactionService.allData(this.id).subscribe(data=>{
      if(data.msgcode==1)
      {
          console.log(data);
          this.brandOwnerTransactionData = data.results;     
          this.dtTrigger.next(); 
          this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
            dtInstance.columns().every(function () {
              const that = this;
              $('input', this.footer()).on('keyup change', function () {
                console.log(this['value']);
                console.log(that.search());
                if (that.search() !== this['value']) {
                  that.search(this['value']).draw();
                }
              });
            });
          });
        

      }                 
              
   });
   
  }
  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }


  exportExcel(service_type: any,payment_date: any) {
    //console.log(service_type);
    //return;
      let data: any;
      let jsonData : any[ ] = [ ];
      this.dataArr.service_type = service_type;
      this.dataArr.payment_date = payment_date;
      this.dataArr.id = this.id;
      this.InfluencerTransactionService.getallExportData(this.dataArr).subscribe(
        (response: any) => {
            //console.log(response);return;
            if (response.msgcode === 1) {
                data = response.results;
                var ListData : any;
                let j = 0;
                const format = 'yyyy/MM/dd';
                const locale = 'en-US';
                let myDate = 'N/A';
                for(var i in data) {
                if(data[i].payout_type=='Auto')
                {
                  myDate = data[i].created_at;
                }
                if(data[i].payout_type=='Manual')
                {
                  myDate = data[i].manual_payout_on;
                }
                var paymentStatus = 'Unpaid';
                if(data[i].payout_status == 'SUCCESS')
                {
                  paymentStatus = 'Paid';
                }
                const formattedDate = (myDate !== null ) ? formatDate(myDate, format, locale) : 'N/A';
                console.log(formattedDate);
                ListData = {
                  "CampaignName" : data[i].campaign_name,
                  "ServiceType" : data[i].service_type,
                  "PaymentOn" : formattedDate,
                  "PaymentReleaseType" : data[i].payout_type,
                  "NetAmount" : data[i].net_amount,
                  "AdminCommission" : data[i].admin_commission,
                  "PaypalCharge" : data[i].paypal_charge_brandowner_amt,
                  "TotalAmount" : data[i].total_amount,
                  "PaymentStatus" : paymentStatus,
                  "TransactionId" : (data[i].transaction_id != null ) ? data[i].transaction_id : 'N/A',
                }
                jsonData.push(ListData);
                j++;
              }
              console.log(jsonData);
              this.InfluencerTransactionService.exportAsExcelFile(jsonData, 'influencer_transaction');
            } else {
                console.log('get page error: ', response.responseDetails);
            }
        },
        error => {
            console.log('get page error: ', error);
        }
        
    );
  
  }

  // convenience getter for easy access to form fields
  get f() { return this.inventoryForm.controls; }

  openModal(template: TemplateRef<any>, id: any) {
    this.modalRef = this.modalService.show(
      template,
      Object.assign({}, 
        { 
          class: 'addFoodInv foodPopup modal-md',
          animated: true,
          keyboard: true,
          backdrop: true,
          ignoreBackdropClick: false
        })
    );
    this.paymentRDate = ''; 
    this.paymentId = id;
    this.influencerId = this.influencerId;
  }

   /**
   * Method used for updating manual payment
   * @memberof HeaderComponent
   */
  public saveinventory() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.inventoryForm.invalid) {
      this.loading = false;
      return;
    }
    this.loading = true;

    this.InfluencerTransactionService.releaseManualPayment(this.inventoryForm.value)
      .subscribe(
        response => {
          if(response.msgcode == 1)
          {
            console.log("Success!");
            //this.toastr.success(response.message, 'Success!');
            //this.foodList = response.data.foodList;
            //this.inventoryList = response.data.foodInventoryList;
            this.modalRef.hide();
            this.loading = false;
            this.toastr.success('Payment released successfully', 'Success');
            this.InfluencerTransactionService.allData(this.id).subscribe(data=>{
              if(data.msgcode==1)
              {
                  this.brandOwnerTransactionData = data.results;  
                  //this.dtElement.ngOnDestroy();
                 // this.dtTrigger.next(); 
                  this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                    dtInstance.destroy();
                    // Call the dtTrigger to rerender again
                    this.dtTrigger.next();
                    dtInstance.columns().every(function () {
                      const that = this;
                      $('input', this.footer()).on('keyup change', function () {
                        console.log(this['value']);
                        console.log(that.search());
                        if (that.search() !== this['value']) {
                          that.search(this['value']).draw();
                        }
                      });
                    });
                  });
              
                }                 
                      
           });
            
          }
          else
          {
            this.loading = false;
            console.log("Error!");
            //this.toastr.error(response.message, 'Error!');
          }
        },
        error => {
          this.loading = false;
          console.log("Error!");
          //this.toastr.error('You don\'t have previlage to access this page.', 'Error!');
        }
      );
  }

  /**
 * Create the pdf 
 */
  captureScreenPdf(invoice_number:any){
    this.InfluencerTransactionService.createPdf(invoice_number).subscribe(
      (response: any) => {
          if (response.msgcode === 1) {
            //this.selectedInfluncersData = response.data
            //this.influencerImagePath = response.influencerImagePath
            //window.location.href=response.data;
            console.log(response.results);
            window.open(response.results, "_blank");
            //console.log(response.data)
          } else {
              console.log('Campaign create error: ', response.responseDetails);
          }
      },
      error => {
          console.log('Campaign Create error: ', error);
      }
    );

  }

  

}
