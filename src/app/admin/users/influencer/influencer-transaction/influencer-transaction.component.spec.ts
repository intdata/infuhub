import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InfluencerTransactionComponent } from './influencer-transaction.component';

describe('InfluencerTransactionComponent', () => {
  let component: InfluencerTransactionComponent;
  let fixture: ComponentFixture<InfluencerTransactionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InfluencerTransactionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfluencerTransactionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
