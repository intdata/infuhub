import { Component, OnInit ,OnDestroy ,AfterViewInit,ViewChild } from '@angular/core';
import { FormBuilder, Validators, ControlContainer, FormGroup, FormControl } from '@angular/forms';
import {Http,Response, Headers, RequestOptions } from '@angular/http';
import {Router,ActivatedRoute} from "@angular/router";
import { CreatorService } from '../../services/creator.service';
import { Subject } from 'rxjs'; 
import { DataTableDirective } from 'angular-datatables'; 
//declare var $: any;
@Component({
  selector: 'app-creator-list',
  templateUrl: './influencer.component.html',
  styleUrls: ['./influencer.component.scss']
})
export class InfluencerComponent implements OnInit,OnDestroy,AfterViewInit {

 // #ViewChild("dataTable");
  creatorData:any[] = [];
  statusData={};
  @ViewChild(DataTableDirective)
    dtElement: DataTableDirective;
    dtOptions: any = {};
    dtTrigger: Subject<any> = new Subject();


  constructor(private creatorService:CreatorService, private router:Router) { }

  ngOnInit() {
   this.dtOptions = {
      //pagingType: 'full_numbers',
      pageLength: 10,   
          columnDefs : [
              {
                  // Target the actions column
                  targets           : [8],
                  responsivePriority: 1,
                  filterable        : false,
                  sortable          : false,
                  orderable: false
              }                
          ]

    };     
}

  ngAfterViewInit(): void { 
    this.creatorService.getCreatorListData().subscribe(data=>{   
      if (data.msgcode == 1) {             
        this.creatorData = data.results;
        this.dtTrigger.next();
            } 
            //=============== for indivisual field serach================//
            this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
              dtInstance.columns().every(function () {
                  const that = this;
                  $('input', this.footer()).on('keyup change', function () {
                      if (that.search() !== this['value']) {
                          that.search(this['value']).draw();
                      }
                  });
              });
          });
          //=============== End for indivisual field serach================//

          });

         

  }

  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }

  deleteCreator = function(id){

    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      if(confirm("Are you sure want to delete influencer ? ")) {

         this.creatorService.deleteCreatorData(id).subscribe(data => {
            if(data.msgcode==1 && data.results.affectedRows >0)
            {
         
             dtInstance.destroy();
             this.ngAfterViewInit();
           }
          });
      }

    });

  } 

  changeStatus = function(id,status){

    this.statusData['id'] =id;
    this.statusData['status'] =status;

    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
    this.creatorService.ChangeStatus(this.statusData).subscribe(data => { 
      if(data.msgcode==1 && data.results.affectedRows >0)
       {         
           dtInstance.destroy();
          this.ngAfterViewInit();
        
      }
    });
  });
  
  } 



}
