import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { CreatorService } from '../../../services/creator.service';

@Component({
  selector: 'app-influencer-details',
  templateUrl: './influencer-details.component.html',
  styleUrls: ['./influencer-details.component.scss']
})
export class InfluencerDetailsComponent implements OnInit {
  id;
  platform;
  fname;
  lname;
  email;
  picture;
  status;
  company_name;
  contact_number;
  description;
  constructor(private creatorService: CreatorService, private router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {

    this.activatedRoute.params.subscribe(params => {
      this.id = params['id'];
  });
  if (this.id > 0) {
      this.creatorService.getSingleCreatorData(this.id).subscribe(data => {
        console.log(data);
          if (data.msgcode == 1 && data.results.length > 0) {
              this.platform = data.results[0].platform_id;
              this.fname = data.results[0].first_name;
              this.lname = data.results[0].last_name;
              this.email = data.results[0].email;
              this.description=data.results[0].description;
              this.company_name = data.results[0].company_name;
              this.contact_number = data.results[0].contact_number;
              this.picture = data.results[0].picture;
              this.status = data.results[0].status;
          }
      });
  }
  }

}
