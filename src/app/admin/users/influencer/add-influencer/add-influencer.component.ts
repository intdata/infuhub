import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup, FormControl } from '@angular/forms';

import { Router, ActivatedRoute } from "@angular/router";
import { CreatorService } from '../../../services/creator.service';
import { NgFlashMessageService } from 'ng-flash-messages';
@Component({
    selector: 'app-add-creator',
    templateUrl: './add-influencer.component.html',
    styleUrls: ['./add-influencer.component.scss']
})
export class AddInfluencerComponent implements OnInit {
    //nrSelect:string = "Active" 
    addcreatorForm: FormGroup;
    submitted = false;
    isPassword = true;
    id;
    platform;
    fname;
    lname;
    description;
    email;
    picture;
    status;
    company_name;
    contact_number;
    actionName;
    constructor(private creatorService: CreatorService, private router: Router, private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder, private ngFlashMessageService: NgFlashMessageService) { }
    influencerImage: Array<File> = [];
    fileChangeEvent(fileInput: any) {
        this.influencerImage = <Array<File>>fileInput.target.files;
    }
    ngOnInit() {
        this.platform = 1;
        this.status = 'Active';
        this.activatedRoute.params.subscribe(params => {
            this.id = params['id'];
        });
        if (this.id > 0) {
            this.creatorService.getSingleCreatorData(this.id).subscribe(data => {
                console.log(data);
                if (data.msgcode == 1 && data.results.length > 0) {
                    this.id = data.results[0].id;
                    this.platform = data.results[0].platform_id;
                    this.fname = data.results[0].first_name;
                    this.lname = data.results[0].last_name;
                    this.description = data.results[0].description;                    
                    this.email = data.results[0].email;
                    this.company_name = data.results[0].company_name;
                    this.contact_number = data.results[0].contact_number;
                    this.picture = data.results[0].picture;
                    this.status = data.results[0].status;
                }
            });
        }
        if (this.id > 0) {
            this.actionName = 'Edit';
            this.isPassword = false;
            this.addcreatorForm = new FormGroup({
                id: new FormControl(''),
                platform: new FormControl(''),
                fname: new FormControl('', <any>Validators.required),
                lname: new FormControl('', <any>Validators.required),
                description: new FormControl(''),
                password: new FormControl(''),
                email: new FormControl('', <any>[Validators.required, Validators.email]),
                company_name: new FormControl(''),
                contact_number: new FormControl('', <any>[Validators.required, Validators.pattern('[0-9]+')]),
                status: new FormControl('', <any>Validators.required)
            });
        }
        else {
            this.actionName = 'Add';
            this.isPassword = true;
            this.addcreatorForm = new FormGroup({
                id: new FormControl(''),
                platform: new FormControl(''),
                fname: new FormControl('', <any>Validators.required),
                lname: new FormControl('', <any>Validators.required),
                description: new FormControl(''),
                password: new FormControl('', <any>Validators.required),
                email: new FormControl('', <any>[Validators.required, Validators.email]),
                company_name: new FormControl(''),
                contact_number: new FormControl('', <any>[Validators.required, Validators.pattern('[0-9]+')]),
                status: new FormControl('', <any>Validators.required)
            });
        }
    }
    get f() {
        return this.addcreatorForm.controls;
    }
    onSubmit() {
        this.submitted = true;
        if (this.addcreatorForm.invalid) {
            return;
        }
        else {
            const imageFiles: Array<File> = this.influencerImage;
                const influencerData: any = new FormData();
                if (imageFiles.length > 0)
                 influencerData.append("image", imageFiles[0], imageFiles[0]['name']);
                influencerData.append("platform",this.addcreatorForm.value.platform);
                influencerData.append("fname",this.addcreatorForm.value.fname);
                influencerData.append("lname",this.addcreatorForm.value.lname);
                influencerData.append("description",this.addcreatorForm.value.description);
                influencerData.append("email",this.addcreatorForm.value.email);                
                influencerData.append("company_name",this.addcreatorForm.value.company_name);
                influencerData.append("contact_number",this.addcreatorForm.value.contact_number);
                influencerData.append("status",this.addcreatorForm.value.status);
            if (this.id > 0) {
                influencerData.append("id",this.id);
                this.creatorService.editCreatorData(influencerData).subscribe(data => {
                    if (data.msgcode == 1 && data.results.affectedRows > 0) {
                        this.router.navigate(['/admin/influencer']);
                    }
                });
            }
            else {
                influencerData.append("password",this.addcreatorForm.value.password);
                this.creatorService.saveCreatorData(influencerData).subscribe(data => {
                    if (data.msgcode == 1 && data.results.affectedRows > 0) {
                        this.ngFlashMessageService.showFlashMessage({
                            // Array of messages each will be displayed in new line
                            messages: ["Influencer Has Been added Succefully"],
                            // Whether the flash can be dismissed by the user defaults to false
                            dismissible: true,
                            // Time after which the flash disappears defaults to 2000ms
                            timeout: false,
                            // Type of flash message, it defaults to info and success, warning, danger types can also be used
                            type: 'success'
                        });
                        //this.router.navigate(['/admin/influencer']);
                    }
                });
            }
        }
    }
    cancelForm = function () {
        this.router.navigate(['/admin/influencer']);
    }
}
