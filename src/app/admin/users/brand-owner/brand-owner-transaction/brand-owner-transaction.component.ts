import { Component, OnInit ,OnDestroy ,AfterViewInit,ViewChild } from '@angular/core';
import { FormBuilder, Validators, ControlContainer, FormGroup, FormControl } from '@angular/forms';
import {Http,Response, Headers, RequestOptions } from '@angular/http';
import { Router, ActivatedRoute } from "@angular/router";
import { Subject } from 'rxjs';
import {BrandTransactionService} from '../../../services/brand-transaction.service';
import { DataTableDirective } from 'angular-datatables'; 
import { formatDate } from "@angular/common";

@Component({
  selector: 'app-brand-owner-transaction',
  templateUrl: './brand-owner-transaction.component.html',
  styleUrls: ['./brand-owner-transaction.component.css']
})
export class BrandOwnerTransactionComponent implements OnInit ,OnDestroy,AfterViewInit {
  brandOwnerTransactionData:any[]=[];
  statusData={};
  public dataArr: any={};
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: any = {};
  dtTrigger: Subject<any> = new Subject();
  id: any;
  
  constructor(
    private BrandTransactionService : BrandTransactionService,
    private router: Router, private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      this.id = params['id'];
    });
    this.dtOptions = {
      //pagingType: 'full_numbers',
      pageLength: 10,
      lengthMenu:[10,20,30], 
      ordering: false,
      sDom: '<"top"l>rt<"bottom"ip><"clear">',
      paging: true,
      columnDefs : [
              {
                  // Target the actions column
                  responsivePriority: 1,
                  filterable        : false,
                  sortable          : false,
                  orderable: false,
                  
              } ,
              { type: 'date-uk', targets: 3 }               
          ],
  
        };
         
  }
  ngAfterViewInit(): void {
    this.BrandTransactionService.allData(this.id).subscribe(data=>{
          
      if(data.msgcode==1)
      {
          console.log(data);
          this.brandOwnerTransactionData = data.results;     
          this.dtTrigger.next(); 
          this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
            dtInstance.columns().every(function () {
              const that = this;
              $('input', this.footer()).on('keyup change', function () {
                console.log(this['value']);
                if (that.search() !== this['value']) {
                  that.search(this['value']).draw();
                }
              });
            });
          });
        

      }                 
              
   });
   
  }
  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }


  exportExcel(service_type: any,payment_date: any) {
    //console.log(service_type);
    //return;
      let data: any;
      let jsonData : any[ ] = [ ];
      this.dataArr.service_type = service_type;
      this.dataArr.payment_date = payment_date;
      this.dataArr.id = this.id;
      this.BrandTransactionService.getallExportData(this.dataArr).subscribe(
        (response: any) => {
            //console.log(response);return;
            if (response.msgcode === 1) {
              data = response.results;
              var ListData : any;
              let j = 0;
              const format = 'yyyy/MM/dd';
              const locale = 'en-US';
              for(var i in data) {
                const myDate = data[i].created_at;
                const formattedDate = formatDate(myDate, format, locale);
                var paymentStatus = 'Unpaid';
                if(data[i].payment_status == 'COMPLETED')
                {
                  paymentStatus = 'Paid';
                }
                ListData = {
                  "CampaignName" : data[i].campaign_name,
                  "ServiceType" : data[i].service_type,
                  "PaymentOn" : formattedDate,
                  "NetAmount" : data[i].net_amount,
                  "AdminCommission" : data[i].admin_commission,
                  "PaypalCharge" : data[i].paypal_charge_brandowner_amt,
                  "TotalAmount" : data[i].total_amount,
                  "PaymentStatus" : paymentStatus,
                  "TransactionId" : data[i].transaction_id,
                }
                jsonData.push(ListData);
                j++;
              }
              console.log(jsonData);
              this.BrandTransactionService.exportAsExcelFile(jsonData, 'brandowner_transaction');
            } else {
                console.log('get page error: ', response.responseDetails);
            }
        },
        error => {
            console.log('get page error: ', error);
        }
        
    );

  }

    /**
 * Create the pdf 
 */
captureScreenPdf(invoice_number:any){
  this.BrandTransactionService.createPdf(invoice_number).subscribe(
    (response: any) => {
        if (response.msgcode === 1) {
          //this.selectedInfluncersData = response.data
          //this.influencerImagePath = response.influencerImagePath
          //window.location.href=response.data;
          console.log(response.results);
          window.open(response.results, "_blank");
          //console.log(response.data)
        } else {
            console.log('Campaign create error: ', response.responseDetails);
        }
    },
    error => {
        console.log('Campaign Create error: ', error);
    }
  );

}

}
