import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BrandOwnerTransactionComponent } from './brand-owner-transaction.component';

describe('BrandOwnerTransactionComponent', () => {
  let component: BrandOwnerTransactionComponent;
  let fixture: ComponentFixture<BrandOwnerTransactionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BrandOwnerTransactionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BrandOwnerTransactionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
