import { Component, OnInit } from "@angular/core";
import {
  FormBuilder,
  Validators,
  ControlContainer,
  FormGroup,
  FormControl,
  AsyncValidatorFn,
  AbstractControl,
  ValidationErrors
} from "@angular/forms";
import { Http, Response, Headers, RequestOptions } from "@angular/http";
import { Router, ActivatedRoute } from "@angular/router";
import { BrandOwnerService } from "../../../services/brand-owner.service";
import { CommonService } from "../../../services/common.service";
import { CountryService } from "./../../../services/country.service";
import { IndustryService } from "./../../../services/industry.service";
import { Observable } from "rxjs/Observable";
@Component({
  selector: "app-add-brand-owner",
  templateUrl: "./add-brand-owner.component.html",
  styleUrls: ["./add-brand-owner.component.scss"]
})
export class AddBrandOwnerComponent implements OnInit {
  brandOwnerForm: FormGroup;
  submitted = false;
  isPassword = true;
  isCon_Password = true;
  isEmailExit = false;
  brandEmailData = {};
  id;
  comapany_name;
  company_ph_no;
  email;
  company_logo;
  company_type;
  status;
  industry;
  country;
  fname;
  lname;
  actionName;
  //aa;
  debouncer: any;
  allCountries: any = [];
  allIndustries: any = [];

  constructor(
    private brandOwnerService: BrandOwnerService,
    private commonService: CommonService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private countryService: CountryService,
    private industryService: IndustryService
  ) {}
  filesToUpload: Array<File> = [];
  fileChangeEvent(fileInput: any) {
    this.filesToUpload = <Array<File>>fileInput.target.files;
  }

  ngOnInit() {
    this.status = "Active";
    this.industry = "";
    this.country = "";
    this.company_type = "";
    this.activatedRoute.params.subscribe(params => {
      this.id = params["id"];
    });
    if (this.id > 0) {
      this.brandOwnerService
        .getSingleBrandOwnerData(this.id)
        .subscribe(data => {
          console.log(data);
          
          if (data.msgcode == 1 && data.results.length > 0) {
            this.id = data.results[0].id;
            this.comapany_name = data.results[0].company_name;
            this.company_logo = data.results[0].company_logo;
            this.company_ph_no = data.results[0].company_ph_no;
            this.fname = data.results[0].fname;
            this.company_type = data.results[0].company_type;
            this.lname = data.results[0].lname;
            this.industry = data.results[0].industry;
            this.country = data.results[0].country_code;
            this.email = data.results[0].email;
            this.status = data.results[0].status;
          }
        });
    }
    if (this.id > 0) {
      this.actionName = "Edit";
      this.isPassword = false;
      this.isCon_Password = false;
      this.brandOwnerForm = new FormGroup({
        id: new FormControl(""),
        comapany_name: new FormControl("", <any>Validators.required),
        company_logo: new FormControl(""),
        company_type: new FormControl(""),
        industry: new FormControl("", <any>Validators.required),
        country: new FormControl("", <any>Validators.required),
        company_ph_no: new FormControl("", <any>[
          Validators.required,
          Validators.pattern("^[0-9]*$"),
          Validators.minLength(10),
          Validators.maxLength(10)
        ]),
        fname: new FormControl("", <any>Validators.required),
        lname: new FormControl("", <any>Validators.required),
        password: new FormControl(""),
        cpassword: new FormControl(""),
        email: new FormControl(
          "",
          <any>[Validators.required, Validators.email],
          [this.isEmailUnique.bind(this)]
        ),
        //status: new FormControl("", <any>Validators.required)
      });
    } else {
      this.actionName = "Add";
      this.isPassword = true;
      this.isCon_Password = true;
      this.brandOwnerForm = new FormGroup({
        id: new FormControl(""),
        comapany_name: new FormControl("", <any>Validators.required),
        company_logo: new FormControl("", <any>Validators.required),
        company_type: new FormControl(""),
        industry: new FormControl("", <any>Validators.required),
        country: new FormControl("", <any>Validators.required),
        company_ph_no: new FormControl("", <any>[
          Validators.required,
          Validators.pattern("^[0-9]*$"),
          Validators.minLength(10),
          Validators.maxLength(10)
        ]),
        fname: new FormControl("", <any>Validators.required),
        lname: new FormControl("", <any>Validators.required),
        password: new FormControl("", <any>Validators.required),
        cpassword: new FormControl("", <any>[Validators.required]),
        email: new FormControl(
          "",
          <any>[Validators.required, Validators.email],
          [this.isEmailUnique.bind(this)]
        ),
        //status: new FormControl("", <any>Validators.required)
      });
    }

    // get all countries
    this.getAllCountries();

    // get all industries
    this.getAllIndustries();
  }
  get f() {
    return this.brandOwnerForm.controls;
  }
  onSubmit() {
    this.submitted = true;
    if (this.brandOwnerForm.invalid) {
      return;
    } else {
      if (this.id > 0) {
        const imageFiles: Array<File> = this.filesToUpload;
        const formData = new FormData();

        if (imageFiles.length > 0) {
          formData.append("image", imageFiles[0], imageFiles[0]["name"]);
        }

        var allFromData = this.brandOwnerForm.controls;
        Object.keys(allFromData).forEach(key => {
          var formVal = this.brandOwnerForm.value[key];
          formData.append(key, formVal);
        });

        this.brandOwnerService.editBrandOwnerData(formData).subscribe(data => {
          if (data.msgcode == 1 && data.results.affectedRows > 0) {
            this.router.navigate(["/admin/brand-owner"]);
          }
        });
      } else {
        const imageFiles: Array<File> = this.filesToUpload;
        const formData = new FormData();

        formData.append("image", imageFiles[0], imageFiles[0]["name"]);

        var allFromData = this.brandOwnerForm.controls;
        Object.keys(allFromData).forEach(key => {
          var formVal = this.brandOwnerForm.value[key];
          formData.append(key, formVal);
        });
        this.brandOwnerService.saveBrandOwnerData(formData).subscribe(data => {
          if (data.msgcode == 1 && data.results.affectedRows > 0) {
            this.router.navigate(["/admin/brand-owner"]);
          }
        });
      }
    }
  }
  cancelForm = function() {
    this.router.navigate(["/admin/brand-owner"]);
  };
  isEmailUnique(control: FormControl) {
    let brandowner_id;
    if (control.value != undefined) {
      if (this.id === undefined) {
        brandowner_id = "";
      } else {
        brandowner_id = this.id;
      }
      this.brandEmailData["email"] = control.value;
      this.brandEmailData["id"] = brandowner_id;
      this.brandEmailData["tablename"] = "brand_owner";
      const q = new Promise((resolve, reject) => {
        setTimeout(() => {
          this.commonService
            .checkEmailExitsOrNot(this.brandEmailData)
            .subscribe(
              rs => {
                resolve(null);
              },
              () => {
                resolve({ isEmailUnique: true });
              }
            );
        }, 1000);
      });
      return q;
    }
  }

  /**
     * Method to get all active countries
     */
    getAllCountries(){
      // call service to get country list
      this.countryService.getAllCountries().subscribe(
          (response: any) => {
              //console.log(response);
              
              if (response.responseCode === 200) {
                  this.allCountries = response.countries;
              } else {
                  console.log('get country list error: ', response.responseDetails);
              }
          },
          error => {
              console.log('get country list error: ', error);
          }
      );
  }

  /**
   * Method to get all active industries
   */
  getAllIndustries(){
      // call service to get industry list
      this.industryService.getAllIndustries().subscribe(
          (response: any) => {
              //console.log(response);
              
              if (response.responseCode === 200) {
                  this.allIndustries = response.industries;
              } else {
                  console.log('get industry list error: ', response.responseDetails);
              }
          },
          error => {
              console.log('get industry list error: ', error);
          }
      );
  }
}
