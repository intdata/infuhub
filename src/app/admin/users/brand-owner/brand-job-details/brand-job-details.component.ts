import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute, RoutesRecognized } from '@angular/router';
import { CampaignService } from '../../../services/campaign.service';
//import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-brand-job-details',
  templateUrl: './brand-job-details.component.html',
  styleUrls: ['./brand-job-details.component.scss']
})
export class BrandJobDetailsComponent implements OnInit {
  public progressArr: Array<any> = [];
  public selectedItemObj;
  public selectedInfluncerId: any = '';
  public status: any = '';
  public campaignHistory: any[] = [];
  public proposalData: any = []
  public allPlatforms: any = [];
  public campaign: any = []
  public id: string;
  public influncerMessageData: any = [];
  public msubmitted = false;
  public messagefilePath = '';
  public imageFile = ['jpg', 'jpeg', 'png']
  public docFile = ['txt', 'csv', 'xls', 'doc', 'docx']
  public pdfFile = ['pdf']
  public statusList = { 'PENDING': 'Pending', 'ACCEPT': 'Request Accepted', 'REJECT': 'Request Not Accepted', 'SUBMITTED': 'Proposal Received', 'CONFIRM': 'Proposal Accepted', 'REFUSE': 'Proposal Not Accepted', 'CONTENT_SUBMITTED': 'Content Received', 'CONTENT_APPROVED': 'Content Approved', 'PAID': 'Paid' };
  public allPaths: any = {};
  public influncerMessageSlice: any = [];
  addScript: boolean = false;
  paypalLoad: boolean = true;
  finalAmount: number = 1;

  constructor(
   // private toastr: ToastrService,
    private route: ActivatedRoute,
    private CampaignService: CampaignService
    ) { }

  ngOnInit() {

    this.route.params.subscribe(params => {
      this.id = params['id'];
      console.log(this.getCampaign(this.id));
      //console.log(this.getassignedInfluencer(this.id));

    });
    //this.getplatforms();

  }

   /**
   * Get campaign data by id
   * @param id 
   */
  getCampaign(id) {
    this.CampaignService.getDataById(id).subscribe(
      (response: any) => {
        //console.log(response);return;
        if (response.responseCode === 200) {
          this.campaign = response.data;
          if (response.data && response.data.CampaignAssigns.length) {
            this.selectedInfluncerId = response.data.CampaignAssigns[0].influencer_id;
            this.getInfluncerData(response.data.CampaignAssigns[0].influencer_id, [], response.data.CampaignAssigns[0].status);
            this.selectedItemObj = response.data.CampaignAssigns[0];
            //console.log(this.selectedItemObj);
            this.allPaths = (response.allPaths) ? response.allPaths : {};
          }

        } else {
          console.log('get page error: ', response.responseDetails);
        }
      },
      error => {
        console.log('get page error: ', error);
      }
    );
  }

    /**
   * Get Influncer Detail
  */
 getInfluncerData(influncer_id, selectedInfluncer = [], status = '') {

  this.status = status;
  //console.log(status)
  this.selectedItemObj = selectedInfluncer;
  this.selectedInfluncerId = influncer_id;
  ///console.log(influncer_id);
  this.CampaignService.getInfluncerdata(this.id, influncer_id).subscribe(
    (response: any) => {
      console.log(response);
      if (response.responseCode === 200) {
        console.log(response);
        this.campaignHistory = response.campaignHistory;
        this.influncerMessageData = response.data;
        this.influncerMessageSlice = this.influncerMessageData.slice(0, 10);
        this.messagefilePath = response.filePath;
      } else {
        console.log('get page error: ', response.responseDetails);
      }
    },
    error => {
      console.log('get page error: ', error);
    }
  );
}

  

  



}
