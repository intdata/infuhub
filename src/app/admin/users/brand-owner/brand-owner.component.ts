import { Component, OnInit ,OnDestroy ,AfterViewInit,ViewChild} from '@angular/core';
import { FormBuilder, Validators, ControlContainer, FormGroup, FormControl } from '@angular/forms';
import {Http,Response, Headers, RequestOptions } from '@angular/http';
import {Router} from "@angular/router";
import { Subject } from 'rxjs';
import { BrandOwnerService } from '../../services/brand-owner.service';
import { DataTableDirective } from 'angular-datatables'; 
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-brand-owner-list',
  templateUrl: './brand-owner.component.html',
  styleUrls: ['./brand-owner.component.scss']
})
export class BrandOwnerComponent implements OnInit ,OnDestroy,AfterViewInit {
  brandOwnerData:any[]=[];
  statusData={};

  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: any = {};
  dtTrigger: Subject<any> = new Subject();
  public signUpOption: any[] = [];

  constructor(
    private brandOwnerService:BrandOwnerService, 
    private router:Router,
    private toastr: ToastrService,
    ) { 
    //super(dialogService);

  }
  ngOnInit() {
    //user signup status
    this.signUpOption = [
      { key: "0", value: "Not Verified" },
      { key: "1", value: "Verified" },
      { key: "2", value: "Approved" }
    ]
    this.dtOptions = {
    //pagingType: 'full_numbers',
    pageLength: 10,
    lengthMenu:[10,50,100],   
        columnDefs : [
            {
                // Target the actions column
                targets           : [10],
                responsivePriority: 1,
                filterable        : false,
                sortable          : false,
                orderable: false
            }                
        ]

      };     
  }

  ngAfterViewInit(): void { 
   this.brandOwnerService.getBrandOwnerData().subscribe(data=>{
          
             if(data.msgcode==1)
             {
               console.log(data);
                this.brandOwnerData = data.results;     
                this.dtTrigger.next(); 
             }                 
                     
          });
      }

   ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }

  deleteBrandOwnder = function(id){
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
    if(confirm("Are you sure to delete brand owner? ")) {
       this.brandOwnerService.deleteBrandOwnerData(id).subscribe(data => { 
           if(data.msgcode==1 && data.results.affectedRows >0)
            {         
                dtInstance.destroy();
                this.ngAfterViewInit();
           }
         });
       }
    });
  } 


  changeStatus = function(id,status){
      this.statusData['id'] =id;
      this.statusData['status'] =status;
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      this.brandOwnerService.ChangeStatus(this.statusData).subscribe(data => { 
        if(data.msgcode==1 && data.results.affectedRows >0)
        {         
            dtInstance.destroy();
            this.ngAfterViewInit();
          
        }
      });
    });
  }
  
  /**
     * update email verification status
     */
  updateSignupStatus = function(id,event: any){
      this.statusData['id'] =id;
      this.statusData['is_email_verified'] =event;
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      this.brandOwnerService.UpdateSignupStatus(this.statusData).subscribe(data => { 
        if(data.msgcode==1 && data.results.affectedRows >0)
        {        
            this.toastr.success('Status updated successfully', 'Success!');
            dtInstance.destroy();
            this.ngAfterViewInit();

        }
      });
    });
  }
}
