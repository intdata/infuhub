
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from "@angular/common/http";
import { FormsModule } from '@angular/forms';
import { NgxPermissionsModule } from 'ngx-permissions';
import { CommonModule } from '@angular/common';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ReactiveFormsModule } from '@angular/forms';
import { EditorModule } from '@tinymce/tinymce-angular';

import { AdminRoutingModule } from './admin-routing.module';
import { NavbarComponent } from './layout/navbar/navbar.component';
import { SidebarComponent } from './layout/sidebar/sidebar.component';
import { FooterComponent } from './layout/footer/footer.component';
import { DashboardComponent } from './accounts/dashboard/dashboard.component';
import { InnerLayoutComponent } from './layout/inner-layout/inner-layout.component';
import { LoginLayoutComponent } from './layout/login-layout/login-layout.component';
import { LoginComponent } from './accounts/login/login.component';
import { AdminService } from './services/admin.service';
import { AddAdminComponent } from './users/admin-user/add-admin/add-admin.component';
import { AdminUserComponent } from './users/admin-user/admin-user.component';
import { CreatorService } from './services/creator.service';
import { InfluencerComponent } from './users/influencer/influencer.component';
import { InfluencerDetailsComponent } from './users/influencer/influencer-details/influencer-details.component';
import { JobDetailsComponent } from './users/influencer/job-details/job-details.component';
import { AddInfluencerComponent } from './users/influencer/add-influencer/add-influencer.component';
import { AddBrandOwnerComponent } from './users/brand-owner/add-brand-owner/add-brand-owner.component';
import { BrandOwnerComponent } from './users/brand-owner/brand-owner.component';
import { BrandOwnerService } from './services/brand-owner.service';
import { DataTablesModule } from "angular-datatables";
import { CommonService } from './services/common.service';
import { CmsComponent } from './content-management/cms/cms.component';
import { AddCmsComponent } from "./content-management/cms/add-cms/add-cms.component";
import { AdmincmsService } from './services/admincms.service';
import { PermissionComponent } from './users/admin-user/permission/permission.component';
import { AdminMenuComponent } from './settings/admin-menu/admin-menu.component';
import { MenuAddComponent } from './settings/admin-menu/menu-add/menu-add.component';
import { NoPermissionComponent } from './settings/no-permission/no-permission.component';
import { ChangePasswordComponent } from './accounts/change-password/change-password.component';
import { SitesettingsComponent } from './settings/sitesettings/sitesettings.component';
import { FaqComponent } from './content-management/faq/faq.component';
import { ComparepasswordDirective } from './directive/comparepassword.directive';
import { AddFaqComponent } from './content-management/faq/add-faq/add-faq.component';
import { FaqService } from './services/faq.service';
import { StrStripHtmlPipe } from './pipe/str-strip-html.pipe';
import { DateTimeFormatPipe } from './pipe/date-time-format.pipe';
import { SitesettingsEditComponent } from './settings/sitesettings/sitesettings-edit/sitesettings-edit.component';

import { AdminRoleComponent } from './settings/admin-role/admin-role.component';
import { AdminRolePermissionComponent } from './settings/admin-role-permission/admin-role-permission.component';
import { AddeditAdminRoleComponent } from './settings/admin-role/addedit-admin-role/addedit-admin-role.component';
import { ProfileComponent } from './accounts/profile/profile.component';
//import { NgFlashMessagesModule } from 'ng-flash-messages';
import { FrontEndMenuManagementComponent } from './settings/front-end-menu-management/front-end-menu-management.component';
import { AddMenuComponent } from './settings/front-end-menu-management/add-menu/add-menu.component';
import { CampaignComponent } from './campaign/campaign.component';
import { CampaignListComponent } from './campaign-list/campaign-list.component';
import { CampaignDetailsComponent } from './campaign/campaign-details/campaign-details.component';
import { CampaignAddEditComponent } from './campaign/campaign-add-edit/campaign-add-edit.component';
import { CampaignService } from './services/campaign.service';
import { CountryService } from "./services/country.service";
import { IndustryService } from "./services/industry.service";
import { NotificationTemplatesService } from "./services/notification-templates.service";

import { EmailTemplateComponent } from './content-management/email-template/email-template.component';
import { TestimonialComponent } from './content-management/testimonial/testimonial.component';

import { EmailTemplateAddComponent } from './content-management/email-template/email-template-add/email-template-add.component';
import { AddTestimonialComponent } from './content-management/testimonial/add-testimonial/add-testimonial.component';
import { EmailTemplateService } from './services/email-template.service';
import { TestimonialService } from './services/testimonial.service';
import { MessageConversationComponent } from './campaign/message-conversation/message-conversation.component';
import { ForgotPasswordComponent } from './accounts/forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './accounts/reset-password/reset-password.component';
import { NotificationTemplatesComponent } from './content-management/notification-templates/notification.component';
import { EditNotificationTemplatesComponent } from './content-management/notification-templates/edit-notification-emplates/edit-notification.component';
import { BrandOwnerTransactionComponent } from './users/brand-owner/brand-owner-transaction/brand-owner-transaction.component';
import { InfluencerTransactionComponent } from './users/influencer/influencer-transaction/influencer-transaction.component';
import { BrandJobDetailsComponent } from './users/brand-owner/brand-job-details/brand-job-details.component';


@NgModule({
  declarations: [
    NavbarComponent,
    SidebarComponent,
    FooterComponent,
    DashboardComponent,
    InnerLayoutComponent,
    LoginLayoutComponent,
    LoginComponent,
    AddAdminComponent,
    AdminUserComponent,
    AddInfluencerComponent,
    InfluencerComponent,
    AddBrandOwnerComponent,
    BrandOwnerComponent,
    CmsComponent,
    AddCmsComponent,
    PermissionComponent,
    AdminMenuComponent,
    MenuAddComponent,
    NoPermissionComponent,
    ChangePasswordComponent,
    SitesettingsComponent,
    FaqComponent,
    ComparepasswordDirective,
    AddFaqComponent,
    StrStripHtmlPipe,
    DateTimeFormatPipe,
    SitesettingsEditComponent,
    AdminRoleComponent,
    AdminRolePermissionComponent,
    AddeditAdminRoleComponent,
    ProfileComponent,
    FrontEndMenuManagementComponent,
    AddMenuComponent,
    InfluencerDetailsComponent,
    JobDetailsComponent,
    CampaignComponent,
    CampaignListComponent,
    CampaignDetailsComponent,
    CampaignAddEditComponent,
    EmailTemplateComponent,
    TestimonialComponent,
    EmailTemplateAddComponent,
    AddTestimonialComponent,
    MessageConversationComponent,
    ForgotPasswordComponent,
    ResetPasswordComponent,
    NotificationTemplatesComponent,
    EditNotificationTemplatesComponent,
    BrandOwnerTransactionComponent,
    InfluencerTransactionComponent,
    BrandJobDetailsComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    HttpClientModule,
    AdminRoutingModule,
    FormsModule,
    ReactiveFormsModule,    
    DataTablesModule,
    EditorModule,
    NgbModule.forRoot(),
    //NgFlashMessagesModule.forRoot(),
    NgxPermissionsModule.forChild()
    
  ],
  providers: [
    AdminService,    
    CreatorService,
    BrandOwnerService,
    CommonService,
    AdmincmsService,
    FaqService,
    CampaignService,
    EmailTemplateService,
    TestimonialService,
    CountryService,
    IndustryService,
    NotificationTemplatesService
  ]
})
export class AdminModule { }
