import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { FormGroup, FormControl, FormsModule, FormArray, FormBuilder, Validators } from '@angular/forms';
import { Constant } from './../../../constant';
import { MustMatch } from './../../directive/must-match.validator';

import { AdminService } from './../../services/admin.service';
@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {
  serverErrorMessages;
  resetPasswordForm: FormGroup;
  submitted: boolean = false;
  constructor(
    private constant: Constant, 
    private adminService: AdminService, 
    private router: Router, 
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit() {

    this.resetPasswordForm = this.formBuilder.group({
      newPassword: ['', [Validators.required, Validators.pattern(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]).{8,20}$/)]],
      confirmPassword: ['', Validators.compose([Validators.required])],
      email: ['', Validators.compose([Validators.required])],
      token: ['', Validators.compose([Validators.required])],
    }, {
      // check whether our password and confirm password match
      validator: MustMatch('newPassword', 'confirmPassword')
    });

    this.activatedRoute
      .queryParams
      .subscribe(params => {
          this.resetPasswordForm.controls["email"].setValue(
            params['email']
          );
          this.resetPasswordForm.controls["token"].setValue(
            params['verication_token']
          );
      });

  }

  // convenience getter for easy access to form fields
  get i() { return this.resetPasswordForm.controls; }

  onSubmit() {

    this.submitted = true;

    // stop here if form is invalid
    if (this.resetPasswordForm.invalid) {
        return;
    }

    this.adminService.resetPassword(this.resetPasswordForm.value).subscribe(
      res => {        
          this.submitted = false;
          this.resetPasswordForm.reset(); 
          this.serverErrorMessages = res.msgtext;        
      },
      err => {
        console.log(err);
        this.submitted = false;
        this.serverErrorMessages = err.msgtext;
      }
    );
  }

}
