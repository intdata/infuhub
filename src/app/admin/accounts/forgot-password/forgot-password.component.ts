import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { FormGroup, FormControl, FormsModule, FormArray, FormBuilder, Validators } from '@angular/forms';
import { Constant } from '../../../constant';

import { AdminService } from '../../services/admin.service';
@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {
  serverErrorMessages;
  loginForm: FormGroup;
  
  constructor(private constant: Constant, private adminService: AdminService, private router: Router, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: new FormControl('', [Validators.required, Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)])
    });
  }
  

  onSubmit(form) {
    this.adminService.forgotPassword(form.value).subscribe(
      res => {        
          this.loginForm.reset(); 
          this.serverErrorMessages = res.msgtext;        
      },
      err => {
        console.log(err);
        this.serverErrorMessages = err.msgtext;
      }
    );
  }

}
