import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup, FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from "@angular/router";
import { AdminService } from '../../services/admin.service';
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  addadminForm: FormGroup;
  submitted = false;
  successMessage = "";
  id;
  fname;
  lname;
  email;
  status;
  image;
  constructor(private adminService: AdminService, private router: Router, private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder) { }
  filesToUpload: Array<File> = [];
  fileChangeEvent(fileInput: any) {
    this.filesToUpload = <Array<File>>fileInput.target.files;
  }
  ngOnInit() {
    this.id = this.adminService.getAdminId();
    this.adminService.getSingleAdminData(this.id).subscribe(data => {
      if (data.msgcode == 1) {
        //console.log(data);
        this.fname = data.results[0].first_name;
        this.lname = data.results[0].last_name;
        this.email = data.results[0].email;
        this.status = data.results[0].status;
        this.image = data.results[0].image;
      }
    });
    this.addadminForm = new FormGroup({
      fname: new FormControl('', <any>Validators.required),
      lname: new FormControl('', <any>Validators.required),
      email: new FormControl('', <any>[Validators.required, Validators.email]),
    });
  }
  get f() { return this.addadminForm.controls; }
  onSubmit() {
    this.submitted = true;
    if (this.addadminForm.invalid) {
      return;
    } else {
      const files: Array<File> = this.filesToUpload;
      const profileData: any = new FormData();
      if (files.length > 0)
      profileData.append("image", files[0], files[0]['name']);
      profileData.append("id", this.id);
      profileData.append("status", this.status);
      profileData.append("email", this.email);
      profileData.append("fname", this.addadminForm.value.fname);
      profileData.append("lname", this.addadminForm.value.lname);
      //this.addadminForm.value.id = this.id;
      //this.addadminForm.value.status = this.status;
      this.adminService.editdminData(profileData).subscribe(data => {
        if (data.msgcode == 1 && data.results.affectedRows > 0) {
          this.successMessage = "Profile updated successfully";
        }
      });
    }
  }
  cancelForm = function () {
    this.router.navigate(['/admin/dashboard']);
  }
}
