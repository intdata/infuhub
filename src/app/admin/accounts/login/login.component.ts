import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { FormGroup, FormControl, FormsModule, FormArray, FormBuilder, Validators } from '@angular/forms';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Constant } from '../../../constant';

import { AdminService } from '../../services/admin.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  serverErrorMessages;

  constructor(
    private constant: Constant, 
    private adminService: AdminService, 
    private router: Router, 
    private formBuilder: FormBuilder
  ) {}

  ngOnInit() {
    if (this.adminService.isLogin()) {
      this.router.navigate(['/admin/dashboard']);
    }
  }


  loginForm = this.formBuilder.group({
    email: new FormControl('', [Validators.required, Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)]),
    password: new FormControl('', [Validators.required])
  });


  onSubmit(form) {
    this.adminService.login(form.value).subscribe(
      res => {
        if (res.msgcode == 3) {
          this.adminService.setToken(res['token']);
          this.adminService.setAdminId(res.results.id);
          this.adminService.setAdminInfo(res.results);
          //window.location.reload();
          //this.router.navigate([this.constant.ADMIN_PATH+'dashboard']);	
          this.router.navigate(['/admin/dashboard'], { queryParams: { reload: true }});	
        } else {
          this.serverErrorMessages = res.msgtext;
        }
      },
      err => {
        console.log(err);
        this.serverErrorMessages = err.msgtext;
      }
    );
  }



}
