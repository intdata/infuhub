import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, ControlContainer, FormGroup, FormControl } from '@angular/forms';
import { Http,Response, Headers, RequestOptions } from '@angular/http';
import { Router,ActivatedRoute } from "@angular/router";
import { AdminService } from '../../services/admin.service';
import { RegistrationValidator } from './check';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {
  changePasswordForm: FormGroup;
  passwordFormGroup;
  submitted = false;
  serverMessage;
  serverResponceCode;

  constructor(private adminService: AdminService, private router: Router, private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder) {}

  ngOnInit() {

	  	this.changePasswordForm = new FormGroup({
		    old_password 	: new FormControl('', < any > Validators.required),
		    new_password 	: new FormControl('', < any > Validators.required),
		    confirm_password: new FormControl('', < any > Validators.required)
		});
  }
	get f() {
        return this.changePasswordForm.controls;
    }
  onSubmit() {
        this.submitted = true;
        if(this.changePasswordForm.invalid) {
            return;
        } else {
        		this.changePasswordForm.value.admin_id = this.adminService.getAdminId();
        		this.adminService.changePassword(this.changePasswordForm.value).subscribe(data => {
                    this.serverMessage = data.msgtext;
                    this.serverResponceCode = data.msgcode;
                });
            }
    }

    cancelForm = function() {
        this.router.navigate(['/admin/dashboard']);
    }
}
