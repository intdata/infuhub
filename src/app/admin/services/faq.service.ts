import { Injectable } from '@angular/core';
import {Http,Response, Headers, RequestOptions } from '@angular/http'; 
import {Constant} from '../../constant'; 
import { Observable } from 'rxjs/Observable';    
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';

@Injectable({
  providedIn: 'root'
})
export class FaqService {

  constructor(private http: Http, private constant:Constant) { }



	addeditFaqData(faqData){
	  	return this.http.post(this.constant.BASE_URL+'faq/addeditFaqData/',faqData).map((response: Response) =>response.json());  
	}

	
	deleteFaqData(id){
		
	  	return this.http.get(this.constant.BASE_URL+'faq/deleteFaqData/'+id).map((response: Response) =>response.json());  
	}

	getAllFaqData(){


	  	return this.http.get(this.constant.BASE_URL+'faq/getAllFaqList/').map((response: Response) =>response.json());  
	}

  getSingleFaqData(id){ 

	  	return this.http.get(this.constant.BASE_URL+'faq/getFaqData/'+id).map((response: Response) =>response.json());  
	   
	} 
}



