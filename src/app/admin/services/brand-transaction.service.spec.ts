import { TestBed } from '@angular/core/testing';

import { BrandTransactionService } from './brand-transaction.service';

describe('BrandTransactionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BrandTransactionService = TestBed.get(BrandTransactionService);
    expect(service).toBeTruthy();
  });
});
