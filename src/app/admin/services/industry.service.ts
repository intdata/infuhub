import { Injectable, Inject } from '@angular/core';
import { Constant } from './../../constant';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observer } from 'rxjs/Observer';
import { Observable } from 'rxjs/Observable';

declare var $: any;

@Injectable()
export class IndustryService {
  private ALL_INDUSTRIES_GET_URL:string;
  constructor(
    private httpClient: HttpClient,
    private constant:Constant
  ) {
    this.ALL_INDUSTRIES_GET_URL = 'get-industries';
  }

  /**
   * Service used to post influncer sign up form
   * @returns {Observable<any>}
   * @memberof IndustryService
   */
  getAllIndustries(): Observable<any> {
    const request = {
      headers: {
        
      },
      url: this.constant.BASE_URL + this.ALL_INDUSTRIES_GET_URL,
    };
    return this.httpClient.get<any>(
      request.url,
      { headers: request.headers }
    );
  }
}
