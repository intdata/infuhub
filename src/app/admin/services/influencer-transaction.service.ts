import { Injectable } from '@angular/core';
import {Http,Response, Headers, RequestOptions } from '@angular/http'; 
import {Constant} from '../../constant'; 

import { Observable } from 'rxjs/Observable';    
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';

const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';

@Injectable({
  providedIn: 'root'
})
export class InfluencerTransactionService {

  constructor(private http: Http, private constant: Constant) { }

  allData(id:any) {
    return this.http.get(this.constant.BASE_URL + 'influencer_admin_transaction/getAllData/'+id).map((response: Response) => response.json());
  }
  //get data for export
  getallExportData(params) {
    return this.http.post(this.constant.BASE_URL + 'influencer_admin_transaction/getAllExportData/',params).map((response: Response) => response.json());
  }

  //get exported excel file 
  public exportAsExcelFile(json: any[], excelFileName: string): void {
    //console.log(json);
    const header = ["Year", "Month", "Make", "Model", "Quantity", "Pct"]
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }
  //save excel file
  private saveAsExcelFile(buffer: any, fileName: string): void {
     const data: Blob = new Blob([buffer], {type: EXCEL_TYPE});
     FileSaver.saveAs(data, fileName + '_export_' + new  Date().getTime() + EXCEL_EXTENSION);
  }

  //update manual payment
  releaseManualPayment(params) {
    //console.log(params);return;
    return this.http.post(this.constant.BASE_URL + 'influencer_admin_transaction/releaseManualPayment/',params).map((response: Response) => response.json());
  }

    /**
  * Generate invoice PDF
  */
  createPdf(invoice_number:any){
    //console.log(invoice_number);
    return this.http.get(this.constant.BASE_URL + 'pdf/create/'+invoice_number).map((response: Response) => response.json());
  }
}
