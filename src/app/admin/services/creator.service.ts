import { Injectable } from '@angular/core';
import {Http,Response, Headers, RequestOptions } from '@angular/http'; 
import {Constant} from '../../constant'; 

import { Observable } from 'rxjs/Observable';    
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
@Injectable({
  providedIn: 'root'
})
export class CreatorService {

  constructor(private http: Http, private constant:Constant) { }



	saveCreatorData(creatorData){
	  	return this.http.post(this.constant.BASE_URL+'creator/saveCreator/',creatorData).map((response: Response) =>response.json());  
	}

	editCreatorData(creatorData){
	  	return this.http.post(this.constant.BASE_URL+'creator/editCreator/',creatorData).map((response: Response) =>response.json());  
	}

	deleteCreatorData(id){
		
	  	return this.http.get(this.constant.BASE_URL+'creator/deleteCreator/'+id).map((response: Response) =>response.json());  
	}


	getCreatorListData(){


	  	return this.http.get(this.constant.BASE_URL+'creator/getAllCreatorData/').map((response: Response) =>response.json());  
	}

    getSingleCreatorData(id){

	  	return this.http.get(this.constant.BASE_URL+'creator/getEditData/'+id).map((response: Response) =>response.json());  
	   
	} 

	ChangeStatus(statusData){
		const headers = new Headers();
		headers.append('Content-Type', 'application/json');

	return this.http.post(this.constant.BASE_URL+'creator/ChangeStatus/',JSON.stringify(statusData),{ headers: headers }).map((response: Response) =>response.json());  
	}

}
