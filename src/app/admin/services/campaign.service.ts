import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Constant } from '../../constant';

@Injectable({
  providedIn: 'root'
})
export class CampaignService {

  constructor(private http: Http, private constant: Constant) { }

  editData(data) {
    console.log(data);
    return this.http.post(this.constant.BASE_URL + 'campaign/getEditData/', data).map((response: Response) => response.json());
  }
  singleData(id) {
    return this.http.get(this.constant.BASE_URL + 'campaign/getSingleData/'+id).map((response: Response) => response.json());
  }
  jobDetail(id, influencerId) {
    return this.http.post(this.constant.BASE_URL + 'campaign/getJobDetail',{id:id, influencerId:influencerId}).map((response: Response) => response.json());
  }

  allData() {
    return this.http.get(this.constant.BASE_URL + 'campaign/getAllData/').map((response: Response) => response.json());
  }
  influncerData(id) {
    return this.http.get(this.constant.BASE_URL + 'campaign/getAllData/'+id).map((response: Response) => response.json());
  }
  brandOwnerData(id) {
    return this.http.get(this.constant.BASE_URL + 'campaign/getBrandOwner/'+id).map((response: Response) => response.json());
  }
  changeStatus(id) {
    return this.http.get(this.constant.BASE_URL + 'campaign/statusChange/'+id).map((response: Response) => response.json());
  }

  assignInfluencerByCampaign(id) {
    return this.http.get(this.constant.BASE_URL + 'campaign/assignInfluencerByCampaign/'+id).map((response: Response) => response.json());
  }

  getCampaignData(id) {
    return this.http.get(this.constant.BASE_URL + 'campaign/getCampaignDataById/'+id).map((response: Response) => response.json());
  }

  getInfluncerMessage(id, influencerId) {
    return this.http.post(this.constant.BASE_URL + 'campaign/getInfluncerMessage',{id:id, influencerId:influencerId}).map((response: Response) => response.json());
  }

  getDataById(id) {
    return this.http.post(this.constant.BASE_URL + 'campaign/campaignById',{id:id}).map((response: Response) => response.json());
  }

  getInfluncerdata(campaign_id, influncer_id) {
    return this.http.post(this.constant.BASE_URL + 'campaign/getInfluncerDetailMessage',{influncerId: influncer_id, campaignId: campaign_id}).map((response: Response) => response.json());
  }

  


}
