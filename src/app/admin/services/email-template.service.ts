import { Injectable } from '@angular/core';
import {Http,Response, Headers, RequestOptions } from '@angular/http'; 
import {Constant} from '../../constant'; 

import { Observable } from 'rxjs/Observable';    
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';

@Injectable({
  providedIn: 'root'
})
export class EmailTemplateService {

  constructor(private http: Http, private constant:Constant) { }
 addEditEmailTemplate(emailTemplateData){

    return this.http.post(this.constant.BASE_URL+'email_template/addEditEmailTemplate/',emailTemplateData).map((response: Response) =>response.json());  
}

deleteEmailData(id){
    return this.http.get(this.constant.BASE_URL+'email_template/deleteEmailTemplate/'+id).map((response: Response) =>response.json());  
}


getAllEmailTemplateData(){


    return this.http.get(this.constant.BASE_URL+'email_template/getAllEmailTemplateData/').map((response: Response) =>response.json());  
}

getEditDataById(id){

    return this.http.get(this.constant.BASE_URL+'email_template/getEditData/'+id).map((response: Response) =>response.json());  
   
} 

ChangeStatus(statusData){
  const headers = new Headers();
 headers.append('Content-Type', 'application/json');

return this.http.post(this.constant.BASE_URL+'email_template/ChangeStatus/',JSON.stringify(statusData),{ headers: headers }).map((response: Response) =>response.json());  
}


}
