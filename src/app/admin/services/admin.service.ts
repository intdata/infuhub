import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Constant } from '../../constant';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';

@Injectable({
	providedIn: 'root'
})
export class AdminService {

	constructor(private http: Http, private constant: Constant) { }

	// ======================= Admin Login Start ====================================== //

	login(authCredentials) {
		return this.http.post(this.constant.BASE_URL + 'adminuser/login/', authCredentials).map((response: Response) => response.json());
	}
	forgotPassword(data) {
		return this.http.post(this.constant.BASE_URL + 'adminuser/forgotPassword/', data).map((response: Response) => response.json());
	}

	resetPassword(data) {
		return this.http.post(this.constant.BASE_URL + 'adminuser/reset-admin-password/', data).map((response: Response) => response.json());
	}

	setToken(token: string) {
		localStorage.setItem('token', token);
	}

	getToken() {
		return localStorage.getItem('token');
	}

	deleteToken() {
		localStorage.removeItem('token');
	}

	setAdminId(id) {
		localStorage.setItem('adminId', id);
	}

	getAdminId() {
		return localStorage.getItem('adminId');
	}

	setAdminInfo(result) {
		localStorage.setItem('adminInfo', JSON.stringify(result));
	}

	getAdminInfo() {
		return localStorage.getItem('adminInfo');
	}

	isLogin() {
		var token = this.getToken();
		if (token) {
			return true;
		} else {
			return false;
		}
	}

	// ======================= Admin Login End ====================================== //

	saveadminData(adminUserData) {
		//console.log(adminUserData);
		return this.http.post(this.constant.BASE_URL + 'adminuser/saveAdminUser/', adminUserData).map((response: Response) => response.json());
	}

	editdminData(adminUserData) {
		console.log(adminUserData);
		return this.http.post(this.constant.BASE_URL + 'adminuser/editAdminUser/', adminUserData).map((response: Response) => response.json());
	}

	deleteAdminData(id) {
		return this.http.get(this.constant.BASE_URL + 'adminuser/deleteAdminUser/' + id).map((response: Response) => response.json());
	}

	getAdminUserData() {
		return this.http.get(this.constant.BASE_URL + 'adminuser/getAllAdminData/').map((response: Response) => response.json());
	}

	getSingleAdminData(id) {
		return this.http.get(this.constant.BASE_URL + 'adminuser/getEditData/' + id).map((response: Response) => response.json());
	}

	changePassword(adminUserData) {
		return this.http.post(this.constant.BASE_URL + 'adminuser/changePassword/', adminUserData).map((response: Response) => response.json());
	}

	setesettingValue() {
		return this.http.get(this.constant.BASE_URL + 'adminuser/setesettingValue/').map((response: Response) => response.json());
	}
	getCurrencyName(currencyId) {
		return this.http.get(this.constant.BASE_URL + 'adminuser/getCurrencyName/'+currencyId).map((response: Response) => response.json());
	}
	getSitesettingData(id){
		return this.http.get(this.constant.BASE_URL+'adminuser/singleSetesettingValue/'+id).map((response: Response) => response.json());
	}

	updateSitesetting(sitesettingValue){
		return this.http.post(this.constant.BASE_URL+'adminuser/updateSitesetting/',sitesettingValue).map((response: Response) =>response.json());
	}
	getAdminRoleData() {
		return this.http.get(this.constant.BASE_URL + 'adminuser/getAdminRoleData/').map((response: Response) => response.json());
	}
	getAllRoleType() {
		return this.http.get(this.constant.BASE_URL + 'adminmenu/getAllRoleType/').map((response: Response) => response.json());
	}
	//========save Admin Role======//
	saveAdminRole(adminRoleData) {
		return this.http.post(this.constant.BASE_URL + 'adminuser/saveAdminRole/', adminRoleData).map((response: Response) => response.json());
	}

}





