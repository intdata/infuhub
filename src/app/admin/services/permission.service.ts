import { Injectable } from '@angular/core';
import { NgxPermissionsService } from 'ngx-permissions';
import { AdminService } from './admin.service';
import { AdminMenuService } from './admin-menu.service';

@Injectable({
  providedIn: 'root'
})
export class PermissionService {

  menuPermissionData = [];
  adminId;
  adminInfo;
  menuCount;
  admin_type;
  constructor(private permissionsService: NgxPermissionsService,
    private adminMenuService: AdminMenuService,
    private adminService: AdminService) { }

  initializeApp(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.adminId = this.adminService.getAdminId();
      //console.log(this.adminId);
      this.adminInfo = JSON.parse(this.adminService.getAdminInfo());
      if(this.adminInfo){
        this.admin_type = this.adminInfo.admin_type;
      }
      else{
        this.admin_type = '';
      }
      if (this.admin_type == 'super-admin') {
        this.adminMenuService.getMenuData().subscribe(data => {
          for (var i = 0; i < data.results.length; i++) {
            this.permissionsService.addPermission(data.results[i].menu_slug + '_add');
            this.permissionsService.addPermission(data.results[i].menu_slug + '_edit');
            this.permissionsService.addPermission(data.results[i].menu_slug + '_delete');
            this.permissionsService.addPermission(data.results[i].menu_slug + '_view');
            this.adminMenuService.getParentMenuName(data.results[i].id).subscribe(pdata => {
              this.permissionsService.addPermission(pdata.results);
            });
          }
        });
        this.permissionsService.addPermission('brand-transaction_details');
        this.permissionsService.addPermission('influencer-transaction_details');
      }
      else{
        this.adminMenuService.getMenuPermissionDataByAdminId(this.adminId).subscribe(data => {
          this.menuPermissionData = data.results;
          this.permissionsService.flushPermissions();
          for (var i = 0; i < data.results.length; i++) {
            if (data.results[i].permission_view == 1) {
              this.permissionsService.addPermission(data.results[i].menu_slug + '_view');
              this.adminMenuService.getParentMenuName(data.results[i].admin_menu_id).subscribe(data => {
                this.permissionsService.addPermission(data.results);
              });
            }
            if (data.results[i].permission_add == 1) {
              this.permissionsService.addPermission(data.results[i].menu_slug + '_add');
            }
            if (data.results[i].permission_edit == 1) {
              this.permissionsService.addPermission(data.results[i].menu_slug + '_edit');
            }
            if (data.results[i].permission_delete == 1) {
              this.permissionsService.addPermission(data.results[i].menu_slug + '_delete');
            }
          }

        });
        this.permissionsService.addPermission('brand-transaction_details');
        this.permissionsService.addPermission('influencer-transaction_details');
      }
            

      setTimeout(() => {
        var permissions = this.permissionsService.getPermissions();
        //console.log(permissions);

        resolve();
      }, 1000);
    });
  }

}
