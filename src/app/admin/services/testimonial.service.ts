import { Injectable } from '@angular/core';
import {Http,Response, Headers, RequestOptions } from '@angular/http'; 
import {Constant} from '../../constant'; 

import { Observable } from 'rxjs/Observable';    
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';

@Injectable({
  providedIn: 'root'
})
export class TestimonialService {

  constructor(private http: Http, private constant:Constant) { }
  addEditTestimonial(testimonialData){

    return this.http.post(this.constant.BASE_URL+'testimonial/addEditTestimonial/',testimonialData).map((response: Response) =>response.json());  
}

deleteTestimonialData(id){
    return this.http.get(this.constant.BASE_URL+'testimonial/deleteTestimonialData/'+id).map((response: Response) =>response.json());  
}


geAllTestimonialData(){
    return this.http.get(this.constant.BASE_URL+'testimonial/geAllTestimonialData/').map((response: Response) =>response.json());  
}

getEditDataById(id){

    return this.http.get(this.constant.BASE_URL+'testimonial/getEditData/'+id).map((response: Response) =>response.json());  
   
} 

ChangeStatus(statusData){
  const headers = new Headers();
 headers.append('Content-Type', 'application/json');

return this.http.post(this.constant.BASE_URL+'testimonial/ChangeStatus/',JSON.stringify(statusData),{ headers: headers }).map((response: Response) =>response.json());  
}


}

