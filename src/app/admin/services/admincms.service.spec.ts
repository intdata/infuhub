import { TestBed } from '@angular/core/testing';

import { AdmincmsService } from './admincms.service';

describe('AdmincmsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AdmincmsService = TestBed.get(AdmincmsService);
    expect(service).toBeTruthy();
  });
});
