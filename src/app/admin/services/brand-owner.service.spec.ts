import { TestBed } from '@angular/core/testing';

import { BrandOwnerService } from './brand-owner.service';

describe('BrandOwnerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BrandOwnerService = TestBed.get(BrandOwnerService);
    expect(service).toBeTruthy();
  });
});
