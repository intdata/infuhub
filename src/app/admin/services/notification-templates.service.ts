import { Injectable, Inject } from '@angular/core';
import { Constant } from '../../constant';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { FormGroup } from '@angular/forms';

import { Observer } from 'rxjs/Observer';
import { Observable } from 'rxjs/Observable';

declare var $: any;

@Injectable()
export class NotificationTemplatesService {
  private ALL_NOTIFICATION_TEMPLATES_POST_URL:string;
  private SINGLE_NOTIFICATION_TEMPLATES_POST_URL:string;
  private NOTIFICATION_TEMPLATE_UPDATE_POST_URL:string;
  constructor(
    private httpClient: HttpClient,
    private constant:Constant
  ) {
    this.ALL_NOTIFICATION_TEMPLATES_POST_URL = 'notification-templates/get-notification-templates';
    this.SINGLE_NOTIFICATION_TEMPLATES_POST_URL = 'notification-templates/get-single-notification-template';
    this.NOTIFICATION_TEMPLATE_UPDATE_POST_URL = 'notification-templates/update-notification-template';
  }

  /**
   * Service used to get all notification templates
   * @returns {Observable<any>}
   * @memberof NotificationTemplatesService
   */
  getAllNotificationTemplates(): Observable<any> {
    const request = {
      headers: {},
      url: this.constant.BASE_URL + this.ALL_NOTIFICATION_TEMPLATES_POST_URL,
      params: {}
    };
    return this.httpClient.post<any>(
      request.url,
      request.params,
      { headers: request.headers }
    );
  }

  /**
   * Service used to get all notification templates
   * @returns {Observable<any>}
   * @memberof NotificationTemplatesService
   */
  getSingleNotificationTemplate(id: any): Observable<any> {
    const request = {
      headers: {},
      url: this.constant.BASE_URL + this.SINGLE_NOTIFICATION_TEMPLATES_POST_URL + '?id=' + id,
    };
    return this.httpClient.get<any>(
      request.url,
      { headers: request.headers }
    );
  }

  /**
   * Method to change personal info
   * @param changePasswordForm 
   */
  editNotificationTemplate(notificationTemplatesInfoForm: FormGroup): Observable<any> {
    
    const request = {
      headers: {},
      url: this.constant.BASE_URL + this.NOTIFICATION_TEMPLATE_UPDATE_POST_URL,
      params: notificationTemplatesInfoForm
    };
    return this.httpClient.post<any>(
      request.url,
      request.params,
      { headers: request.headers }
    );
  }
}
