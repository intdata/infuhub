import { Injectable } from '@angular/core';
import {Http,Response, Headers, RequestOptions } from '@angular/http'; 
import {Constant} from '../../constant'; 

import { Observable } from 'rxjs/Observable';    
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
@Injectable({
  providedIn: 'root'
})
export class BrandOwnerService {

  constructor(private http: Http, private constant:Constant) { }
	saveBrandOwnerData(brandOwnerData){
	  	return this.http.post(this.constant.BASE_URL+'brand_owner/saveBrandOwner/',brandOwnerData).map((response: Response) =>response.json());  
	}

	editBrandOwnerData(brandOwnerData){
	  	return this.http.post(this.constant.BASE_URL+'brand_owner/editBrandOwner/',brandOwnerData).map((response: Response) =>response.json());  
	}

	deleteBrandOwnerData(id){
		
	  	return this.http.get(this.constant.BASE_URL+'brand_owner/deleteBrandOwner/'+id).map((response: Response) =>response.json());  
	}


	getBrandOwnerData(){
	  	return this.http.get(this.constant.BASE_URL+'brand_owner/getAllBrandOwnerData/').map((response: Response) =>response.json());  
	}

  getSingleBrandOwnerData(id){

	  	return this.http.get(this.constant.BASE_URL+'brand_owner/getEditData/'+id).map((response: Response) =>response.json());  
	   
	} 

	ChangeStatus(statusData){
	     	const headers = new Headers();
				headers.append('Content-Type', 'application/json');
				return this.http.post(this.constant.BASE_URL+'brand_owner/ChangeStatus/',JSON.stringify(statusData),{ headers: headers }).map((response: Response) =>response.json());  
	}

	UpdateSignupStatus(statusData){
			//const headers = new Headers();
			//headers.append('Content-Type', 'application/json');
			return this.http.post(this.constant.BASE_URL+'brand_owner/UpdateSignupStatus/',statusData).map((response: Response) =>response.json());  


			//return this.http.post(this.constant.BASE_URL+'brand_owner/UpdateSignupStatus/',JSON.stringify(statusData),{ headers: headers }).map((response: Response) =>response.json());  
}
 

}

