import { Injectable } from '@angular/core';
import {Http,Response, Headers, RequestOptions } from '@angular/http'; 
import {Constant} from '../../constant'; 

//import { Observable } from 'rxjs/Observable';  
import {Observable} from 'rxjs/Rx';  
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/delay';

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  constructor(private http: Http, private constant:Constant) { }

  /* checkEmailExitsOrNot(email){

     return this.http.post(this.constant.BASE_URL+'brand_owner/checkEmailExitsOrNot/',email).map((response: Response) =>response.json());  
	   
	}*/


    checkEmailExitsOrNot(requireData) {
        const headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this.http.post(this.constant.BASE_URL+'brand_owner/checkEmailExitsOrNot',JSON.stringify(requireData),{ headers: headers })
            .map((response: Response) => response.json())
            .catch(this.handleError);
    }

    private handleError(error: any) {
        //console.log(error);
        return Observable.throw(error.json());
        ;
    }  

    getAllCurrencies() {
      const headers = new Headers();
      headers.append('Content-Type', 'application/json');
      return this.http.get(this.constant.BASE_URL+'common_function/getAllCurrencies',{ headers: headers })
          .map((response: Response) => response.json())
          .catch(this.handleError);
  }

}
