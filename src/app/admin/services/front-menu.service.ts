import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Constant } from '../../constant';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';

@Injectable({
	providedIn: 'root'
})
export class FrontMenuService {

	constructor(private http: Http, private constant: Constant) { }

	getMenuData() {
		return this.http.get(this.constant.BASE_URL + 'front_end_menu/menuList/').map((response: Response) => response.json());
	}

	getSingleMenuData(id) {
		return this.http.get(this.constant.BASE_URL + 'front_end_menu/singleMenu/' + id).map((response: Response) => response.json());
	}
//=================Fornt -end Menu =========================================================//
	getFrontParentMenuList(){
		return this.http.get(this.constant.BASE_URL+'front_end_menu/getFrontParentMenuList/').map((response: Response) =>response.json());	
	}
	
	addFrontMenuData(data) {
		return this.http.post(this.constant.BASE_URL + 'front_end_menu/addFrontMenuData/', data).map((response: Response) => response.json());
  }

  deleteMenu(id) {
	return this.http.get(this.constant.BASE_URL + 'front_end_menu/deleteMenu/'+id).map((response: Response) => response.json());
}

getPageList(id){
	return this.http.get(this.constant.BASE_URL+'admincms/getPageList/').map((response: Response) =>response.json());	
}
	
	
}
